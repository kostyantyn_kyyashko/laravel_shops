<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSubSubCategory extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'product_sub_sub_categories';

    protected $fillable = ['product_id','sub_sub_category_id'];

    protected $guarded = ['id'];


}
