<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'partners';

    protected $fillable = [
        'name_ro',
        'name_ru',
        'name_en',
    ];

    protected $guarded = ['id'];

}
