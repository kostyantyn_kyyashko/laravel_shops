<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Card extends Model {

    protected $table = 'cards';

    protected $fillable = [
        'name_ro',
        'barcode',
        'discount',
        'user_id',
    ];

    protected $guarded = ['id'];

    # Relationship
    public function user()
    {
        return $this->belongsTo(User::class,  'card_id');
    }

}
