<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model {

    protected $table = 'emails';

    protected $fillable = [
        'name_ro',
    ];

    protected $guarded = ['id'];


}
