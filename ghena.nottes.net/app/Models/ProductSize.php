<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'product_sizes';

    protected $fillable = ['product_id','qt','size_id'];

    protected $guarded = ['id'];

    # Relationship
    public function sizes()
    {
        return $this->belongsTo(Size::class, 'size_id');
    }

}
