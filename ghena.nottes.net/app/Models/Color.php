<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model {

    protected $table = 'colors';

    protected $fillable = [
        'name_ro',
        'name_ru',
        'code',
        'name_en',
    ];

    protected $guarded = ['id'];


}
