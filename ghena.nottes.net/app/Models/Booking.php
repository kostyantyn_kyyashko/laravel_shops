<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model {

    protected $table = 'bookings';

    protected $fillable = [
        'page_url',
        'first_name',
        'service_id',
        'services',
        'page_url',// 'Page de pe care a venit'
        'email',
        'phone',
    ];

    protected $guarded = ['id'];


}
