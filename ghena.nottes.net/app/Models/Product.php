<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Product extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'products';

    protected $fillable = [
        'slug',
        'image',
        'video',// upload video
        'weight',
        'category_id',
        'price',
        'name_ro',
        'text_ro',
        'path',
        'ext',
        'onMostPopularHome',
        'onLastCollectionHome',
        'orderOnCategory',// Order on short bya hin home categories
    ];

    protected $guarded = ['id'];

    #Functions
    public static function scopeWithCats($query,$catsID)
    {
        if(!is_array($catsID)) {
            $catsID = [$catsID];
        }
        return $query->select('products.*')
                        ->leftJoin('product_categories','product_categories.product_id','=','products.id')
                        ->whereIN('product_categories.category_id',$catsID);
    }

    public static function scopeWithSubCats($query,$catsID)
    {
        if(!is_array($catsID)) {
            $catsID = [$catsID];
        }
        return $query->select('products.*')
                        ->leftJoin('product_sub_categories','product_sub_categories.product_id','=','products.id')
                        ->whereIN('product_sub_categories.sub_category_id',$catsID);
    }

    public static function scopeSearchByCatSubCatSubSubCatIDs($query,$cats)
    {

        if($cats['isSubSubCategory'])
        {
            // Search by Sub Sub Category
            return $query->select('products.*')
                ->leftJoin('product_sub_sub_categories','product_sub_sub_categories.product_id','=','products.id')
                ->whereIN('product_sub_sub_categories.sub_sub_category_id',[$cats['isSubSubCategory']]);
        }
        else if($cats['isSubCategory'])
        {
            // Search by Sub Category
            return $query->select('products.*')
                ->leftJoin('product_sub_categories','product_sub_categories.product_id','=','products.id')
                ->whereIN('product_sub_categories.sub_category_id',[$cats['isSubCategory']]);
        }
        else
        {
            // Search by Category
            return $query->select('products.*')
                ->leftJoin('product_categories','product_categories.product_id','=','products.id')
                ->whereIN('product_categories.category_id',[$cats['isCategory']]);
        }
    }

    # Relationship
    // Multi Cats
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id')->withTimestamps();
    }
    // Multi Sub Cats
    public function subcategories()
    {
        return $this->belongsToMany(Category::class, 'product_sub_categories', 'product_id', 'sub_category_id')->withTimestamps();
    }
    // Multi Sub Sub Cats
    public function subsubcategories()
    {
        return $this->belongsToMany(Category::class, 'product_sub_sub_categories', 'product_id', 'sub_sub_category_id')->withTimestamps();
    }

    // Multi Sizes
    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'product_sizes', 'product_id', 'size_id')->withPivot(['qt']);
    }

    // Multi Sizes
    public function colors()
    {
        return $this->belongsToMany(Color::class, 'product_colors', 'product_id', 'color_id');
    }

    #Mutators
    /**
     * Get Thumb Images
     *
     * @return string
     */
    public function getThumbAttribute()
    {
        return $this->path.'_'.'360x240'.'.'.$this->ext;
    }


    /**
     * Filter cars on advanced search
     *
     * @param $query
     * @param $request
     * @return mixed
     */
    public static function scopeSearchFilter($query, $request,$options = '')
    {
        $lng = App::getLocale();
        $cats = isset($options['cats']) ? $options['cats'] : ['isSubSubCategory' =>0,'isSubCategory' => 0,'isCategory' => ''];

        $query = $query->leftJoin('product_categories','product_categories.product_id','=','products.id')
                    ->leftJoin('product_sub_categories','product_sub_categories.product_id','=','products.id')
                    ->leftJoin('product_sub_sub_categories','product_sub_sub_categories.product_id','=','products.id');

        if($cats['isSubSubCategory'])
        {
            // Search by Sub Sub Category
            $query = $query->whereIN('product_sub_sub_categories.sub_sub_category_id',[$cats['isSubSubCategory']]);
        }
        else if($cats['isSubCategory'])
        {
            // Search by Sub Category
            $query = $query->whereIN('product_sub_categories.sub_category_id',[$cats['isSubCategory']]);
        }
        else if($cats['isCategory'])
        {
            // Search by Category
            $query = $query->whereIN('product_categories.category_id',[$cats['isCategory']]);
        }


        // Models
        if($request->input('models')) {
            $models = $request->input('models');
            if($options['category']->parent_id == 0)
            {
                $query = $query->whereIN('product_sub_categories.sub_category_id',$models);
            }
            else
            {
                $query = $query->whereIN('product_sub_sub_categories.sub_sub_category_id',$models);
            }
        }
        //  Colletions
        if($request->input('collections')) {
            $collections = $request->input('collections');
            $query = $query->whereIN('product_sub_categories.sub_category_id',$collections);
        }

        // by sizes
        if($request->input('sizes')) {
            $sizes = $request->input('sizes');
            $query = $query->whereIN('product_sizes.size_id',$sizes);
        }

        // by colors
        if($request->input('colors')) {
            $colors = $request->input('colors');
            $query = $query->leftJoin('product_colors','product_colors.product_id','=','products.id')
                            ->whereIN('product_colors.color_id',$colors);
        }

        // Pret de la
        if($request->input('price_from')) {
            $query = $query->where('price_'.$lng,'>=',$request->input('price_from'));
        }
        // Pret pina la
        if($request->input('price_to')) {
            $query = $query->where('price_'.$lng,'<=',$request->input('price_to'));
        }
        $query = $query->orderBy('price_'.$lng,'desc');
        return $query;
    }

    # Functions

    public function formattedSlug()
    {
        return route('product',$this->slug);
    }

    public function slug()
    {
        return route('product',$this->slug);
    }

    public function thumb($size = '',$imageNr = '')
    {
        return empty($size) ? $this->{'image'.$imageNr} : $this->{'path'.$imageNr}.'_'.$size.'.'.$this->{'ext'.$imageNr};
    }
}
