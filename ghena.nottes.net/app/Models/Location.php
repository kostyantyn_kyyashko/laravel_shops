<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

    protected $table = 'locations';

    protected $fillable = [
        'short_title',
        'code',
        'name_ro',
        'name_ru',
        'name_en',
    ];

    protected $guarded = ['id'];


}
