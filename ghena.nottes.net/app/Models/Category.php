<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    protected $fillable = [
        'name_ro',
        'name_ru',
        'name_en',
        'page_id',
        'name2_ro',
        'name2_ru',
        'name2_en',
        'text_ro',
        'text_ru',
        'text_en',
        'text2_ro',
        'text2_ru',
        'text2_en',
        'active',
        'order',
        'product1_id',// for shop by
        'product2_id',// for shop by
        'onHomeShopBy',// on home at the bottom in shop by
        'isCollection',
        'product_id',
        'have_childrens',
        'views',
    ];

    protected $guarded = ['id'];

    #realtions
    
    public function subCategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id')->withTimestamps();;
    }

//    public function productsAll()
//    {
//        return $this->hasMany(Product::class,'category_id','id');
//    }

    // For shop by Products
    public function product1()
    {
        return $this->belongsTo(Product::class,'product1_id');
    }

    public function product2()
    {
        return $this->belongsTo(Product::class,'product2_id');
    }

    public static function getList()
    {
        return parent::where('active',1)->get();
    }

    public static function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public static function scopeSelectFields($query)
    {
        return $query->select('id','name_ro','name_ru','slug');
    }

    public function slug()
    {
        return route('category',[$this->slug]);
    }

}
