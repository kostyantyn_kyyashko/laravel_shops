<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryProdcuts extends Model {


    protected $dates = ['deleted_at'];

    protected $table = 'product_categories';

    protected $fillable = ['category_id','product_id'];

    protected $guarded = ['id'];


}
