<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderHome extends Model {

    /**
     * When admin set the money for the user
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'slider_home';

    protected $fillable = [
        'location_id',
        'product_id',
        'order',
        'name_ro',
        'name_ru',
        'link_ro',
        'link_ru',
        'image',
        'image1',
        'image2',
        'active',
    ];

    protected $guarded = ['id'];

    # Relationship
    public function product()
    {
        return $this->belongsTo(Product::class,  'product_id');
    }



}
