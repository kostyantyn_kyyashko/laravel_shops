<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Vars extends Model {

//    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'vars';

    protected $fillable = [
        'name_ro',
        'name_ru',
        'name_md',
        'name_eu',
        'name_us',
        'text_ro',
        'text_ru',
        'text_md',
        'text_eu',
        'text_us',
        'text_en',
        'var'
    ];

    protected $guarded = ['id'];

    public static function getList($lng = 'ro')
    {
        Cache::forget('vars');
        if (Cache::has('vars')) {
            $result = Cache::get('vars');
        } else {
            $list = Vars::get();
            $result = [];
            if($list) {
                foreach($list as $item) {
                    $result[$item->var] = $item->{'name_'.$lng};
                }
            }
            Cache::forever('vars', $result);
        }

        return $result;
    }
}
