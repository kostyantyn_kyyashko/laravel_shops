<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Wishlist extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'wishlists';

    protected $fillable = [
        'product_id',
        'user_id',
        'session',
    ];

    protected $guarded = ['id'];

    # Relationship

    // Product
    public function product()
    {
        return $this->belongsTo(Product::class,  'product_id');
    }


    #functions
    /**
     * Add products to wishlists
     * @param $product_id
     */
    public static function add($product_id)
    {
        $data = Auth::user() ? ['product_id' => $product_id,'user_id' =>  Auth::user()->id] : ['product_id' => $product_id,'session' => session()->getId()];
        parent::updateOrInsert($data,$data);
    }
    /**
     * Remove products to wishlists
     * @param $product_id
     */
    public static function remove($product_id)
    {
        $data = Auth::user() ? ['product_id' => $product_id,'user_id' =>  Auth::user()->id] : ['product_id' => $product_id,'session' => session()->getId()];
        parent::where($data)->delete();
    }

    public static function getWishlistIDs()
    {
        $data = Auth::user() ? ['user_id' =>  Auth::user()->id] : ['session' => session()->getId()];
        return parent::select('product_id')->where($data)->pluck('product_id')->toArray();
    }


    public static function updateWishlistToUserID()
    {
        Wishlist::where('session',session()->getId())->update(['user_id' =>  Auth::user()->id]);
    }

    public function slug()
    {
        return route('product',$this->slug);
    }

}
