<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model {

    protected $table = 'sizes';

    protected $fillable = [
        'name_ro',
        'name_ru',
        'name_en',
        'code',
    ];

    protected $guarded = ['id'];

}
