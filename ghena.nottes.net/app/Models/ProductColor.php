<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model {

    protected $table = 'product_colors';

    protected $fillable = [
        'color_id',
        'price',
        'product_id',
    ];

    protected $guarded = ['id'];

    # Relationship
    public function colors()
    {
        return $this->belongsTo(Color::class, 'color_id');
    }
}
