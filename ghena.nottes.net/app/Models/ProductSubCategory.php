<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSubCategory extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'product_sub_categories';

    protected $fillable = ['product_id','category_id'];

    protected $guarded = ['id'];


}
