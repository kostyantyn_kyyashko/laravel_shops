<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderHeader extends Model {

    /**
     * When admin set the money for the user
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'slider_header';

    protected $fillable = [
        'location_id',
        'name_ro',
        'image',
        'active',
    ];

    protected $guarded = ['id'];

    public static function homeList($location)
    {
        $sliderList  = SliderHeader::select("slider_header.*")
                                   ->where('locations.slug',$location)
                                   ->leftJoin('locations','locations.id','=','slider_header.location_id')
                                   ->get();
        return $sliderList;
    }


}
