<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Route;

class Page extends Model {

    protected $dates = ['deleted_at'];

    protected $table = 'pages';

    protected $fillable = [
        'location_id',
        'name_ro',
        'name_ru',
        'name_en',
        'name2_ro',
        'name2_ru',
        'name2_en',
        'text_ro',
        'text_ru',
        'text_en',
        'text2_ro',
        'text2_ru',
        'text2_en',
        'haveSubmenu',// have crutoi submenu
        'active',
        'order',
        'onNavMenu',
        'category_id',
        'onFooter',
        'parent_id',
        'have_childrens',
        'onFooterShop',
        'onFooterHelpAndInformation',
        'isCategory',// is categor for menu
        'views',
    ];

    protected $guarded = ['id'];

    public static function getList()
    {
        return parent::where('active',1)->get();
    }

    public static function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public static function scopeSelectFields($query)
    {
        return $query->select('id','name_ro','name_ru','slug');
    }

    #Mutators
    /**
     * Get Thumb Images
     *
     * @return string
     */
    public function getThumbAttribute()
    {
        return $this->path.'_'.'360x240'.'.'.$this->ext;
    }


    # Relationship

    // Locatie
    public function locatie()
    {
        return $this->belongsTo(Location::class,  'location_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,  'category_id');
    }

    public function subpages()
    {
        return $this->hasMany(Page::class, 'parent_id');
    }

    # Functions

    public function formattedSlug()
    {
        $slug = Route::has($this->slug) ? route($this->slug) : route('info',[$this->slug]);
        $slug = $this->isCategory && $this->category_id > 0 ? route('category',[$this->category->slug]) : $slug;
        return $slug;
    }


    public function getSubMenu()
    {
        $subMenuList = '';
        if($this->haveSubmenu) {
            $subMenuList = Category::where('page_id',$this->id)->get();
        }
        return $subMenuList;
    }
    /*
     * Show contend depend of location
     *
     */
    public static function ScopeByLocation($query,$location)
    {
        return $query->select("pages.*")
                    ->where('locations.slug',$location)
                    ->leftJoin('locations','locations.id','=','pages.location_id');
    }

}
