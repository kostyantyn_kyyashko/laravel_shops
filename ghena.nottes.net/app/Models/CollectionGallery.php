<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectionGallery extends Model {

    protected $table = 'collection_galleries';

    protected $fillable = [
        'name',
        'ext',
        'collection_id',
        'path',
    ];

    protected $guarded = ['id'];


}
