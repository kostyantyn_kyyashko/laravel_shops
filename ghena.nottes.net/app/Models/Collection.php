<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Collection extends Model {

    protected $table = 'collections';

    protected $fillable = [
        'onNewsFooter',
        'name_ro',
        'name_ru',
        'name_md',
        'name_eu',
        'name_us',
        'link_ro',
        'link_ru',
        'link_md',
        'link_eu',
        'link_us',
        'link_en'
    ];

    protected $guarded = ['id'];

}
