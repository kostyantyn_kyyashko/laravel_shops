<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model {

    protected $table = 'restaurants';

    protected $fillable = [
        'slug',
        'image',
        'lat',
        'lng',
        'name_ro',
        'name_ru',
        'name_en',
        'text_ro',
        'text_ru',
        'text_en',
        'text2_ro',
        'text2_ru',
        'text2_en',
    ];

    protected $guarded = ['id'];

    # Relationship

    // Galerie
    public function gallery()
    {
        return $this->hasMany(RestaurantGallery::class,  'restaurant_id');
    }

}
