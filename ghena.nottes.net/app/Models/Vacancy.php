<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model {

    protected $table = 'vacancies';

    protected $fillable = [
        'fileName',
        'path',
        'func_id',// functia , if from pages table
        'location_id',
        'page_url',// 'Page de pe care a venit'
    ];

    protected $guarded = ['id'];


}
