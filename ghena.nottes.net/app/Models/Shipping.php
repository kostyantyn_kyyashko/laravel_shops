<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model {

    protected $table = 'shipping';

    protected $fillable = [
        'name_ro',
        'name_ru',
        'name_en',
    ];

    protected $guarded = ['id'];


}
