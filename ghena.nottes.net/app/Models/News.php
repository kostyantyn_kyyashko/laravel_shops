<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class News extends Model {

//    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'news';

    protected $fillable = [
        'slug',
        'category_id',
        'name_ro',
        'name_ru',
        'name_en',
        'active',
        'date',
        'text_ro',
        'text_ru',
        'text_en',
        'onPopularNews',
        'views',
        'path',
        'ext',
        'image',
    ];

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public static function getList($limit = '',$request = '')
    {
        $query = parent::orderBy('date','desc');

        if($request && $request->input('start')) {
            $query = $query->where('date','>=',Carbon::parse($request->input('start'))->format('Y-m-d'));
        }

        if($request && $request->input('end')) {
            $query = $query->where('date','<=',Carbon::parse($request->input('end'))->format('Y-m-d'));
        }

        $query = $query->paginate($limit);

        return $query;
    }


    public static function scopeActive($query)
    {
        return $query->where('active',1);
    }
}
