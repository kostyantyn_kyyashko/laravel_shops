<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model {

    protected $table = 'order_products';

    protected $fillable = [
        'order_id',
        'product_id',
        'size_id',
        'color_id',
        'qt',
        'price',
    ];

    protected $guarded = ['id'];

}
