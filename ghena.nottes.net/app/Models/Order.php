<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $table = 'orders';

    protected $fillable = [
        'total',
        'phone',
        'discount',
        'shipping_date',
        'delivery_date',
        'user_id',
        'first_name',
        'last_name',
        'email',
        'address1',
        'address2',
        'location',
        'district',
        'qt',
        'country',
        'postal_code',
        'invoiceID',
        'payType', // // 1 - CASH on delivery , 2 - CARD on delivery
        'payNote', // Notification on payment
        'status', // 1 - achitat , 2 - neachitat , 3 - canceled
        'note', // for payment
        'date',
        'shipping_cost',
        'shipping_id',
        'subtotal',
        'total',
        'note',
    ];

    protected $guarded = ['id'];

    #Relation

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products', 'order_id', 'product_id')->with(['sizes','colors'])->withPivot(['qt','size_id','color_id']);
    }

}
