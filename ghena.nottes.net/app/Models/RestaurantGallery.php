<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantGallery extends Model {

    protected $table = 'restaurant_galleries';

    protected $fillable = [
        'name',
        'ext',
        'restaurant_id',//
        'path',
    ];

    protected $guarded = ['id'];

}
