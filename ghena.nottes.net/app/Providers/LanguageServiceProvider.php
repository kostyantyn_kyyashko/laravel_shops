<?php

/*
| file: app/Providers/LocalServiceProvider.php
| to load composer require-dev packages only when in local dev env
| this SP goes in app/Providers/ as usual and then
| add this to providers array in config/app.php:
| App\Providers\LocalServiceProvider::class
*/

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class LanguageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
       $this->setRouteLang($request);
    }
 
    /**
     * Register any application services.
     *
     * @return void
     */
    public static function setRouteLang($request)
    {
        $language  = $request->segment(1);
        if(isset(config('app.locales')[$language])) {
            App::setLocale($language);
//            Session::put('locale',$language);
//            session(['locale' => $language]);
        }
        else
        {
//            dd(session('locale'));
//            if(!Session::has('locale'))
//            {
//                Session::put('locale', config('app.locale'));
//            } else {
//                App::setLocale(Session::get('locale'));
//            }
            App::setLocale(config('app.locale'));
        }
//        dd(session('locale'));
        return App::getLocale();
//
//        // old
//        if(!\Session::has('locale'))
//        {
//            \Session::put('locale', \Config::get('app.locale'));
//        }

//        $locale = App::getLocale();
//        if (App::isLocale('en')) {
//            //
//        }
//        App::setLocale(\Session::get('locale'));
        //        Config::set('routeLang',$routeLang);
    }
}
