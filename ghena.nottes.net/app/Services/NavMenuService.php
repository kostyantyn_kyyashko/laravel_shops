<?php

namespace App\Services;

use App\Models\Color;
use App\Models\ContactCategory;
use App\Models\Location;
use App\Models\Page;
use Illuminate\Support\Facades\Cache;

class NavMenuService
{
    public function getNav()
    {
        Cache::forget('navMenu');
        if (Cache::has('navMenu')) {
            $menu = Cache::get('navMenu');
        }
        else
        {
            $collection       = collect(Page::get());

            // on footer privacyMenu
            $menu['privacyMenu'] = $collection->filter(function ($value, $key) {
                if($value->id == 15 || $value->id == 16) {
                    return $value;
                }
            });

            // on menu header
            $menu['navMenu'] = $collection->filter(function ($value, $key) {
                if($value->onNavMenu == 1) {
                    return $value;
                }
            });
            $menu['navMenu']  = $menu['navMenu']->sortBy('order')->all();

            // show on footer in Shop Column
            $menu['navFooterShop'] = $collection->filter(function ($value, $key) {
                if($value->onFooterShop == 1) {
                    return $value;
                }
            });

            // show on footer in Help&Information Column
            $menu['navFooterHelpInformation'] = $collection->filter(function ($value, $key) {
                if($value->onFooterHelpAndInformation == 1) {
                    return $value;
                }
            });

            $menu['locations'] = Location::get();
            $menu['colors']    = Color::get();

            // add  to cache
            Cache::forever('navMenu', $menu);
        }
        return $menu;
    }
}