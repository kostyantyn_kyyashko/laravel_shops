<?php

namespace App\Services;


use App\Models\Car;
use App\Models\News;
use App\Models\NewsRent;
use App\Models\Product;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchService
{
    public function search($request,$lng,$page)
    {
        $perPage     = 10;
        $result      = '';
        $s           = $request->input('s');
        if($s) {
            // Search Products
            $queryProducts = Product::where('name_'.$lng, 'like','%'.$s.'%')
                                      ->orWhere('name2_'.$lng, 'like','%'.$s.'%')
                                      ->orWhere('name3_'.$lng, 'like','%'.$s.'%')
                                      ->orWhere('text_'.$lng, 'like','%'.$s.'%')
                                      ->get();

            $searhResult = $queryProducts;

            // pagination
            $currentPage      = LengthAwarePaginator::resolveCurrentPage();
            $itemCollection   = collect($searhResult);
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            $paginatedItems   = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            $paginatedItems->setPath($request->url());

            $result      = $paginatedItems;
        }

        return $result;
    }
}