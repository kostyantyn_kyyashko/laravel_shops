<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;
use SplFileInfo;

class ImageService
{

    /**
     * Generate all type of articles images (120x80,280x200,400x400)
     * @param $article
     * @param ImageService $imageService
     */
    public function generateImages($article,$table = 'product',$dirID = '',$image = '')
    {
        $imageToResize = $image == '' ? $article->image : $image;
        $original =  $imageToResize != '' ? public_path($imageToResize) : '';
//        dd($original);
        if($original && file_exists($original)) {
            switch ($table) {
                case 'partner' :
                    $dimensions = resizeDimensionsPartner();
                    break;
                case 'collection' :
                    $dimensions = resizeDimensionsCollection();
                    break;
                case 'category' :
                    $dimensions = resizeDimensionsCategory();
                    break;
                case 'product' :
                    $dimensions = resizeProducts();
                    break;
                default:
                    $dimensions = resizeDimensions();
                    break;
            }
            foreach ($dimensions as $dim) {
                $dim    = explode('x',$dim);
                $width  = $dim[0];
                $height = $dim[1];
                $this->resize_image($imageToResize,$article->id,$width, $height, 'exact',$table,$dirID);
            }
        }
    }

    public function resize_image($path,$id, $width, $height, $mode = '',$type = 'news',$dirID = '')
    {
        $orignal_path      = $path;
        $pathinfo          = pathinfo($orignal_path);
        $filename          = $pathinfo['filename'];
        $extension         = $pathinfo['extension'];

        // check dir
        $dir_path = $dirID ?  public_path('/uploads/' . $type . '/'.$dirID.'/') : public_path('/uploads/' . $type . '/');
        $dest     = $dir_path.'/'.$filename.'_'.$width.'x'.$height.'.'.$extension;

        if(file_exists(public_path($orignal_path)))
        {
            $img = Image::make((public_path($orignal_path)));
            switch ($width.'x'.$height) {
                // article long // todo add on home and cat detail
                case '600x600':
                    $img->heighten($height, function ($constraint) {
                        $constraint->upsize();
                    });
                    break;
                // article standart // todo add on home and cat detail
                // todo try to fit correct
                case '360x240':
                    $img->heighten($height, function ($constraint) {
                        $constraint->upsize();
                    });
                    break;
                default:
                    $img->fit($width, $height, function ($constraint) {
                        $constraint->upsize();
                    });

                    break;
            }
            $img->save($dest);

        }
        return $dest;
    }

    public function generateImagesNews($article)
    {
        $original =  $article->image != '' ? public_path($article->image) : '';
        if($original && file_exists($original)) {
            $dimensions = resizeDimensionsNews();
            foreach ($dimensions as $dim) {
                $dim    = explode('x',$dim);
                $width  = $dim[0];
                $height = $dim[1];
                $this->resize_image($article->image,$article->id,$width, $height, 'exact','news');

            }
        }
    }
}