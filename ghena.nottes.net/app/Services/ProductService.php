<?php

namespace App\Services;

use App\Models\Tag;
use Carbon\Carbon;

class ProductService
{

    public function listOfCategories($request)
    {
        $catsArr = [];
        if($request->input('categories')) {
                foreach ($request->input('categories') as $cat) {
                    array_push($catsArr, $cat);
                }
        }
        return $catsArr;
    }


    public function listOfSubCategories($request)
    {
        $catsArr = [];
        if($request->input('subcategories')) {
            foreach ($request->input('subcategories') as $cat) {
                array_push($catsArr, $cat);
            }
        }
        return $catsArr;
    }

    public function listOfSubSubCategories($request)
    {
        $catsArr = [];
        if($request->input('subsubcategories')) {
            foreach ($request->input('subsubcategories') as $cat) {
                array_push($catsArr, $cat);
            }
        }
        return $catsArr;
    }


    public function listOfSizes($request)
    {
        $catsArr = [];
        if($request->input('sizes')) {
            foreach ($request->input('sizes') as $cat) {
                array_push($catsArr, $cat);
            }
        }
        return $catsArr;
    }

    public function listOfColors($request)
    {
        $catsArr = [];
        if($request->input('colors')) {
            foreach ($request->input('colors') as $cat) {
                array_push($catsArr, $cat);
            }
        }
        return $catsArr;
    }

}