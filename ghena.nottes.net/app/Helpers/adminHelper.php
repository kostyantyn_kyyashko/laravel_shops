<?php

function showTags($tags)
{
    $result = '';
    if($tags) {
        foreach ($tags as $tag) {
            $tagArr[] = $tag['name'];
        }
        $result = implode(',',$tagArr);
    }
    return $result;
}

function delete_dir($path) {
    return !empty($path) && is_file($path) ?
        @unlink($path) :
        (array_reduce(glob($path.'/*'), function ($r, $i) { return $r && delete_dir($i); }, TRUE)) && @rmdir($path);
}

