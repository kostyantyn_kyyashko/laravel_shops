<?php

// PAYNET

use Carbon\Carbon;

function resizeDimensions()
{
    return  [
        '360x240', // stiri v1
        '720x400', // stiri v1
    ];
}

function resizeProducts()
{
    return  [
        '570x820', // prodcut list http://test.test/products.html
        '349x490', // home slider header( orig 349x490)
//        '260x406', // home last colection ( orig 415x648)
        '550x405', // home last colection img 2  ( orig 880x649)
        '550x370', // home last colection img v2 ca sa fie in rind cu restul
        '260x370', // home SHOP BY , account payment detail,http://test.test/cart-2.html,http://test.test/cart-4.html,http://test.test/cart-6.html
//        '255x367', // home MOST POPULAR
//        '235x335', // account order list  ( orig 260x370)
//        '307x436', // account wish list   ( orig 260x370)
//        '388x561', // product detail for add to card  ( orig 570x820)
    ];
}

function resizeDimensionsPartner()
{
    return  [
        '168x119',
    ];
}

function resizeDimensionsCollection()
{
    return  [
        '193x276',
        '437x607',
        '900x676',
    ];
}

function resizeDimensionsCategory()
{
    return  [
        '560x805',
        '255x362',
        '405x606',
        '603x548',
    ];
}

function resizeDimensionsNews()
{
    return  [
        '356x218',
        '1170x400',
    ];
}

function ExternalDate()
{
    $addHours = ENV('ADAPTING_HOURS');
    $date = strtotime("+".$addHours." hour");
//    return date('Y-m-d', $date).'T'.date('H:i:s', $date);
    return date('Y-m-d').'T'.date('H:i:s');
}

function ExpiryDateGet()
{
    $addHours = ENV('EXPIRY_DATE_HOURS');
    $date = strtotime("+".$addHours." hour");
    return date('Y-m-d', $date).'T'.date('H:i:s', $date);
}

function SignatureGet($pay)
{
    $_sing_raw  = ENV('MERCHANT_Currency');
    $_sing_raw .= $pay['CustomerAddress'].$pay['CustomerCode'].$pay['CustomerName'];
    $_sing_raw .= $pay['ExpiryDate'].strval($pay['ExternalID']).ENV('MERCHANT_CODE');
    $_sing_raw .= $pay['Amount'].$pay['serviceName'].$pay['serviceDescription'];
    $_sing_raw .= ENV('MERCHANT_SEC_KEY');

//    echo "<br />--".strval($pay['ExternalID']);
//    echo "<br />--".$_sing_raw;
//    echo "<br />--".base64_encode(md5(trim($_sing_raw), true));
//   dd(11);
    return base64_encode(md5(trim($_sing_raw), true));
}

function diffInHours($end,$start = '')
{
    $start = Carbon::now();
    $date  = Carbon::parse($end);
    $diff  = $date->diffInHours($start);
    return $diff;
}

// END PAYNET


function statusType()
{
    return ['1' => 'Achitat','2' => 'NeAchitat','3' => 'Anulat'];
}

function productsType()
{
    return ['1' => 'Produs','2' => 'Card'];
}


function generateUID()
{
//    return round(microtime(true) * 1000);
    return uniqid();

    $data = random_bytes(16);
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


function langSwitch($langCode)
{
    $request = new \Illuminate\Support\Facades\Request();
    $langSegment = $request::segment(1);
    if($langSegment) {
        $current = preg_replace('#^/?([a-z]{2}/)?#', null, preg_replace('#^/([a-z]{2})?$#', null, $request::getPathInfo()));
    } else {
        $current = ltrim($request::getPathInfo(), '/ ');
    }
    return '/'.$langCode .($current != ''  ?  '/' . $current : '');
}


function valutaValue($value)
{
    $valuta = [
        'md' => 'MDL',
        'ro' => 'RON',
        'ru' => 'RUB',
        'eu' => '€',
        'us' => '$',
    ];
    return isset($valuta[$value]) ? $valuta[$value] : 'MDL';
}

function langs()
{
   $location = \App\Models\Location::orderBy('order','ASC')->get();
   $langs = [];
   foreach ($location as $value) {
       $langs[] = ['code' => $value->code,'title' => $value->short_title];
   }
   return $langs;
}
function exceptCode()
{
    return ['md','ro','ru','eu','us'];
}

function monthListName($m,$lng)
{
    $months['ro'] = array('Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie');
    $months['en'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $months['ru'] = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентярь','Октябрь','Ноябрь','Декабрь');
    return $months[$lng][$m-1];
}


function nl2pNew( $string, $line_breaks = true, $xml = true ) {

    // Remove current tags to avoid double-wrapping.
    $string = str_replace( array( '<p>', '</p>' ), '', $string );

    // Default: Use <br> for single line breaks, <p> for multiple line breaks.
    if ( $line_breaks == true ) {
        $string = '<p>' . preg_replace(
                array( "/([\n]{2,})/i", "/([\r\n]{3,})/i", "/([^>])\n([^<])/i" ),
                array( "</p>\n<p>", "</p>\n<p>", '$1<br' . ( $xml == true ? ' /' : '' ) . '>$2' ),
                trim( $string ) ) . '</p>';

        // Use <p> for all line breaks if $line_breaks is set to false.
    } else {
        $string = '<p>' . preg_replace(
                array( "/([\n]{1,})/i", "/([\r]{1,})/i" ),
                "</p>\n<p>",
                trim( $string ) ) . '</p>';
    }

    // Remove empty paragraph tags.
    $string = str_replace( '<p></p>', '', $string );

    // Return string.
    return $string;

}

function shortDesc($value, $limit = 160)
{
    $short = strip_tags(htmlspecialchars_decode($value));
    return str_limit($short, $limit);
}

function isMobile()
{
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
        return true;
    } else {
        return false;
    }
}

function showDate($date)
{
    return date('d F, Y',strtotime($date));
}
