<?php
namespace App\Events;

use App\Models\Article;
use Illuminate\Queue\SerializesModels;

class ArticleViews extends Event
{
    use SerializesModels;

    public $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function broadcastOn()
    {
        return [];
    }
}