<?php

namespace App\Imports;

use App\Models\Card;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

/**
 * artisan make:import CsvImport
 */

class CsvImport implements ToModel, WithChunkReading
{

    public function model(array $row)
    {
        if (empty($row[0])) {
            return null;
        }
//
        $card = Card::where('barcode',$row[0])->first();
        if(empty($card)) {
            return new Card([
                'barcode' => $row[0],
                'name_ro' => $row[1],
                'discount' => $row[2],
            ]);
        } else {
            return null;
        }
    }
//
//    public function batchSize(): int
//    {
//        return 100;
//    }

    public function chunkSize(): int
    {
        return 100;
    }
}
