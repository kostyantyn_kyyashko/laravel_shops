<?php
namespace App\Listeners;

use App\Events\ArticleViews;
use App\Models\Views;

class ArticleViewsListner
{
    public function __construct()  { }

    public function handle(ArticleViews $event)
    {
        $data = [
                 'table' => 'articles',
                 'table_id' => $event->article->id,
                 'ip' => \Request::ip()
            ];
        $article = Views::where($data)->count();
        if($article == 0) {
            Views::create($data);
            $event->article->increment('views');
        }

    }
}