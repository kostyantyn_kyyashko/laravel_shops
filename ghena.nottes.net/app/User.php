<?php

namespace App;

use App\Models\Card;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password',
        'first_name',
        'last_name',
        'phone',
        'email',
        'address1',
        'address2',
        'location',
        'district',
        'country',
        'postal_code',
        'person_type',// 1  Persoană fizică , 2 socieatea
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    # Relationship
    public function card()
    {
        return $this->belongsTo(Card::class,  'card_id');
    }

    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();

        if(is_null($check)){
            return static::create($input);
        }

        return $check;
    }


    public function addNewGoogle($input)
    {
        $check = static::where('google_id',$input['google_id'])->first();

        if(is_null($check)){
            return static::create($input);
        }

        return $check;
    }
}
