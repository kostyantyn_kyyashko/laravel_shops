<?php

namespace App\Http\Transformers\Product;

use App\Http\Transformers\Transformer;

class ProductVueTransformer extends Transformer
{
    private $lng;
    public function __construct($lng)
    {
        $this->lng        = $lng;
    }

    public function transform($product)
    {
          return $data = [
            'id'           => $product['id'],
            'slug'         => $product['slug'],
            'image'        => $product['path'].'_570x820.'.$product['ext'],
            'name'         => $product['name_'.$this->lng],
            'link'         => $product['name_'.$this->lng],
            'price'        => $product['price_'.$this->lng],
//            'link'         => routeUrl(route('product', ['slug' => $product['slug']]),$this->lng)."?start=$productInfo[start]&end=$productInfo[end]&name=$productInfo[name]&phone=$productInfo[phone]",

        ];
    }
}
