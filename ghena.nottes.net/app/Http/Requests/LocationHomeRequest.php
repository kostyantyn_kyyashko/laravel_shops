<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationHomeRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'location_code.required'  => 'Please select a country',
        ];
    }

}
