<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ro' => 'required',
//            'sizes' => 'required',
//            'color' => 'required',
//            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_ro.required'  => 'Numele este obligatoriu.',
            'category_id.required'  => 'Categoria este obligatoriu.',
        ];
    }

}
