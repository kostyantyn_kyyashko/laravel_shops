<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class MiddlewareApp {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language  = $request->segment(1);
        $routeLang = '';
        if(isset(config('app.locales')[$language])) {
            App::setLocale($language);
            $routeLang = $language;
        }
//        Config::set('routeLang',$routeLang);

//        dd(App::getLocale());
//        // old
//        if(!\Session::has('locale'))
//        {
//            \Session::put('locale', \Config::get('app.locale'));
//        }

//        $locale = App::getLocale();
//        if (App::isLocale('en')) {
//            //
//        }
//        App::setLocale(\Session::get('locale'));

        return $next($request);
    }

}