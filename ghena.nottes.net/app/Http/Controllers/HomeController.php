<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationHomeRequest;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\SliderHome;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class HomeController extends AppController
{

    /**
     * Show the application home page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if(!session()->exists('hideLocationPage')) {
            return redirect(route('locations'));
        }
        $page               = Page::find(1);
        $activeMenu         = $page->id;
        $mostPopularCat     = Category::find(6);// Best Boys
        $byTheLook          = Category::find(7);
        $byTheLookList      = Category::where('parent_id',7)->orderBy('order','asc')->get();
        $mostPopularList    = Product::where('onMostPopularHome',1)->get();
        $lastCollectionList = Product::where('onLastCollectionHome',1)->get();
        $shopByCatList      = Category::where('onHomeShopBy',1)->with(['product1','product2'])->get();
        $sliderHomeList     = SliderHome::with(['product'])->get();
        $playVideo          = Page::find(14);
        $getCoupon          = Page::find(12);
        return view('home.index',compact('page',
                                    'byTheLookList',
                                    'activeMenu',
                                    'mostPopularList',
                                    'mostPopularCat',
                                    'shopByCatList',
                                    'sliderHomeList',
                                    'lastCollectionList',
                                    'getCoupon',
                                    'playVideo',
                                    'byTheLook'
        ));
    }

    /*
     * Show locations when open first website
     */
    public function locations()
    {
        $showHeader   = false;
        $showFooter   = false;
        return view('home.locations',compact('showHeader','showFooter'));
    }

    /*
     * Set location
     */
    public function setLocation(LocationHomeRequest $request)
    {
        App::setLocale($request->input('location_code'));
        Session::put('hideLocationPage',1,Carbon::now()->addHours(23));
        return redirect('/'.$request->input('location_code'));
    }

}
