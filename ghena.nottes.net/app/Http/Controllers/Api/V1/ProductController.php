<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\Product\ProductVueTransformer;
use App\Models\Category;
use App\Models\Product;
use App\Models\Size;
use App\Models\Wishlist;
use App\Traits\APIExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;


class ProductController extends ApiController
{
    use APIExceptionTrait;

    /**
     * Show all Products filtered
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        $category    = Category::select('id','parent_id')->find($request->input('category_id'));
        $subCategory = Category::select('id','parent_id')->find($request->input('sub_category_id'));
        $CatSubCatSubSubCatIDs = $this->getCategoryIDsForSearch($category,$subCategory);

        $vars                = Cache::get('vars');
        $lng                 = App::getLocale();
        $valuta              = valutaValue($lng);
        $productList         = Product::SearchFilter($request,['cats' => $CatSubCatSubSubCatIDs,'category' => $category])->paginate(20);
        $productsResult      = $productList->toArray();
        $totalProducts       = $productsResult['total'];
        $wishlistIDs         = Wishlist::getWishlistIDs();
        $isAjax              = true;

        $product_list_area       = view("category._common_list.product_list",compact('productList','wishlistIDs','lng','vars','isAjax','valuta'))->render();
        $product_pagination_area = view("category._common_list.product_pagination",compact('productList','totalProducts','lng','vars'))->render();

        return $this->respond([
            'product_list_area' => $product_list_area,
            'product_pagination_area' => $product_pagination_area,
        ]);
    }

    /*
     * Determine selected Category if is Cat ,Sub or Sub Sub Category for serach
     * IS THE SAME LIKE ON CategoryController.php
     */
    public function getCategoryIDsForSearch($category,$subCategory = '')
    {
        $isCategory       = 0;
        $isSubCategory    = 0;
        $isSubSubCategory = 0;
        // is selected only one category
        if($subCategory == '')
        {
            // then category is like subcategory
            if($category->parent_id > 0) {
                // check it's not sub sub sub Cat
                $parentCategory = Category::select('id','parent_id')->where('id',$category->parent_id)->first();
                if(!empty($parentCategory) && $parentCategory->parent_id > 0) {
                    $isSubSubCategory = $category->id;
                } else {
                    $isSubCategory = $category->id;
                }
            } else {
                $isCategory    = $category->id;
            }
        }
        else
        {
            $parentCategory = Category::select('id','parent_id')->where('id',$subCategory->parent_id)->first();
            if(!empty($parentCategory) && $parentCategory->parent_id > 0) {
                $isSubSubCategory = $subCategory->id;
            } else {
                $isSubCategory    = $subCategory->id;
            }
        }
        return [
            'isCategory'       => $isCategory,
            'isSubCategory'    => $isSubCategory,
            'isSubSubCategory' => $isSubSubCategory,
        ];
    }

    /**
     * Add Products to wishlist list
     *
     * @return \Illuminate\Http\Response
     */
    public function addWishlist(Request $request)
    {
        Wishlist::add($request->input('product_id'));
    }

    /**
     * Remove Products from wishlist list
     *
     * @return \Illuminate\Http\Response
     */
    public function removeWishlist(Request $request)
    {
        Wishlist::remove($request->input('product_id'));
    }

    /*
     * Old for Vue
     */
    public function getListVue(Request $request)
    {
        $lng                 = App::getLocale();
        $products            = Product::SearchFilter($request)->paginate(2);
        $productsResult      = $products->toArray();
        $dataTransformer     = new ProductVueTransformer($lng);
        $productsList        = $dataTransformer->transformCollection($productsResult['data']);

        $product_list_area   = view("product._common_list.prouct-list",compact('productsList','lng','vars'))->render();

        return $this->respond([
            'currentPage' => empty($request->input('page')) ? 1 : $request->input('page'),
            'totalPages' => $productsResult['last_page'],
            'totalFoundPages' => $productsResult['total'],
            'product_list_area' => $product_list_area,
        ]);
    }

}
