<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Email;
use App\Models\Order;
use App\Models\PaymentLog;
use App\Traits\APIExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Mail;

class PaymentController extends ApiController
{
    use APIExceptionTrait;

    /**
     * Paynet callback notifaction
     *
     * @return \Illuminate\Http\Response
     */
    public function panetNotifacatios(Request $request)
    {
        $paymentInfo = file_get_contents('php://input');
        $result = json_decode($paymentInfo);
        $logs       = [];
        $ExternalID = '';
        $getOrder   = '';
        if(isset($result->EventType))
        {
            $logs['EventType']  = $result->EventType;
            // set paid order
            $ExternalID  = isset($result->Payment->ExternalId) ? $result->Payment->ExternalId : '';
            $getOrder    = Order::find($ExternalID);
            if($getOrder)
            {
                $getOrder->status  = $result->EventType == 'PAID' ? 1 : 3 ;// 1 - achitat , 2 - neachitat , 3 - canceled
                $getOrder->payNote = $result->EventType == 'PAID' ? 'Plata a fost confirmata cu succes in sistemul paynet cu orderID ='.$ExternalID : 'Plata a fost anulata in sistemul paynet cu orderID ='.$ExternalID;
                $getOrder->save();
                $logs['note']= $getOrder->payNote;
            }
            else
            {
                $logs['note'] = "The system didn't found order with id = ".$ExternalID;
            }
        }
        else
        {
            $logs['note'] = "The system didn't EventType,ExternalID - please check format";
        }

        $logs['ExternalID'] = $ExternalID;
        $logs['date']  = date('Y-m-d H:i:s');
        $logs['debug'] = $paymentInfo;
        $logs_id = PaymentLog::create($logs);

        // SEND EMAIL
        $emails = Email::get();

        if($emails) {
            $email_tpl['subject']  = 'Notificare plata , comanda cu id = '.$ExternalID;
            $email_tpl['note']     = $logs['note'];
            $email_tpl['to_email'] = env('PAYMENT_NOTIFY');
            $email_tpl['orderID']  = $ExternalID;
            $email_tpl['order']    = $getOrder;
            $email_tpl['logs_id']  = $logs_id;

            $email_tpl['to_email'] = $emails[0]->name_ro;
            $email_tpl['cc']       = Email::where('id','!=',$emails[0]->id)->pluck('name_ro')->toArray();

            Mail::send('emails.payment', $email_tpl, function ($message) use ($email_tpl) {
                $message->to([$email_tpl['to_email']]);
                if($email_tpl['cc']) {
                    $message->cc($email_tpl['cc']);
                }
                $message->subject($email_tpl['subject']);
            });
        }

    }

}