<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Location;
use App\Models\Restaurant;
use App\Models\Product;
use App\Models\Vars;
use App\Traits\APIExceptionTrait;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CartController extends ApiController
{
    use APIExceptionTrait;
    // https://laravel.com/docs/5.6/eloquent-resources
    // https://github.com/Crinsane/LaravelShoppingcart
    /**
     * Add Products to cart list
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $request)
    {
        $id      = $request->input('id');
        $product = Product::find($id);
        $lng     = App::getLocale();
        if($product && $request->input('qt') > 0) {
            Cart::add([
                'id' => $product->id,
                'price' => $product->{'price_'.$lng},
                'name' => $product->{'name_'.$lng},
                'qty' => $request->input('qt'),
                'options' => [
                    'desc' => $product->{'desc_'.$lng},
                    'image' => $product->image,
                    'slug' => $product->slug,
                    'color_id' => $request->input('color_id'),
                    'size_id' => $request->input('size_id'),
                ]
            ]);
        }
        return $this->respond(['count' => Cart::count(),'message' => 'Adaugat cu success !!!']);
    }

    /**
     * Remove Products from cart list
     *
     * @return \Illuminate\Http\Response
     */
    public function removeFromCart(Request $request)
    {
        if($request->input('rowId')) {
            Cart::remove($request->input('rowId'));
        }
    }

    /**
     * Update cart Products qt with plus
     *
     */
    public function updateCartProductQt(Request $request)
    {
        $rowId   = $request->input('rowId');
        if($rowId) {
            Cart::update($rowId, $request->input('qt'));
        }
        return $this->respond(['total' => Cart::total()]);
    }

    /**
     * Update cart Products Options ( color_id,size_id)
     *
     */
    public function updateCartProductOptions(Request $request)
    {
        $rowId   = $request->input('rowId');
        if($rowId) {
            $cart = Cart::get($rowId);
            $options = [
                'desc' => $cart->options->desc,
                'image' => $cart->options->image,
                'slug' => $cart->options->slug,
                'color_id' => $cart->options->color_id,
                'size_id' => $cart->options->size_id,
            ];
            $cartData = [
                'id' => $cart->id,
                'qty' => $cart->qty,
                'name' => $cart->name,
                'price' => $cart->price,
            ];
            $options[$request->input('optionName')] = $request->input('optionValue');

            $cartData['options'] = $options;

            $cartUpdated = Cart::update($rowId,$cartData);
            return $this->respond(['rowId' => $cartUpdated->rowId]);
        }
    }

    /**
     * Remove Products to cart list
     *
     * @return \Illuminate\Http\Response
     */
    public function removeCart(Request $request)
    {
        Compare::where(['car_id' => $request->input('car_id'),'session' => session()->getId()])->delete();
    }
}
