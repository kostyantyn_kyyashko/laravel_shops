<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\CollectionRequest;
use App\Models\Collection;
use App\Models\CollectionGallery;
use App\Services\ImageService;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class CollectionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('admin.collection.index');
    }

    public function data()
    {
        $tables = Collection::select(['id', 'name_ro']);
        return DataTables::of($tables)
                ->addColumn('action', function (Collection $tables) {
                    $html = '<a href="' . URL::to('admin/collection/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.collection.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CollectionRequest $request,ImageService $imageService)
    {
        $collection = new Collection($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $collection->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
            $collection->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $collection->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $collection->{'video_'.$lang['code']} = $request->get('video_'.$lang['code']);
        }
        $collection->onNewsFooter = $request->input('onNewsFooter');
        $collection->save();

        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($collection->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Collection::where('slug',$slug_unique)->where('id','!=',$collection->id)->first();
        $collection->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;

        // Image
        if ($request->hasFile('image'))
        {
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/collection/' .$collection->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/collection/' .$collection->id. '/' .$filename));
            $collection->image = '/uploads/collection/' .$collection->id. '/' .$filename;
            $collection->path  = '/uploads/collection/' .$collection->id. '/' .$filenameOrigin;
            $collection->ext   = $extension;
            $imageService->generateImages($collection,'collection');
        }
        $collection->save();

        if ($collection->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/collection')->with('success', trans('collection/message.success.create'));
            } else {
                return redirect('admin/collection/'.$collection->id.'/edit')->with('success', trans('collection/message.success.create'));
            }
        } else {
            return Redirect::route('admin/collection')->withInput()->with('error', trans('collection/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *\
     * @param  Collection $collection
     * @return Response
     */
    public function edit(Collection $collection)
    {
        $gallery   = CollectionGallery::where('collection_id',$collection->id)->orderBy('order','desc')->get();
        return view('admin.collection.edit', compact('collection','gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Collection $collection
     * @return Response
     */
    public function update(CollectionRequest $request,Collection $collection,ImageService $imageService)
    {
        // remove old image
        if ($request->input('delete_image') == 'on')
        {
            $img = public_path($collection->image);
            if(file_exists($img)) {
                unlink($img);
            }
            $collection->image = '';
            // delte images with extension
            $dimesion = resizeDimensionsCollection();
            foreach ($dimesion as $value) {
                $image = $collection->path.'_'.$value.'.'.$collection->ext;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
        }
        if ($request->hasFile('image1') || $request->input('delete_image1') == 'on')
        {
            if($collection->image1 != '' && file_exists(public_path($collection->image1))) {
                unlink(public_path($collection->image1));
            }
            $collection->image1 = '';
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value2) {
                $image2 = $collection->path1.'_'.$value2.'.'.$collection->ext1;
                if(file_exists(public_path($image2))) {
                    unlink(public_path($image2));
                }
            }
        }

        foreach(langs() as $key => $lang) {
            $collection->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
            $collection->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $collection->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $collection->{'video_'.$lang['code']} = $request->get('video_'.$lang['code']);
        }

        // Image
        if($request->hasFile('image'))
        {
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value) {
                $image = $collection->path.'_'.$value.'.'.$collection->ext;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }

            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/collection/' .$collection->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/collection/' .$collection->id. '/' .$filename));
            $collection->image = '/uploads/collection/' .$collection->id. '/' .$filename;
            $collection->path  = '/uploads/collection/' .$collection->id. '/' .$filenameOrigin;
            $collection->ext   = $extension;
            $imageService->generateImages($collection,'collection',$collection->id);
        }

        // Image 2
        if($request->hasFile('image1'))
        {
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value) {
                $image = $collection->path1.'_'.$value.'.'.$collection->ext1;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
            $image1       = $request->file('image1');
            $extension   = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1_resize   = Image::make($image1->getRealPath());
            $dir_path = public_path('/uploads/collection/' .$collection->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image1_resize->save(public_path('uploads/collection/' .$collection->id. '/' .$filename));
            $collection->image1 = '/uploads/collection/' .$collection->id. '/' .$filename;
            $collection->path1  = '/uploads/collection/' .$collection->id. '/' .$filenameOrigin;
            $collection->ext1   = $extension;
            $imageService->generateImages($collection,'collection',$collection->id,$collection->image1);
        }


        $collection->onNewsFooter = $request->input('onNewsFooter');
        $collection->save();

        if ($request->input('save_and_exit')) {
            return redirect('admin/collection')->with('success', trans('collection/message.success.update'));
        } else {
            return redirect('admin/collection/'.$collection->id.'/edit')->with('success', trans('collection/message.success.update'));
        }

    }


    /**
     * Remove collection.
     *
     * @param Collection $collection
     * @return Response
     */
    public function getModalDelete(Collection $collection)
    {
        $model = 'collection';
        $item  = $collection;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.collection.delete', ['id' => $collection->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('collection/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Collection $collection
     * @return Response
     */
    public function destroy(Collection $collection)
    {
        if ($collection->delete()) {
            return redirect('admin/collection')->with('success', trans('collection/message.success.destroy'));
        } else {
            return Redirect::route('admin/collection')->withInput()->with('error', trans('collection/message.error.delete'));
        }
    }

}
