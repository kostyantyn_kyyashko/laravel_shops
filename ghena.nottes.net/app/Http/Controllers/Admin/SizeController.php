<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\SizeRequest;
use App\Models\Size;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;

class SizeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.size.index');
    }

    public function data()
    {
        $tables = Size::select(['id', 'name_ro']);

        return DataTables::of($tables)
                ->addColumn('action', function (Size $tables) {
                    $html = '<a href="' . URL::to('admin/size/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.size.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.size.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SizeRequest $request)
    {
        $size = new Size($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $size->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $size->update($request->all());
        $size->save();

        if ($size->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/size')->with('success', trans('size/message.success.create'));
            } else {
                return redirect('admin/size/'.$size->id.'/edit')->with('success', trans('size/message.success.create'));
            }
        } else {
            return Redirect::route('admin/size')->withInput()->with('error', trans('size/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Size $size
     * @return Response
     */
    public function edit(Size $size)
    {
        return view('admin.size.edit', compact('size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Size $size
     * @return Response
     */
    public function update(SizeRequest $request,Size $size)
    {

        foreach(langs() as $key => $lang) {
            $size->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $size->update($request->all());

        $size->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/size')->with('success', trans('size/message.success.update'));
        } else {
            return redirect('admin/size/'.$size->id.'/edit')->with('success', trans('size/message.success.update'));
        }

    }


    /**
     * Remove size.
     *
     * @param Size $size
     * @return Response
     */
    public function getModalDelete(Size $size)
    {
        $model = 'size';
        $item  = $size;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.size.delete', ['id' => $size->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('size/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Size $size
     * @return Response
     */
    public function destroy(Size $size)
    {
        if ($size->delete()) {
            return redirect('admin/size')->with('success', trans('size/message.success.destroy'));
        } else {
            return Redirect::route('admin/size')->withInput()->with('error', trans('size/message.error.delete'));
        }
    }

}
