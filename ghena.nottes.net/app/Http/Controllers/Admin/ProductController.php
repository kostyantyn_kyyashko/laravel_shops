<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\ProdcutCategories;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\ProductSubCategory;
use App\Models\ProductSubSubCategory;
use App\Models\Size;
use App\Services\ImageService;
use App\Services\ProductService;
use App\Traits\APIExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    use APIExceptionTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.product.index');
    }

    public function data()
    {
        $tables = Product::select(['id', 'name_ro','price_ro','image'])->with(['categories']);

        return DataTables::of($tables)
                ->addColumn('action', function (Product $tables) {
                    $html = '<a href="' . URL::to('admin/product/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.product.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->addColumn('category', function (Product $tables) {
                    return implode(",",$tables->categories()->pluck('name_ro')->toArray());
                })
                ->addColumn('price', function (Product $tables) {
                    return $tables->price_ro;
                })
                ->removeColumn('id')
                ->make(true);
    }


    public function getSubCats(Request $request)
    {
        $catIDs = $request->input('catIDs');
        if($catIDs) {
            $subcategories = Category::whereIN('parent_id',$catIDs)->pluck('name_ro', 'id');
            $html = View::make('admin.product.subcats_form')->with(['subcategories' => $subcategories])->render();
        } else {
            $html = '';
        }
        return [
            'html' => $html
        ];
    }

    public function getSubSubCats(Request $request)
    {
        $catIDs = $request->input('catIDs');
        if($catIDs) {
            $subcategories = Category::whereIN('parent_id',$catIDs)->pluck('name_ro', 'id');
            $html = View::make('admin.product.subsubcats_form')->with(['subsubcategories' => $subcategories])->render();
        } else {
            $html = '';
        }
        return [
            'html' => $html
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories    = Category::whereNull('parent_id')->orWhere('parent_id',0)->pluck('name_ro', 'id');
        $sizes         = Size::pluck('name_ro', 'id');
        $colors        = Color::pluck('name_ro', 'id');
        return view('admin.product.create',compact('categories','sizes','colors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductRequest $request,ImageService $imageService,ProductService $productService)
    {
        $product = new Product($request->except('files','image'));

        $rules =  [
            'colors' => 'required',
        ];
        $messages = [
            'colors.required' => 'Culorile sunt obligatorii',
        ];
        $validator = Validator::make($request->all(), $rules,$messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $product->save();

        $productSizes = ProductSize::whereNull('product_id')->orWhere('product_id',0)->get();
        if(!$productSizes->isEmpty()) {
            foreach ($productSizes as $productsize) {
                ProductSize::where('id',$productsize->id)->update(['product_id' => $product->id]);
            }
        } else {
            $validator = ['Marimile sunt obligatorii'];
            return redirect()->back()->withInput()->withErrors($validator);
        }


        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $product->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $product->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $product->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $product->{'name3_'.$lang['code']} = $request->get('name3_'.$lang['code']);
            $product->{'price_'.$lang['code']} = $request->get('price_'.$lang['code']);
        }
        $product->update($request->all());
        $product->onMostPopularHome = $request->get('onMostPopularHome');
        $product->onLastCollectionHome = $request->get('onLastCollectionHome');
        $product->save();

        // categories
        if($request->categories) {
            $categories = $productService->listOfCategories($request);
            $product->categories()->sync($categories);
        }
        // subcategories
        if($request->subcategories) {
            $subcategories = $productService->listOfSubCategories($request);
            $product->subcategories()->sync($subcategories);
        }
        // subsubcategories
        if($request->subsubcategories) {
            $subsubcategories = $productService->listOfSubSubCategories($request);
            $product->subsubcategories()->sync($subsubcategories);
        }
        // colors
        if($request->colors) {
            $colors = $productService->listOfColors($request);
            $product->colors()->sync($colors);
        }

        $product->save();
        // Image
        if ($request->hasFile('image'))
        {
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/product/' .$product->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/product/' .$product->id. '/' .$filename));
            $product->image = '/uploads/product/' .$product->id. '/' .$filename;
            $product->path  = '/uploads/product/' .$product->id. '/' .$filenameOrigin;
            $product->ext   = $extension;
            $imageService->generateImages($product,'product',$product->id);
        }

        $this->uploadImageWithExt($request,$product, 'product', 1);
        $this->uploadImageWithExt($request,$product, 'product', 2);
        $this->uploadImageWithExt($request,$product, 'product', 3);
        $this->uploadImageWithExt($request,$product, 'product', 4);


        // Video
        if ($request->hasFile('video'))
        {
            // delte videos with extension
            foreach ($dimensionList as $value) {
                $video = $product->video;
                if(file_exists(public_path($video))) {
                    unlink(public_path($video));
                }
            }
            $video       = $request->file('video');
            $extension   = $video->getClientOriginalExtension();
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $dir_path = public_path('/uploads/product/' .$product->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $video->move(public_path('uploads/product/' .$product->id. '/' ), $filename);
            $product->video = '/uploads/product/' .$product->id. '/' .$filename;
        }

        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($product->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Product::where('slug',$slug_unique)->where('id','!=',$product->id)->first();
        $product->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;
        $product->save();



        if ($product->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/product')->with('success', trans('product/message.success.create'));
            } else {
                return redirect('admin/product/'.$product->id.'/edit')->with('success', trans('product/message.success.create'));
            }
        } else {
            return Redirect::route('admin/product')->withInput()->with('error', trans('product/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return Response
     */
    public function edit(Product $product)
    {
        $categories    = Category::whereNull('parent_id')->orWhere('parent_id',0)->pluck('name_ro', 'id');
        $subCatIDs     = $product->categories->pluck('id')->toArray();
        $subcategories = Category::whereIN('parent_id',$subCatIDs)->pluck('name_ro', 'id');

        $subsubCatIDs     = $product->subcategories->pluck('id')->toArray();

        $subsubcategories = Category::whereIN('parent_id',$subsubCatIDs)->pluck('name_ro', 'id');
        $sizes        = Size::pluck('name_ro', 'id');
        $colors       = Color::pluck('name_ro', 'id');
        $productSizes = ProductSize::where('product_id',$product->id)->with(['sizes'])->orderBy('id','desc')->get();

        return view('admin.product.edit', compact('product','productSizes','categories','subsubcategories','subcategories','sizes','colors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Product $product
     * @return Response
     */
    public function update(ProductRequest $request,Product $product,ImageService $imageService,ProductService $productService)
    {

        $rules =  [
            'colors' => 'required',
        ];
        $messages = [
            'colors.required' => 'Culorile sunt obligatorii',
        ];
        $validator = Validator::make($request->all(), $rules,$messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $productSizes = ProductSize::where('product_id',$product->id)->get();
        if($productSizes->isEmpty()) {
            $validator = ['Marimile sunt obligatorii'];
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $dimensionList = resizeProducts();

        if($request->input('delete_video') == 'on') {
            $img = public_path($product->video);
            if(file_exists($img) && $product->video) {
                unlink($img);
            }
            $product->video = '';
        }

        // remove old image
        if($request->input('delete_image') == 'on') {$this->deleteImages($product, '',$dimensionList);}
        if($request->input('delete_image1') == 'on') {$this->deleteImages($product, '1',$dimensionList);}
        if($request->input('delete_image2') == 'on') {$this->deleteImages($product, '2',$dimensionList);}
        if($request->input('delete_image3') == 'on') {$this->deleteImages($product, '3',$dimensionList);}
        if($request->input('delete_image4') == 'on') {$this->deleteImages($product, '4',$dimensionList);}

        foreach(langs() as $key => $lang) {
            $product->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $product->{'price_'.$lang['code']} = $request->get('price_'.$lang['code']);
            $product->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $product->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $product->{'name3_'.$lang['code']} = $request->get('name3_'.$lang['code']);

        }
        $product->update($request->all());


        // Image
        if ($request->hasFile('image'))
        {
            // delte images with extension
            foreach ($dimensionList as $value) {
                $image = $product->path.'_'.$value.'.'.$product->ext;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/product/' .$product->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/product/' .$product->id. '/' .$filename));
            $product->image = '/uploads/product/' .$product->id. '/' .$filename;
            $product->path  = '/uploads/product/' .$product->id. '/' .$filenameOrigin;
            $product->ext   = $extension;
            $imageService->generateImages($product,'product',$product->id);
        }

        $this->uploadImageWithExt($request,$product, 'product', 1,$dimensionList);
        $this->uploadImageWithExt($request,$product, 'product', 2,$dimensionList);
        $this->uploadImageWithExt($request,$product, 'product', 3,$dimensionList);
        $this->uploadImageWithExt($request,$product, 'product', 4,$dimensionList);

        // Video
        if ($request->hasFile('video'))
        {
            // delte videos with extension
            foreach ($dimensionList as $value) {
                $video = $product->video;
                if(file_exists(public_path($video))) {
                    unlink(public_path($video));
                }
            }
            $video       = $request->file('video');
            $extension   = $video->getClientOriginalExtension();
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $dir_path = public_path('/uploads/product/' .$product->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $video->move(public_path('uploads/product/' .$product->id. '/' ), $filename);
            $product->video = '/uploads/product/' .$product->id. '/' .$filename;
        }

        // categories
        ProdcutCategories::where('product_id',$product->id)->delete();
        if($request->categories) {
            $categories = $productService->listOfCategories($request);
            $product->categories()->sync($categories);
        }
        // subcategories
        ProductSubCategory::where('product_id',$product->id)->delete();
        if($request->subcategories) {
            $subcategories = $productService->listOfSubCategories($request);
            $product->subcategories()->sync($subcategories);
        }
        // subsubcategories
        ProductSubSubCategory::where('product_id',$product->id)->delete();

        if($request->subsubcategories) {
            $subsubcategories = $productService->listOfSubSubCategories($request);
            $product->subsubcategories()->sync($subsubcategories);
        }
        // colors
        if($request->colors) {
            $colors = $productService->listOfColors($request);
            $product->colors()->sync($colors);
        }
        $product->onLastCollectionHome = $request->get('onLastCollectionHome');
        $product->onMostPopularHome = $request->get('onMostPopularHome');
        $product->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/product')->with('success', trans('product/message.success.update'));
        } else {
            return redirect('admin/product/'.$product->id.'/edit')->with('success', trans('product/message.success.update'));
        }

    }


    /**
     * Remove product.
     *
     * @param Product $product
     * @return Response
     */
    public function getModalDelete(Product $product)
    {
        $model = 'product';
        $item  = $product;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.product.delete', ['id' => $product->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('product/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $img = public_path($product->image);
        $product_id = $product->id;
        if($product->image && file_exists($img)) {
            unlink($img);
        }
        if ($product->delete()) {
            ProductSize::where('product_id',$product_id)->delete();
            ProductColor::where('product_id',$product_id)->delete();
            ProdcutCategories::where('product_id',$product_id)->delete();
            ProductSubCategory::where('product_id',$product_id)->delete();
            ProductSubSubCategory::where('product_id',$product_id)->delete();
            return redirect('admin/product')->with('success', trans('product/message.success.destroy'));
        } else {
            return Redirect::route('admin/product')->withInput()->with('error', trans('product/message.error.delete'));
        }
    }


    /**
     * @param $request
     * @param $product
     * @param string $table
     * @param string $imageNr
     * @param string $removeOldImages = 1, list of array with dimensions
     */
    public function uploadImageWithExt($request,$product,$table = 'product',$imageNr = '',$removeOldImages = '')
    {
        $imageService =new ImageService();
        // Image
        if ($request->hasFile('image'.$imageNr))
        {
            // remove image
            if($removeOldImages) {
                $img = public_path($product->{'image'.$imageNr});
                if(file_exists($img) && $product->{'image'.$imageNr}) {
                    unlink($img);
                }
                $product->{'image'.$imageNr} = '';

                // delte images with extension
                $dimesion = $removeOldImages;
                if(is_array($dimesion)) {
                    foreach ($dimesion as $value) {
                        $image = $product->{'path'.$imageNr}.'_'.$value.'.'.$product->{'ext'.$imageNr};
                        if(file_exists(public_path($image))) {
                            unlink(public_path($image));
                        }
                    }
                }
            }
            $image       = $request->file('image'.$imageNr);
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/'.$table.'/' .$product->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/'.$table.'/' .$product->id. '/' .$filename));
            $product->{'image'.$imageNr} = '/uploads/'.$table.'/' .$product->id. '/' .$filename;
            $product->{'path'.$imageNr}  = '/uploads/'.$table.'/' .$product->id. '/' .$filenameOrigin;
            $product->{'ext'.$imageNr}   = $extension;
            $imageService->generateImages($product,$table,$product->id,$product->{'image'.$imageNr});
            $product->save();
        }
    }

    public function deleteImages($product,$imageNr = '',$dimensionsList = '')
    {
        $img = public_path($product->{'image'.$imageNr});
        if(file_exists($img) && $product->{'image'.$imageNr}) {
            unlink($img);
        }
        $product->{'image'.$imageNr} = '';

        // delte images with extension
        if($dimensionsList) {
            foreach ($dimensionsList as $value) {
                $image = $product->{'path'.$imageNr}.'_'.$value.'.'.$product->{'ext'.$imageNr};
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
                $product->{'path'.$imageNr} = '';
                $product->{'ext'.$imageNr} = '';
            }
        }
        $product->save();

    }


    public function deleteProprietes(Request $request)
    {
        $id = $request->input('id');
        $table = $request->input('table');
        switch ($table) {
            case 'product_colors' :
                ProductColor::where('id',$id)->delete();
                break;
            case 'product_soles' :
                ProductSole::where('id',$id)->delete();
                break;
            case 'product_antistatics' :
                ProductAntistatic::where('id',$id)->delete();
                break;
            case 'product_perforations' :
                ProductPerforation::where('id',$id)->delete();
                break;
            case 'product_toecaps' :
                ProductToecap::where('id',$id)->delete();
                break;
            case 'product_linings' :
                ProductLining::where('id',$id)->delete();
                break;
            case 'product_fronts' :
                ProductFront::where('id',$id)->delete();
                break;
            case 'product_typeSoles' :
                ProductTypeSole::where('id',$id)->delete();
                break;
            case 'product_sizes' :
                ProductSize::where('id',$id)->delete();
                break;
            case 'product_brants' :
                ProductBrant::where('id',$id)->delete();
                break;
        }
    }

    public function addProprietes(Request $request)
    {
        $product_id     = $request->input('product_id');
        $selected_value = $request->input('selected_value'); // selected id (color,fata,perforatie)
        $price          = $request->input('price');
        $qt             = $request->input('qt');
        $table          = $request->input('table');
        $html           = '';
        switch ($table) {
            case 'product_colors' :
                ProductColor::create([
                    'color_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productColors = ProductColor::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_colors_list',['productColors' => $productColors])->render();
                break;
            case 'product_soles' :
                ProductSole::create([
                    'sole_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productSoles = ProductSole::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_soles_list',['productSoles' => $productSoles])->render();
                break;
            case 'product_antistatics' :
                ProductAntistatic::create([
                    'antistatic_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productAntistatics = ProductAntistatic::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_antistatics_list',['productAntistatics' => $productAntistatics])->render();
                break;
            case 'product_perforations' :
                ProductPerforation::create([
                    'perforation_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productPerforations = ProductPerforation::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_perforations_list',['productPerforations' => $productPerforations])->render();
                break;
            case 'product_toecaps' :
                ProductToecap::create([
                    'toecap_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productToecaps = ProductToecap::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_toecaps_list',['productToecaps' => $productToecaps])->render();
                break;
            case 'product_linings' :
                ProductLining::create([
                    'lining_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productLinings = ProductLining::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_linings_list',['productLinings' => $productLinings])->render();
                break;
            case 'product_fronts' :
                ProductFront::create([
                    'front_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productFronts = ProductFront::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_fronts_list',['productFronts' => $productFronts])->render();
                break;
            case 'product_typeSoles' :
                ProductTypeSole::create([
                    'typeSole_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productTypeSoles = ProductTypeSole::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_typeSoles_list',['productTypeSoles' => $productTypeSoles])->render();
                break;
            case 'product_sizes' :
                ProductSize::create([
                    'size_id' => $selected_value,
                    'product_id' => $product_id,
                    'qt' => $price,
                ]);
                $productSizes = ProductSize::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_sizes_list',['productSizes' => $productSizes])->render();
                break;
            case 'product_brants' :
                ProductBrant::create([
                    'brant_id' => $selected_value,
                    'product_id' => $product_id,
                    'price' => $price,
                ]);
                $productBrants = ProductBrant::where('product_id',$product_id)->orderBy('id','desc')->get();
                $html = View::make('admin.product.product_brants_list',['productBrants' => $productBrants])->render();
                break;
        }
        return $this->respond(['html' => $html]);
    }

}
