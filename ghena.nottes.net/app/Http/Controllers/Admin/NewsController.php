<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use App\Services\ImageService;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use DOMDocument;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!Sentinel::hasAccess('news.view')) {
                Redirect::to(route('admin.permission'))->send();
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.news.index');
    }

    public function data()
    {
        $tables = News::select(['id', 'name_ro','date']);
        return DataTables::of($tables)
                ->addColumn('action', function (News $tables) {
                    $html = '<a href="' . URL::to('admin/news/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.news.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->editColumn('date', function (News $tables) {
                    return date('d.m.Y',strtotime($tables->date));
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!Sentinel::hasAccess('news.edit')) {
            Redirect::to(route('admin.permission'))->send();
        }
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
    */

    public function store(NewsRequest $request,ImageService $imageService)
    {
        $news = new News($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $news->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $news->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $news->{'short_'.$lang['code']} = $request->get('short_'.$lang['code']);
            $news->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $news->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $news->{'name3_'.$lang['code']} = $request->get('name3_'.$lang['code']);
            $news->{'name4_'.$lang['code']} = $request->get('name4_'.$lang['code']);
        }
        $news->date = Carbon::parse($request->get('date'))->format('Y-m-d');
        if($request->input('time')) {
            $news->date = $news->date.' '.$request->input('time');
        }
        $news->save();

        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($news->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = News::where('slug',$slug_unique)->where('id','!=',$news->id)->first();
        $news->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;


        // Image
        if ($request->hasFile('image'))
        {
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/news/' .$news->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/news/' .$news->id. '/' .$filename));
            $news->image = '/uploads/news/' .$news->id. '/' .$filename;
            $news->path  = '/uploads/news/' .$news->id. '/' .$filenameOrigin;
            $news->ext   = $extension;
            $imageService->generateImagesNews($news);
        }

        // Image 1
        if($request->hasFile('image1'))
        {
            $image1          = $request->file('image1');
            $extension      = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1resize   = Image::make($image1->getRealPath());
            $image1resize->save(public_path('uploads/news/'.$filename));
            $news->image1 = '/uploads/news/'.$filename;
            $news->path1 = '/uploads/news/'.$filename;

            $news->path1  = '/uploads/news/' .$filenameOrigin;
            $news->ext1   = $extension;

            $imageService->generateImagesNews($news);
        }
        $news->save();

        if ($news->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/news')->with('success', trans('news/message.success.create'));
            } else {
                return redirect('admin/news/'.$news->id.'/edit')->with('success', trans('news/message.success.create'));
            }
        } else {
            return Redirect::route('admin/news')->withInput()->with('error', trans('news/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  News $news
     * @return Response
     */
    public function edit(News $news)
    {
        if(!Sentinel::hasAccess('news.edit')) {
            Redirect::to(route('admin.permission'))->send();
        }
        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  News $news
     * @return Response
     */
    public function update(NewsRequest $request,News $news,ImageService $imageService)
    {
        // remove old image
        if ($request->hasFile('image') || $request->input('delete_image') == 'on')
        {
            if($news->image != '' && file_exists(public_path($news->image))) {
                unlink(public_path($news->image));
            }
            $news->image = '';
            // delte images with extension
            $dimesion = resizeDimensionsNews();
            foreach ($dimesion as $value) {
                $image = $news->path.'_'.$value.'.'.$news->ext;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
        }
        // remove old image 2
        if ($request->hasFile('image1') || $request->input('delete_image1') == 'on')
        {
            if($news->image1 != '' && file_exists(public_path($news->image1))) {
                unlink(public_path($news->image1));
            }
            $news->image1 = '';
            // delte images with extension
            $dimesion = resizeDimensionsNews();
            foreach ($dimesion as $value) {
                $image = $news->path1.'_'.$value.'.'.$news->ext1;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
        }

        foreach(langs() as $key => $lang) {
            $news->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $news->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $news->{'short_'.$lang['code']} = $request->get('short_'.$lang['code']);
            $news->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $news->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $news->{'name3_'.$lang['code']} = $request->get('name3_'.$lang['code']);
            $news->{'name4_'.$lang['code']} = $request->get('name4_'.$lang['code']);
        }
        $news->date = Carbon::parse($request->get('date'))->format('Y-m-d');
        if($request->input('time')) {
            $news->date = $news->date.' '.$request->input('time');
        }
        $news->save();

        // Image
        if($request->hasFile('image'))
        {
            $dimesion = resizeDimensionsNews();
            foreach ($dimesion as $value) {
                $image = $news->path.'_'.$value.'.'.$news->ext;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
            $image          = $request->file('image');
            $extension      = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/news/'.$filename));
            $news->image = '/uploads/news/'.$filename;
            $news->path = '/uploads/news/'.$filename;

            $news->path  = '/uploads/news/' .$filenameOrigin;
            $news->ext   = $extension;

            $imageService->generateImagesNews($news);
        }
        // Image 1
        if($request->hasFile('image1'))
        {
            $dimesion = resizeDimensionsNews();
            foreach ($dimesion as $value) {
                $image = $news->path1.'_'.$value.'.'.$news->ext1;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
            $image1          = $request->file('image1');
            $extension      = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1resize   = Image::make($image1->getRealPath());
            $image1resize->save(public_path('uploads/news/'.$filename));
            $news->image1 = '/uploads/news/'.$filename;
            $news->path1 = '/uploads/news/'.$filename;

            $news->path1  = '/uploads/news/' .$filenameOrigin;
            $news->ext1   = $extension;

            $imageService->generateImagesNews($news);
        }
        $news->save();

        if ($request->input('save_and_exit')) {
            return redirect('admin/news')->with('success', trans('news/message.success.update'));
        } else {
            return redirect('admin/news/'.$news->id.'/edit')->with('success', trans('news/message.success.update'));
        }

    }


    /**
     * Remove news.
     *
     * @param News $news
     * @return Response
     */
    public function getModalDelete(News $news)
    {
        $model = 'news';
        $item  = $news;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.news.delete', ['id' => $news->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('news/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  News $news
     * @return Response
     */
    public function destroy(News $news)
    {
        if ($news->delete()) {
            return redirect('admin/news')->with('success', trans('news/message.success.destroy'));
        } else {
            return Redirect::route('admin/news')->withInput()->with('error', trans('news/message.error.delete'));
        }
    }

    /**
     * Generate all type of articles images (120x80,280x200,400x400)
     * @param $news
     * @param ImageService $imageService
     */
    public function generateImages($news)
    {
        $original =  $news->image != '' ? public_path($news->image) : '';
        if($original && file_exists($original)) {

            $dimensions = resizeDimensions();
            foreach ($dimensions as $dim) {
                $dim    = explode('x',$dim);
                $width  = $dim[0];
                $height = $dim[1];
                $dest = $this->resize_image($news->image,$news->id,$width, $height, 'exact');
            }
        }
    }

}
