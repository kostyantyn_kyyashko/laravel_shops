<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\PartnerRequest;
use App\Models\Partner;
use App\Services\ImageService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class PartnerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.partner.index');
    }

    public function data()
    {
        $tables = Partner::select(['id', 'name_ro']);
        return DataTables::of($tables)
            ->addColumn('action', function (Partner $tables) {
                $html = '<a href="' . URL::to('admin/partner/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                $html.= '<a href="'.route('admin.partner.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(PartnerRequest $request,ImageService $imageService)
    {
        $partner = new Partner($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $partner->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $partner->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
        }

        $partner->save();

        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($partner->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Partner::where('slug',$slug_unique)->where('id','!=',$partner->id)->first();
        $partner->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;


        // Image
        if ($request->hasFile('image'))
        {
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/partner/' .$partner->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/partner/' .$partner->id. '/' .$filename));
            $partner->image = '/uploads/partner/' .$partner->id. '/' .$filename;
            $partner->path  = '/uploads/partner/' .$partner->id. '/' .$filenameOrigin;
            $partner->ext   = $extension;
            $imageService->generateImages($partner,'partner');
        }
        $partner->save();

        if ($partner->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/partner')->with('success', trans('partner/message.success.create'));
            } else {
                return redirect('admin/partner/'.$partner->id.'/edit')->with('success', trans('partner/message.success.create'));
            }
        } else {
            return Redirect::route('admin/partner')->withInput()->with('error', trans('partner/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Partner $partner
     * @return Response
     */
    public function edit(Partner $partner)
    {
        return view('admin.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Partner $partner
     * @return Response
     */
    public function update(PartnerRequest $request,Partner $partner,ImageService $imageService)
    {
        // remove old image
        if ($request->input('delete_image') == 'on')
        {
            delete_dir(public_path('/uploads/partner/' .$partner->id));
            $partner->image = '';
        }

        foreach(langs() as $key => $lang) {
            $partner->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
            $partner->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $partner->save();

        // Image
        if($request->hasFile('image'))
        {
            delete_dir(public_path('/uploads/partner/' .$partner->id));
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/partner/' .$partner->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/partner/' .$partner->id. '/' .$filename));
            $partner->image = '/uploads/partner/' .$partner->id. '/' .$filename;
            $partner->path  = '/uploads/partner/' .$partner->id. '/' .$filenameOrigin;
            $partner->ext   = $extension;
            $imageService->generateImages($partner,'partner');
        }
        $partner->save();

        if ($request->input('save_and_exit')) {
            return redirect('admin/partner')->with('success', trans('partner/message.success.update'));
        } else {
            return redirect('admin/partner/'.$partner->id.'/edit')->with('success', trans('partner/message.success.update'));
        }

    }


    /**
     * Remove partner.
     *
     * @param Partner $partner
     * @return Response
     */
    public function getModalDelete(Partner $partner)
    {
        $model = 'partner';
        $item  = $partner;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.partner.delete', ['id' => $partner->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('partner/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Partner $partner
     * @return Response
     */
    public function destroy(Partner $partner)
    {
        if ($partner->delete()) {
            return redirect('admin/partner')->with('success', trans('partner/message.success.destroy'));
        } else {
            return Redirect::route('admin/partner')->withInput()->with('error', trans('partner/message.error.delete'));
        }
    }

    /**
     * Generate all type of articles images (120x80,280x200,400x400)
     * @param $partner
     * @param ImageService $imageService
     */
    public function generateImages($partner)
    {
        $original =  $partner->image != '' ? public_path($partner->image) : '';
        if($original && file_exists($original)) {

            $dimensions = resizeDimensions();
            foreach ($dimensions as $dim) {
                $dim    = explode('x',$dim);
                $width  = $dim[0];
                $height = $dim[1];
                $dest = $this->resize_image($partner->image,$partner->id,$width, $height, 'exact');
            }
        }
    }

}
