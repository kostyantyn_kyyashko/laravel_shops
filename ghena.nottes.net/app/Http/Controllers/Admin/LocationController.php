<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\LocationRequest;
use App\Models\Location;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;

class LocationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.location.index');
    }

    public function data()
    {
        $tables = Location::select(['id', 'name_ro','code','short_title']);

        return DataTables::of($tables)
                ->addColumn('action', function (Location $tables) {
                    $html = '<a href="' . URL::to('admin/location/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.location.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.location.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(LocationRequest $request)
    {
        Cache::forget('navMenu');
        $location = new Location($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $location->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $location->update($request->all());

        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($location->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Location::where('slug',$slug_unique)->where('id','!=',$location->id)->first();
        $location->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$location->id;

        $location->save();

        if ($location->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/location')->with('success', trans('location/message.success.create'));
            } else {
                return redirect('admin/location/'.$location->id.'/edit')->with('success', trans('location/message.success.create'));
            }
        } else {
            return Redirect::route('admin/location')->withInput()->with('error', trans('location/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Location $location
     * @return Response
     */
    public function edit(Location $location)
    {
        return view('admin.location.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Location $location
     * @return Response
     */
    public function update(LocationRequest $request,Location $location)
    {
        Cache::forget('navMenu');
        foreach(langs() as $key => $lang) {
            $location->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $location->update($request->all());

        $location->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/location')->with('success', trans('location/message.success.update'));
        } else {
            return redirect('admin/location/'.$location->id.'/edit')->with('success', trans('location/message.success.update'));
        }

    }


    /**
     * Remove location.
     *
     * @param Location $location
     * @return Response
     */
    public function getModalDelete(Location $location)
    {
        $model = 'location';
        $item  = $location;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.location.delete', ['id' => $location->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('location/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Location $location
     * @return Response
     */
    public function destroy(Location $location)
    {
        if ($location->delete()) {
            return redirect('admin/location')->with('success', trans('location/message.success.destroy'));
        } else {
            return Redirect::route('admin/location')->withInput()->with('error', trans('location/message.error.delete'));
        }
    }

}
