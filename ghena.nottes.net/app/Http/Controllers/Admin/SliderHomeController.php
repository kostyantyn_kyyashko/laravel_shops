<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Location;
use App\Models\Product;
use App\Models\SliderHome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Intervention\Image\Facades\Image;

class SliderHomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.slider_home.index');
    }

    public function data()
    {
        $tables = SliderHome::select(['id', 'name_ro','product_id']);
        return DataTables::of($tables)
            ->addColumn('action', function (SliderHome $tables) {
                $html = '<a href="' . URL::to('admin/slider_home/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                $html.= '<a href="'.route('admin.slider_home.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                return $html;
            })
            ->addColumn('product', function (SliderHome $tables) {
                if($tables->product_id > 0) {
                    $parent = Product::where('id',$tables->product_id)->first();
                    return (empty($parent)) ? '' : $parent->name_ro;
                } else {
                    return '';
                }
            })
            ->removeColumn('parent_id')
            ->removeColumn('product_id')
            ->removeColumn('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $products = Product::pluck('name_ro', 'id');
        return view('admin.slider_home.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $slider_home = new SliderHome($request->all());
        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $slider_home->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $slider_home->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $slider_home->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
        }

        // Image
        if ($request->hasFile('image'))
        {
            $image          = $request->file('image');
            $filename       = uniqid().'.'.($image->extension()?: 'png');
            $image_resize   = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/slider_home/'.$filename));
            $slider_home->image = '/uploads/slider_home/'.$filename;
        }
        // Image 1
        if ($request->hasFile('image1'))
        {
            $image1          = $request->file('image1');
            $extension      = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1_resize   = Image::make($image1->getRealPath());

            $image1_resize->save(public_path('uploads/slider_home/' .$filename));
            $slider_home->image1 = '/uploads/slider_home/' .$filename;
        }
        // Image 2
        if ($request->hasFile('image2'))
        {
            $image2          = $request->file('image2');
            $filename       = uniqid().'.'.($image2->extension()?: 'png');
            $image2_resize   = Image::make($image2->getRealPath());
            $image2_resize->save(public_path('uploads/slider_home/'.$filename));
            $slider_home->image2 = '/uploads/slider_home/'.$filename;
        }

        $slider_home->product_id = $request->input('product_id');
        $slider_home->save();

        if ($slider_home->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/slider_home')->with('success', trans('slider_home/message.success.create'));
            } else {
                return redirect('admin/slider_home/'.$slider_home->id.'/edit')->with('success', trans('slider_home/message.success.create'));
            }
        } else {
            return Redirect::route('admin/slider_home')->withInput()->with('error', trans('slider_home/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  SliderHome $slider_home
     * @return Response
     */
    public function edit(SliderHome $slider_home)
    {
        $products = Product::pluck('name_ro', 'id');
        return view('admin.slider_home.edit', compact('slider_home','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SliderHome $slider_home
     * @return Response
     */
    public function update(Request $request,SliderHome $slider_home)
    {
        // remove old image
        if ($request->hasFile('image') || $request->input('delete_image') == 'on')
        {
            if($slider_home->image != '' && file_exists(public_path($slider_home->image))) {
                unlink(public_path($slider_home->image));
                $slider_home->image = '';
            }
        }
        if ($request->hasFile('image1') || $request->input('delete_image1') == 'on')
        {
            if($slider_home->image1 != '' && file_exists(public_path($slider_home->image1))) {
                unlink(public_path($slider_home->image1));
                $slider_home->image1 = '';
            }
        }
        if ($request->hasFile('image2') || $request->input('delete_image2') == 'on')
        {
            if($slider_home->image2 != '' && file_exists(public_path($slider_home->image2))) {
                unlink(public_path($slider_home->image2));
                $slider_home->image2 = '';
            }
        }

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $slider_home->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $slider_home->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $slider_home->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
        }

        // Image
        if ($request->hasFile('image'))
        {
            // delete old image
            if($slider_home->image) {
                $img = public_path($slider_home->image);
                if(file_exists($img)) {
                    unlink($img);
                }
            }
            $image          = $request->file('image');
            $filename       = uniqid().'.'.($image->extension()?: 'png');
            $image_resize   = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/slider_home/'.$filename));
            $slider_home->image = '/uploads/slider_home/'.$filename;
        }

        // Image 1
        if ($request->hasFile('image1'))
        {
            $image1          = $request->file('image1');
            $extension      = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1_resize   = Image::make($image1->getRealPath());

            $image1_resize->save(public_path('uploads/slider_home/' .$filename));
            $slider_home->image1 = '/uploads/slider_home/' .$filename;
        }
        // Image 2
        if ($request->hasFile('image2'))
        {
            // delete old image2
            if($slider_home->image2) {
                $img = public_path($slider_home->image2);
                if(file_exists($img)) {
                    unlink($img);
                }
            }
            $image2          = $request->file('image2');
            $filename       = uniqid().'.'.($image2->extension()?: 'png');
            $image2_resize   = Image::make($image2->getRealPath());
            $image2_resize->save(public_path('uploads/slider_home/'.$filename));
            $slider_home->image2 = '/uploads/slider_home/'.$filename;
        }

        $slider_home->product_id = $request->input('product_id');

        $slider_home->save();

        if ($request->input('save_and_exit')) {
            return redirect('admin/slider_home')->with('success', trans('slider_home/message.success.update'));
        } else {
            return redirect('admin/slider_home/'.$slider_home->id.'/edit')->with('success', trans('slider_home/message.success.update'));
        }

    }


    /**
     * Remove slider_home.
     *
     * @param SliderHome $slider_home
     * @return Response
     */
    public function getModalDelete(SliderHome $slider_home)
    {
        $model = 'slider_home';
        $item  = $slider_home;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.slider_home.delete', ['id' => $slider_home->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('slider_home/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  SliderHome $slider_home
     * @return Response
     */
    public function destroy(SliderHome $slider_home)
    {
        $img = public_path($slider_home->image);
        if($slider_home->image && file_exists($img)) {
            unlink($img);
        }
        if ($slider_home->delete()) {
            return redirect('admin/slider_home')->with('success', trans('slider_home/message.success.destroy'));
        } else {
            return Redirect::route('admin/slider_home')->withInput()->with('error', trans('slider_home/message.error.delete'));
        }
    }

}
