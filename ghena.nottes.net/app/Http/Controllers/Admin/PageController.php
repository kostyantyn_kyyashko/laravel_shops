<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\PageRequest;
use App\Models\Category;
use App\Models\Location;
use App\Models\Page;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Cache;

class PageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.page.index');
    }

    public function data()
    {
        $tables = Page::select(['id', 'name_ro','parent_id','order'])->with(['locatie']);

        return DataTables::of($tables)
                ->addColumn('action', function (Page $tables) {
                    $html = '<a href="' . URL::to('admin/page/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.page.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->addColumn('parent', function (Page $tables) {
                    if($tables->parent_id > 0) {
                        $parent = Page::where('id',$tables->parent_id)->first();
                        return (empty($parent)) ? '' : $parent->name_ro;
                    } else {
                        return '';
                    }
                })
                ->removeColumn('parent_id')
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $pages      = Page::pluck('name_ro', 'id');
        $categories = Category::pluck('name_ro', 'id');
        return view('admin.page.create',compact('pages','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PageRequest $request)
    {
        $page = new Page($request->except('files','image','image1'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $page->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $page->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $page->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $page->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $page->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $page->{'video_'.$lang['code']} = $request->get('video_'.$lang['code']);
            $page->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
        }
        $page->onFooter  = $request->get('onFooter');
        $page->onNavMenu = $request->get('onNavMenu');
        $page->parent_id = $request->get('parent_id');
        $page->isCategory = $request->get('isCategory');
        $page->haveSubmenu = $request->get('haveSubmenu');
        $page->onFooterHelpAndInformation     = $request->get('onFooterHelpAndInformation');
        $page->onFooterShop     = $request->get('onFooterShop');
        $page->category_id     = $request->get('category_id');
        $page->order = $request->get('order');

        $page->save();

        // Image
        if ($request->hasFile('image'))
        {
            $image          = $request->file('image');
            $extension      = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/page/' .$page->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/page/' .$page->id. '/' .$filename));
            $page->image = '/uploads/page/' .$page->id. '/' .$filename;
        }

        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($page->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Page::where('slug',$slug_unique)->where('id','!=',$page->id)->first();
        $page->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;
        $page->save();

        // check if have chiildrens
        if($page->parent_id) {
            $parentPage = Page::where('id',$page->parent_id)->first();
            $parentPage->have_childrens = 1;
            $parentPage->save();
        }
        // remove menu from cache
        Cache::forget('navMenu');

        if ($page->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/page')->with('success', trans('page/message.success.create'));
            } else {
                return redirect('admin/page/'.$page->id.'/edit')->with('success', trans('page/message.success.create'));
            }
        } else {
            return Redirect::route('admin/page')->withInput()->with('error', trans('page/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Page $page
     * @return Response
     */
    public function edit(Page $page)
    {
        $pages      = Page::where('id','!=',$page->id)->pluck('name_ro', 'id');
        $categories = Category::pluck('name_ro', 'id');
        return view('admin.page.edit', compact('page','pages','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Page $page
     * @return Response
     */
    public function update(PageRequest $request,Page $page)
    {
        // remove old image
        if ($request->hasFile('image') || $request->input('delete_image') == 'on')
        {
            if($page->image != '' && file_exists(public_path($page->image))) {
                unlink(public_path($page->image));
                $page->image = '';
            }
        }

        if ($request->hasFile('image1') || $request->input('delete_image1') == 'on')
        {
            if($page->image1 != '' && file_exists(public_path($page->image1))) {
                unlink(public_path($page->image1));
                $page->image1 = '';
            }
        }
        if ($request->hasFile('image2') || $request->input('delete_image2') == 'on')
        {
            if($page->image2 != '' && file_exists(public_path($page->image2))) {
                unlink(public_path($page->image2));
                $page->image2 = '';
            }
        }
        if ($request->hasFile('image3') || $request->input('delete_image3') == 'on')
        {
            if($page->image3 != '' && file_exists(public_path($page->image3))) {
                unlink(public_path($page->image3));
                $page->image3 = '';
            }
        }

        foreach(langs() as $key => $lang) {
            $page->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $page->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $page->{'text3_'.$lang['code']} = $request->get('text3_'.$lang['code']);
            $page->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $page->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $page->{'video_'.$lang['code']} = $request->get('video_'.$lang['code']);
            $page->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
        }
        $page->onNavMenu     = $request->get('onNavMenu');
        $page->onFooterHelpAndInformation     = $request->get('onFooterHelpAndInformation');
        $page->onFooterShop     = $request->get('onFooterShop');
        $page->haveSubmenu     = $request->get('haveSubmenu');
        $page->onFooter  = $request->get('onFooter');
        $page->parent_id = $request->get('parent_id');
        $page->category_id = $request->get('category_id');
        $page->order = $request->get('order');
        $page->isCategory = $request->get('isCategory');


        // Image
        if ($request->hasFile('image'))
        {
            $image          = $request->file('image');
            $extension      = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/page/' .$page->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/page/' .$page->id. '/' .$filename));
            $page->image = '/uploads/page/' .$page->id. '/' .$filename;
        }

        // Image 1
        if ($request->hasFile('image1'))
        {
            $image1          = $request->file('image1');
            $extension      = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1_resize   = Image::make($image1->getRealPath());
            $dir_path = public_path('/uploads/page/' .$page->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }

            $image1_resize->save(public_path('uploads/page/' .$page->id. '/' .$filename));
            $page->image1 = '/uploads/page/' .$page->id. '/' .$filename;
        }
        // Image 2
        if ($request->hasFile('image2'))
        {
            // delete old image2
            if($page->image2) {
                $img = public_path($page->image2);
                if(file_exists($img)) {
                    unlink($img);
                }
            }
            $image2          = $request->file('image2');
            $filename       = uniqid().'.'.($image2->extension()?: 'png');
            $dir_path = public_path('/uploads/page/' .$page->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }

            $image2_resize   = Image::make($image2->getRealPath());
            $image2_resize->save(public_path('uploads/page/'.$filename));
            $page->image2 = '/uploads/page/'.$filename;
        }
        // Image 3
        if ($request->hasFile('image3'))
        {
            // delete old image3
            if($page->image3) {
                $img = public_path($page->image3);
                if(file_exists($img)) {
                    unlink($img);
                }
            }
            $image3          = $request->file('image3');
            $filename       = uniqid().'.'.($image3->extension()?: 'png');
            $image3_resize   = Image::make($image3->getRealPath());
            $image3_resize->save(public_path('uploads/page/'.$filename));
            $page->image3 = '/uploads/page/'.$filename;
        }
        $page->save();

        $updateChildrens = 1;
        if($request->input('parent_id') == 0 && $page->parent_id > 0 ) {
            // reset for all
            $parentpage     = Page::where('id',$page->parent_id)->first();
            $haveChildrens = Page::where('parent_id',$page->parent_id)->where('id','!=',$page->id)->first();
            $parentpage->have_childrens = empty($haveChildrens) ? 0 : 1;
            $parentpage->save();
        }
        if($page->parent_id && $updateChildrens) {
            $parentpage = Page::where('id',$page->parent_id)->first();
            $parentpage->have_childrens = 1;
            $parentpage->save();
        }
        // remove menu from cache
        Cache::forget('navMenu');

        if ($request->input('save_and_exit')) {
            return redirect('admin/page')->with('success', trans('page/message.success.update'));
        } else {
            return redirect('admin/page/'.$page->id.'/edit')->with('success', trans('page/message.success.update'));
        }

    }


    /**
     * Remove page.
     *
     * @param Page $page
     * @return Response
     */
    public function getModalDelete(Page $page)
    {
        $model = 'page';
        $item  = $page;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.page.delete', ['id' => $page->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('page/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Page $page
     * @return Response
     */
    public function destroy(Page $page)
    {
        if ($page->delete()) {
            return redirect('admin/page')->with('success', trans('page/message.success.destroy'));
        } else {
            return Redirect::route('admin/page')->withInput()->with('error', trans('page/message.error.delete'));
        }
    }

}
