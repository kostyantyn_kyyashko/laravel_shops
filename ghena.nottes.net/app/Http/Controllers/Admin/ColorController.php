<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\ColorRequest;
use App\Models\Color;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;

class ColorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.color.index');
    }

    public function data()
    {
        $tables = Color::select(['id', 'name_ro']);

        return DataTables::of($tables)
                ->addColumn('action', function (Color $tables) {
                    $html = '<a href="' . URL::to('admin/color/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.color.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ColorRequest $request)
    {
        $color = new Color($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $color->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $color->update($request->all());
        $color->save();

        if ($color->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/color')->with('success', trans('color/message.success.create'));
            } else {
                return redirect('admin/color/'.$color->id.'/edit')->with('success', trans('color/message.success.create'));
            }
        } else {
            return Redirect::route('admin/color')->withInput()->with('error', trans('color/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Color $color
     * @return Response
     */
    public function edit(Color $color)
    {
        return view('admin.color.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Color $color
     * @return Response
     */
    public function update(ColorRequest $request,Color $color)
    {

        foreach(langs() as $key => $lang) {
            $color->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $color->update($request->all());

        $color->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/color')->with('success', trans('color/message.success.update'));
        } else {
            return redirect('admin/color/'.$color->id.'/edit')->with('success', trans('color/message.success.update'));
        }

    }


    /**
     * Remove color.
     *
     * @param Color $color
     * @return Response
     */
    public function getModalDelete(Color $color)
    {
        $model = 'color';
        $item  = $color;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.color.delete', ['id' => $color->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('color/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Color $color
     * @return Response
     */
    public function destroy(Color $color)
    {
        if ($color->delete()) {
            return redirect('admin/color')->with('success', trans('color/message.success.destroy'));
        } else {
            return Redirect::route('admin/color')->withInput()->with('error', trans('color/message.error.delete'));
        }
    }

}
