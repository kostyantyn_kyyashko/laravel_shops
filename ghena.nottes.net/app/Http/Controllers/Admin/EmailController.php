<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\EmailRequest;
use App\Models\Email;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;

class EmailController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.email.index');
    }

    public function data()
    {
        $tables = Email::select(['id', 'name_ro']);

        return DataTables::of($tables)
                ->addColumn('action', function (Email $tables) {
                    $html = '<a href="' . URL::to('admin/email/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.email.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.email.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(EmailRequest $request)
    {
        $email = new Email($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $email->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $email->update($request->all());
        $email->save();

        if ($email->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/email')->with('success', trans('email/message.success.create'));
            } else {
                return redirect('admin/email/'.$email->id.'/edit')->with('success', trans('email/message.success.create'));
            }
        } else {
            return Redirect::route('admin/email')->withInput()->with('error', trans('email/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Email $email
     * @return Response
     */
    public function edit(Email $email)
    {
        return view('admin.email.edit', compact('email'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Email $email
     * @return Response
     */
    public function update(EmailRequest $request,Email $email)
    {

        foreach(langs() as $key => $lang) {
            $email->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $email->update($request->all());

        $email->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/email')->with('success', trans('email/message.success.update'));
        } else {
            return redirect('admin/email/'.$email->id.'/edit')->with('success', trans('email/message.success.update'));
        }

    }


    /**
     * Remove email.
     *
     * @param Email $email
     * @return Response
     */
    public function getModalDelete(Email $email)
    {
        $model = 'email';
        $item  = $email;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.email.delete', ['id' => $email->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('email/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Email $email
     * @return Response
     */
    public function destroy(Email $email)
    {
        if ($email->delete()) {
            return redirect('admin/email')->with('success', trans('email/message.success.destroy'));
        } else {
            return Redirect::route('admin/email')->withInput()->with('error', trans('email/message.error.delete'));
        }
    }

}
