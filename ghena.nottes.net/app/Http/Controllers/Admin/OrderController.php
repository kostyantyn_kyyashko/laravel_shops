<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Order;
use App\Models\OrderProduct;
use App\User;
use App\UserAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Intervention\Image\Facades\Image;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.order.index');
    }

    public function data()
    {
        $tables = Order::select(['id','first_name','last_name','created_at','total','status','invoiceID','payType']);

        return DataTables::of($tables)
                ->addColumn('action', function (Order $tables) {
                    $html = '<a href="' . URL::to('admin/order/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.order.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';

                    return $html;
                })
                ->editColumn('fullName', function (Order $tables) {
                    return $tables->first_name.' '.$tables->last_name;
                })
                ->editColumn('date', function (Order $tables) {
                    return date('d.m.Y',strtotime($tables->created_at));
                })
                ->editColumn('status', function (Order $tables) {
                    return $tables->status == 1 ? 'Achitat' : ($tables->status == 2  ? 'NeAchitat' : 'Anulat');
                })
                ->editColumn('payType', function (Order $tables) {
                    return $tables->payType == 1 ? 'Numerar' : 'Card';
                })
                ->removeColumn('first_name')
                ->removeColumn('last_name')
                ->removeColumn('invoiceID')
                ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Order $order
     * @return Response
     */
    public function edit(Order $order)
    {
        $statusType = statusType();
        $users      = User::where('id','>',1)->pluck('email', 'id');
        $order      = Order::where('id',$order->id)->with(['products'   ])->first();
        return view('admin.order.edit', compact('order','statusType','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Order $order
     * @return Response
     */
    public function update(Request $request,Order $order)
    {
        $order->update($request->all());
        $order->status = $request->get('status');
        $order->user_id = $request->get('user_id');
        $order->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/order')->with('success', trans('order/message.success.update'));
        } else {
            return redirect('admin/order/'.$order->id.'/edit')->with('success', trans('order/message.success.update'));
        }

    }


    /**
     * Remove order.
     *
     * @param Order $order
     * @return Response
     */
    public function getModalDelete(Order $order)
    {
        $model = 'order';
        $item  = $order;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.order.delete', ['id' => $order->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('order/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Order $order
     * @return Response
     */
    public function destroy(Order $order)
    {
        $order_product = OrderProduct::where('order_id',$order->id)->get();
        if ($order->delete()) {
            if($order_product) {
                foreach ($order_product as $prod) {
                    $prod->delete();
                }
            }
            return redirect('admin/order')->with('success', trans('order/message.success.destroy'));
        } else {
            return Redirect::route('admin/order')->withInput()->with('error', trans('order/message.error.delete'));
        }
    }

}
