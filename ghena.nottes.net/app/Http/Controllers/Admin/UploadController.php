<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\BrandGallery;
use App\Models\CollectionGallery;
use App\Models\ContactGallery;
use App\Models\ProductGallery;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Traits\APIExceptionTrait;

class UploadController extends Controller
{
    use APIExceptionTrait;

    /*
     * Upload images from gallery
     * Send id and type of gallery
     */
    public function do_upload(Request $request,ImageService $imageService)
    {
        $result = [];
        $type   = $request->input('data-type');
        $id     = $request->input('id');

        $upload_path = $type;
        // Image
        if ($request->hasFile('image_do_upload'))
        {
            $image          = $request->file('image_do_upload');
            $filename       = uniqid();
            $file_ext       = ($image->extension()?: 'png');
            $filenameFull   = uniqid().'.'.($image->extension()?: 'png');
            $image_resize   = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/'.$upload_path.'/'.$filenameFull));

            switch ($type) {
                case 'collection_gallery':
                    $collection_gallery        = CollectionGallery::create(['collection_id' => $id,'name' => $filename]);
                    $collection_gallery->path  = '/uploads/'.$upload_path.'/'.$filenameFull;
                    $collection_gallery->ext   = $file_ext;
                    $collection_gallery->order = $collection_gallery->id;
                    $collection_gallery->save();
                    $result['path']    = $collection_gallery->path;
                    $result['id']      = $collection_gallery->id;
                    $result['success'] = true;
                    break;
                case 'product_gallery':
                    $product_gallery        = ProductGallery::create(['product_id' => $id,'name' => $filename]);
                    $product_gallery->path  = '/uploads/'.$upload_path.'/'.$filenameFull;
                    $product_gallery->ext   = $file_ext;
                    $product_gallery->order = $product_gallery->id;
                    $product_gallery->save();
                    $result['path']    = $product_gallery->path;
                    $result['id']      = $product_gallery->id;
                    $result['success'] = true;

                    $product_gallery->image = $product_gallery->path;
                    $imageService->generateImages($product_gallery,'product_gallery');
                    break;
                case 'contact_gallery':
                    $contact_gallery        = ContactGallery::create(['contact_id' => $id,'name' => $filename]);
                    $contact_gallery->path  = '/uploads/'.$upload_path.'/'.$filenameFull;
                    $contact_gallery->ext   = $file_ext;
                    $contact_gallery->order = $contact_gallery->id;
                    $contact_gallery->save();
                    $result['path']    = $contact_gallery->path;
                    $result['id']      = $contact_gallery->id;
                    $result['success'] = true;
                    break;
            }
        }
        echo json_encode($result);
    }

    /*
     * Reorder Image from Galleries
     * Send id and type of gallery
     * Send postions
     */
    public function reorderGallery(Request $request)
    {
        $galleryIDs = $request->input('galleryIDs');
        $type       = $request->input('data-type');

        $order = 1;
        if($galleryIDs) {
            foreach ($galleryIDs as $key => $id) {
                $id = str_replace("image_","",$id);
                switch ($type) {
                    case 'collection_gallery':
                        if($id) {
                            CollectionGallery::where('id',$id)->update(['order' => $order]);
                            $order++;
                        }
                        break;

                    case 'product_gallery':
                        if($id) {
                            ProductGallery::where('id',$id)->update(['order' => $order]);
                            $order++;
                        }
                        break;

                    case 'contact_gallery':
                        if($id) {
                            ContactGallery::where('id',$id)->update(['order' => $order]);
                            $order++;
                        }
                        break;
                }
            }
        }
        return $this->respond(['message' => 'Salvat cu success !!!']);
    }


    /**
     * Remove image from gallery
     * @param Request $request
     */
    public function removeGallery(Request $request)
    {
        $type   = $request->input('data-type');
        $id     = $request->input('id');

        switch ($type) {

            case 'product_gallery':
                $product_gallery = ProductGallery::where('id',$id)->first();
                if($product_gallery) {
                    if($product_gallery->path != '' && file_exists(public_path($product_gallery->path))) {
                        unlink(public_path($product_gallery->path));
                    }
                    delete_dir(public_path('/uploads/'.$type.'/' .$product_gallery->id));
                    $product_gallery->delete();
                }
                break;

            case 'collection_gallery':
                    $collection_gallery = CollectionGallery::where('id',$id)->first();
                    if($collection_gallery) {
                        if($collection_gallery->path != '' && file_exists(public_path($collection_gallery->path))) {
                            unlink(public_path($collection_gallery->path));
                        }
                        $collection_gallery->delete();
                    }
                break;
            case 'contact_gallery':
                $contact_gallery = ContactGallery::where('id',$id)->first();
                if($contact_gallery) {
                    if($contact_gallery->path != '' && file_exists(public_path($contact_gallery->path))) {
                        unlink(public_path($contact_gallery->path));
                    }
                    $contact_gallery->delete();
                }
                break;
        }
        echo json_encode(['message' => 'Success']);
    }
}
