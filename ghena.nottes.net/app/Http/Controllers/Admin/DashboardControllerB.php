<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Blog;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Sentinel;
use Analytics;
use View;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Charts;
use App\Datatable;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\Period;
use Illuminate\Support\Carbon;
use File;

class DashboardController extends Controller
{
    public function showHome()
    {

        // last week
        $last_week = Order::select(DB::raw( "DATE_FORMAT(created_at,'%d.%m.%Y') as day"),DB::raw( "COUNT(*) as count_row"))
                            ->where('created_at','<=',Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))
                            ->orderBy("created_at")
                            ->groupBy(DB::raw("day(created_at)"))
                            ->get()
                            ->take(7);
        $last_week_final = [];
        if($last_week) {
            foreach($last_week as $value) {
                $last_week_final[] = [$value->day,$value->count_row];
            }
        }

        // last month
        $last_month = Order::select(DB::raw( 'MONTHNAME(created_at) as date'),DB::raw( "COUNT(*) as count_row"))
                            //  ->where('created_at','<=',Carbon::now()->subDays(300)->format('Y-m-d H:i:s'))
                            ->orderBy("created_at")
                            ->groupBy(DB::raw("MONTHNAME(created_at)"))
                            ->get()
                            ->take(12);
        $last_month_final = [];
        if($last_month) {
            foreach($last_month as $value) {
                $last_month_final[] = [$value->date,$value->count_row];
            }
        }

        if(Sentinel::check())
            return view('admin.dash.index',
                [
                    'last_week'=> json_encode($last_week_final),
                    'last_month'=> json_encode($last_month_final)
                ] );
        else
            return redirect('admin/signin')->with('error', 'You must be logged in!');
    }

    public function showPersmissionDenied()
    {
        return view('admin.permission');
    }
}
