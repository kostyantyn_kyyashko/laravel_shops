<?php

namespace App\Http\Controllers\Admin;
use App\Exports\CardExport;
use App\Http\Controllers\Controller;

use App\Http\Requests\CardRequest;
use App\Imports\CardImport;
use App\Imports\CsvImport;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class CardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin.card.index');
    }

    public function export()
    {
        return Excel::download(new CardExport, 'cards.xlsx');
    }

    public function upload(Request $request)
    {
        if($request->input('ac') == 'upload') {
            if ($request->hasFile('file'))
            {
                if(file_exists(public_path('/uploads/card/tmpl.xls'))) {
                    unlink(public_path('/uploads/card/tmpl.xls'));
                }
                $file = $request->file('file');
                $file_name = 'tmpl.'.$file->getClientOriginalExtension();
                $file->move(public_path('/uploads/card'),$file_name);
                // do import
                // https://docs.laravel-excel.com/2.1/getting-started/
                Excel::import(new CsvImport, public_path('uploads/card/').$file_name);
                return redirect('admin/card')->with('success', 'Incarcarea cu success !!!');

            } else {
                return redirect('admin/card')->with('error', 'Va rugram sa alegeti un fisier');
            }
        }
        return redirect('admin/card');
    }

    public function data()
    {
        $tables = Card::select(['id', 'name_ro','barcode','discount','user_id']);

        return DataTables::of($tables)
                ->addColumn('action', function (Card $tables) {
                    $html = '<a href="' . URL::to('admin/card/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.card.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->addColumn('activated', function (Card $tables) {
                    return ($tables->user_id > 0) ? 'Da' : 'Nu';
                })
                ->removeColumn('id')
                ->removeColumn('user_id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.card.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CardRequest $request)
    {
        $card = new Card($request->except('files','image'));

        $lang['code'] = 'ro';
        $card->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        $card->update($request->all());
        $card->save();

        if ($card->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/card')->with('success', trans('card/message.success.create'));
            } else {
                return redirect('admin/card/'.$card->id.'/edit')->with('success', trans('card/message.success.create'));
            }
        } else {
            return Redirect::route('admin/card')->withInput()->with('error', trans('card/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Card $card
     * @return Response
     */
    public function edit(Card $card)
    {
        return view('admin.card.edit', compact('card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Card $card
     * @return Response
     */
    public function update(CardRequest $request,Card $card)
    {
        $lang['code'] = 'ro';
        if(empty($card->user_id)) {
            $card->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $card->update($request->all());

        $card->save();


        if ($request->input('save_and_exit')) {
            return redirect('admin/card')->with('success', trans('card/message.success.update'));
        } else {
            return redirect('admin/card/'.$card->id.'/edit')->with('success', trans('card/message.success.update'));
        }

    }


    /**
     * Remove card.
     *
     * @param Card $card
     * @return Response
     */
    public function getModalDelete(Card $card)
    {
        $model = 'card';
        $item  = $card;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.card.delete', ['id' => $card->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('card/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Card $card
     * @return Response
     */
    public function destroy(Card $card)
    {
        if ($card->delete()) {
            return redirect('admin/card')->with('success', trans('card/message.success.destroy'));
        } else {
            return Redirect::route('admin/card')->withInput()->with('error', trans('card/message.error.delete'));
        }
    }

}
