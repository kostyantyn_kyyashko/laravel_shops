<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Blog;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Sentinel;
use Analytics;
use View;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Charts;
use App\Datatable;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\Period;
use Illuminate\Support\Carbon;
use File;

class DashboardController extends Controller
{
    public function showHome()
    {
        $totalSumToday       = Order::where('date','=',Carbon::now()->format('Y-m-d'))
                                   ->where('status',1)
                                   ->sum('total');
        $totalOrderedToday   = Order::where('date','=',Carbon::now()->format('Y-m-d'))
                                   ->count();
        $totalNotPaidToday   = Order::where('date','=',Carbon::now()->format('Y-m-d'))
                                    ->where('status',2)
                                    ->count();

        $totalLast7Days         = Order::where('date','<=',Carbon::now()->subDays(7)->format('Y-m-d'))
                                    ->where('status',1)
                                    ->sum('total');
        $totalOrderedLast7Days  = Order::where('date','<=',Carbon::now()->subDays(7)->format('Y-m-d'))
                                        ->count();
        $totalNotPaidLast7Days  = Order::where('date','<=',Carbon::now()->subDays(7)->format('Y-m-d'))
                                    ->where('status',2)
                                    ->count();

        $totalFullPeriod        = Order::where('status',1)->sum('total');
        $totalOrderedFullPeriod = Order::count();
        $totalNotPaidFullPeriod = Order::where('status',2)->count();

        if(Sentinel::check())
            return view('admin.dash.index',
                [
                    'totalSumToday'=> $totalSumToday,
                    'totalOrderedToday'=> $totalOrderedToday,
                    'totalNotPaidToday'=> $totalNotPaidToday,

                    'totalLast7Days'=> $totalLast7Days,
                    'totalOrderedLast7Days'=> $totalOrderedLast7Days,
                    'totalNotPaidLast7Days'=> $totalNotPaidLast7Days,

                    'totalFullPeriod'=> $totalFullPeriod,
                    'totalOrderedFullPeriod'=> $totalOrderedFullPeriod,
                    'totalNotPaidFullPeriod'=> $totalNotPaidFullPeriod,
                ] );
        else
            return redirect('admin/signin')->with('error', 'You must be logged in!');
    }

    public function showPersmissionDenied()
    {
        return view('admin.permission');
    }
}
