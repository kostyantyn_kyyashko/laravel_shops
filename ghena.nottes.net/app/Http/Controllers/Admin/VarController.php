<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\VarRequest;
use App\Models\Vars;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Cache;

class VarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.var.index');
    }

    public function data()
    {
        $tables = Vars::select(['id', 'name_ro','var','text_ro']);
        return DataTables::of($tables)
                ->addColumn('action', function (Vars $tables) {
                    $html = '<a href="' . URL::to('admin/var/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    return $html;
                })
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.var.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(VarRequest $request)
    {
	    $var = new Vars($request->all());
        $var->var = strtolower(str_slug($request->input('name_ro'),'_'));
        $var->save();

	    // var is unique
        $checkVar = Vars::where('id','!=',$var->id)->where('var',$var->var)->first();
        if($checkVar) {
            $var->var = $var->var.'_'.$var->id;
            $var->save();
        }
        // remove  from cache
        Cache::forget('vars');

        if ($var->save()) {
            return redirect('admin/var')->with('success', trans('var/message.success.create'));
        } else {
            return Redirect::route('admin/var')->withInput()->with('error', trans('var/message.error.create'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Var $var
     * @return Response
     */
    public function edit(Vars $var)
    {
        return view('admin.var.edit', compact('var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Var $var
     * @return Response
     */
    public function update(Vars $var, VarRequest $request)
    {
        // remove  from cache
        Cache::forget('vars');
        if ($var->update($request->all())) {
            return redirect('admin/var')->with('success', trans('var/message.success.update'));
        } else {
            return Redirect::route('admin/var')->withInput()->with('error', trans('var/message.error.update'));
        }
    }


    /**
     * Remove var.
     *
     * @param Var $var
     * @return Response
     */
    public function getModalDelete(Vars $var)
    {
        $model = 'var';
        $item  = $var;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.var.delete', ['id' => $var->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('var/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Var $var
     * @return Response
     */
    public function destroy(Vars $var)
    {
        if ($var->delete()) {
            return redirect('admin/var')->with('success', trans('var/message.success.destroy'));
        } else {
            return Redirect::route('admin/var')->withInput()->with('error', trans('var/message.error.delete'));
        }
    }

}
