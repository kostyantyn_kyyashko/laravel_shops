<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Location;
use App\Models\SliderHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Intervention\Image\Facades\Image;

class SliderHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.slider_header.index');
    }

    public function data()
    {
        $tables = SliderHeader::select(['id', 'image','location_id']);

        return DataTables::of($tables)
                ->addColumn('action', function (SliderHeader $tables) {
                    $html = '<a href="' . URL::to('admin/slider_header/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.slider_header.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->addColumn('location', function (SliderHeader $tables) {
                    if($tables->location_id > 0) {
                        $parent = Location::where('id',$tables->location_id)->first();
                        return (empty($parent)) ? '' : $parent->name_ro;
                    } else {
                        return '';
                    }
                })
                ->removeColumn('location_id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $slider_headers = SliderHeader::pluck('name_ro', 'id');
        $locations = Location::pluck('name_ro', 'id');
        return view('admin.slider_header.create',compact('slider_headers','locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $slider_header = new SliderHeader($request->all());
        $slider_header->name_ro = $slider_header->id;
        $slider_header->save();
        // Image
        if ($request->hasFile('image'))
        {
            $image          = $request->file('image');
            $filename       = uniqid().'.'.($image->extension()?: 'png');
            $image_resize   = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/slider_header/'.$filename));
            $slider_header->image = '/uploads/slider_header/'.$filename;
        }
        $slider_header->save();

        if ($slider_header->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/slider_header')->with('success', trans('slider_header/message.success.create'));
            } else {
                return redirect('admin/slider_header/'.$slider_header->id.'/edit')->with('success', trans('slider_header/message.success.create'));
            }
        } else {
            return Redirect::route('admin/slider_header')->withInput()->with('error', trans('slider_header/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  SliderHeader $slider_header
     * @return Response
     */
    public function edit(SliderHeader $slider_header)
    {
        $slider_headers = SliderHeader::where('id','!=',$slider_header->id)->pluck('name_ro', 'id');
        $locations = Location::pluck('name_ro', 'id');
        return view('admin.slider_header.edit', compact('slider_header','locations','slider_headers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SliderHeader $slider_header
     * @return Response
     */
    public function update(Request $request,SliderHeader $slider_header)
    {
        // remove old image
        if ($request->input('delete_image') == 'on')
        {
            $img = public_path($slider_header->image);
            if(file_exists($img)) {
                unlink($img);
            }
            $slider_header->image = '';
        }

        // Image
        if ($request->hasFile('image'))
        {
            // delete old image
            if($slider_header->image) {
                $img = public_path($slider_header->image);
                if(file_exists($img)) {
                    unlink($img);
                }
            }
            $image          = $request->file('image');
            $filename       = uniqid().'.'.($image->extension()?: 'png');
            $image_resize   = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/slider_header/'.$filename));
            $slider_header->image = '/uploads/slider_header/'.$filename;
        }
        $slider_header->name_ro = $slider_header->id;
        $slider_header->save();

        if ($request->input('save_and_exit')) {
            return redirect('admin/slider_header')->with('success', trans('slider_header/message.success.update'));
        } else {
            return redirect('admin/slider_header/'.$slider_header->id.'/edit')->with('success', trans('slider_header/message.success.update'));
        }

    }


    /**
     * Remove slider_header.
     *
     * @param SliderHeader $slider_header
     * @return Response
     */
    public function getModalDelete(SliderHeader $slider_header)
    {
        $model = 'slider_header';
        $item  = $slider_header;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.slider_header.delete', ['id' => $slider_header->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('slider_header/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  SliderHeader $slider_header
     * @return Response
     */
    public function destroy(SliderHeader $slider_header)
    {
        $img = public_path($slider_header->image);
        if(file_exists($img)) {
            unlink($img);
        }
        if ($slider_header->delete()) {
            return redirect('admin/slider_header')->with('success', trans('slider_header/message.success.destroy'));
        } else {
            return Redirect::route('admin/slider_header')->withInput()->with('error', trans('slider_header/message.error.delete'));
        }
    }

}
