<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\RoleRequest;
use App\Models\Permission;
use App\Models\Role;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!Sentinel::getUser()->inRole('admin')) {
                Redirect::to(route('admin.permission'))->send();
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.role.index');
    }

    public function data()
    {
        $tables = Role::select(['id', 'name'])->where('id','>',1);

        return DataTables::of($tables)
                ->addColumn('action', function (Role $tables) {
                    $html = '<a href="' . URL::to('admin/role/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    $html.= '<a href="'.route('admin.role.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RoleRequest $request)
    {
        $role = new Role($request->except('files','image'));
        $role->name = $request->input('name');
        $role->slug = str_slug($role->name);
        $role->save();

        if ($role->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/role')->with('success', trans('role/message.success.create'));
            } else {
                return redirect('admin/role/'.$role->id.'/edit')->with('success', trans('role/message.success.create'));
            }
        } else {
            return Redirect::route('admin/role')->withInput()->with('error', trans('role/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     * @return Response
     */
    public function edit(Role $role)
    {
        $rolesByID = Sentinel::findRoleById($role->id);
        $role_permissions = $rolesByID->permissions;
        $permissions = Permission::get();
        return view('admin.role.edit', compact('role','permissions','role_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Role $role
     * @return Response
     */
    public function update(RoleRequest $request,Role $role)
    {
        $rolesByID = Sentinel::findRoleById($role->id);
        $role_permissions = $rolesByID->permissions;

        $role->name = $request->input('name');
        $role->save();

        $permissions = Permission::get();
        foreach ($permissions as $value) {
            // View
            $perm       = $value->slug.'.view';
            $perm_value = $request->get($value->slug.'_view') == 'on' ? true : false;
            if(isset($role_permissions[$perm])) {
                $rolesByID->updatePermission($perm,$perm_value);
            } else {
                $rolesByID->addPermission($perm,$perm_value);
            }
            // Edit
            $perm       = $value->slug.'.edit';
            $perm_value = $request->get($value->slug.'_edit') == 'on' ? true : false;
            if(isset($role_permissions[$perm])) {
                $rolesByID->updatePermission($perm,$perm_value);
            } else {
                $rolesByID->addPermission($perm,$perm_value);
            }
            // Delete
            $perm       = $value->slug.'.delete';
            $perm_value = $request->get($value->slug.'_delete') == 'on' ? true : false;
            if(isset($role_permissions[$perm])) {
                $rolesByID->updatePermission($perm,$perm_value);
            } else {
                $rolesByID->addPermission($perm,$perm_value);
            }
        }
        $rolesByID->save();

        if ($request->input('save_and_exit')) {
            return redirect('admin/role')->with('success', trans('role/message.success.update'));
        } else {
            return redirect('admin/role/'.$role->id.'/edit')->with('success', trans('role/message.success.update'));
        }

    }


    /**
     * Remove role.
     *
     * @param Role $role
     * @return Response
     */
    public function getModalDelete(Role $role)
    {
        $model = 'role';
        $item  = $role;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.role.delete', ['id' => $role->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('role/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role $role
     * @return Response
     */
    public function destroy(Role $role)
    {
        $img = public_path($role->image);
        if(file_exists($img)) {
            unlink($img);
        }
        if ($role->delete()) {
            return redirect('admin/role')->with('success', trans('role/message.success.destroy'));
        } else {
            return Redirect::route('admin/role')->withInput()->with('error', trans('role/message.error.delete'));
        }
    }

}
