<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Services\ImageService;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    public function data()
    {
        $tables = Category::select(['id', 'name_ro','parent_id','order']);

        return DataTables::of($tables)
                ->addColumn('action', function (Category $tables) {
                    $html = '<a href="' . URL::to('admin/category/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>'.trans('general.edit').'</a>&nbsp;&nbsp;&nbsp;';
                    if($tables->id != 7) {
                        $html.= '<a href="'.route('admin.category.confirm-delete', $tables->id).'" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>'.trans('general.delete').'
                              </a>';
                    }

                    return $html;
                })
                ->addColumn('parent', function (Category $tables) {
                    if($tables->parent_id > 0) {
                        $parent = Category::where('id',$tables->parent_id)->first();
                        return (empty($parent)) ? '' : $parent->name_ro;
                    } else {
                        return '';
                    }
                })
                ->removeColumn('parent_id')
                ->removeColumn('id')
                ->rawColumns(['name_ro','action'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categorys = Category::pluck('name_ro', 'id');
        $products = Product::pluck('name_ro', 'id');
        $pages      = Page::where('parent_id',0)->orWhereNull('parent_id')->pluck('name_ro', 'id');
        return view('admin.category.create',compact('categorys','products','pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoryRequest $request,ImageService $imageService)
    {
        $category = new Category($request->except('files','image','image1','image2'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $category->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $category->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $category->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $category->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
            $category->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
            $category->{'price_'.$lang['code']} = $request->get('price_'.$lang['code']);
        }
        $category->parent_id = $request->get('parent_id');
        $category->order = $request->get('order');
        $category->page_id = $request->get('page_id');

        $category->save();

        $category->update($request->all());

        // Image
        if($request->hasFile('image'))
        {
            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/category/' .$category->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/category/' .$category->id. '/' .$filename));
            $category->image = '/uploads/category/' .$category->id. '/' .$filename;
            $category->path  = '/uploads/category/' .$category->id. '/' .$filenameOrigin;
            $category->ext   = $extension;
            $imageService->generateImages($category,'category',$category->id);
        }

        // Image 2
        if($request->hasFile('image1'))
        {
            $image1       = $request->file('image1');
            $extension   = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1_resize   = Image::make($image1->getRealPath());
            $dir_path = public_path('/uploads/category/' .$category->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image1_resize->save(public_path('uploads/category/' .$category->id. '/' .$filename));
            $category->image1 = '/uploads/category/' .$category->id. '/' .$filename;
            $category->path1  = '/uploads/category/' .$category->id. '/' .$filenameOrigin;
            $category->ext1   = $extension;
            $imageService->generateImages($category,'category',$category->id,$category->image1);
        }

        // Image 3
        if($request->hasFile('image2'))
        {
            $image2       = $request->file('image2');
            $extension   = $image2->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image2_resize   = Image::make($image2->getRealPath());
            $dir_path = public_path('/uploads/category/' .$category->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image2_resize->save(public_path('uploads/category/' .$category->id. '/' .$filename));
            $category->image2 = '/uploads/category/' .$category->id. '/' .$filename;
            $category->path2  = '/uploads/category/' .$category->id. '/' .$filenameOrigin;
            $category->ext2   = $extension;
            $imageService->generateImages($category,'category',$category->id,$category->image2);
        }


        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($category->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Category::where('slug',$slug_unique)->where('id','!=',$category->id)->first();
        $category->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;
        $category->save();

        // check if have chiildrens
        if($category->parent_id) {
            $parentCategory = Category::where('id',$category->parent_id)->first();
            $parentCategory->have_childrens = 1;
            $parentCategory->save();
        }
        // remove menu from cache
        Cache::forget('navMenu');

        if ($category->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/category')->with('success', trans('category/message.success.create'));
            } else {
                return redirect('admin/category/'.$category->id.'/edit')->with('success', trans('category/message.success.create'));
            }
        } else {
            return Redirect::route('admin/category')->withInput()->with('error', trans('category/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        $categorys  = Category::where('id','!=',$category->id)->pluck('name_ro', 'id');
        $products   = Product::pluck('name_ro', 'id');
        $pages      = Page::where('parent_id',0)->orWhereNull('parent_id')->pluck('name_ro', 'id');
        return view('admin.category.edit', compact('category','categorys','products','pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function update(CategoryRequest $request,Category $category,ImageService $imageService)
    {
        // remove old image
        if ($request->hasFile('image') || $request->input('delete_image') == 'on')
        {
            if($category->image != '' && file_exists(public_path($category->image))) {
                unlink(public_path($category->image));
            }
            $category->image = '';
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value1) {
                $image1 = $category->path.'_'.$value1.'.'.$category->ext;
                if(file_exists(public_path($image1))) {
                    unlink(public_path($image1));
                }
            }
        }
        if ($request->hasFile('image1') || $request->input('delete_image1') == 'on')
        {
            if($category->image1 != '' && file_exists(public_path($category->image1))) {
                unlink(public_path($category->image1));
            }
            $category->image1 = '';
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value2) {
                $image2 = $category->path1.'_'.$value2.'.'.$category->ext1;
                if(file_exists(public_path($image2))) {
                    unlink(public_path($image2));
                }
            }
        }
        if ($request->hasFile('image2') || $request->input('delete_image2') == 'on')
        {
            if($category->image2 != '' && file_exists(public_path($category->image2))) {
                unlink(public_path($category->image2));
            }
            $category->image2 = '';
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value3) {
                $image3 = $category->path2.'_'.$value3.'.'.$category->ext2;
                if(file_exists(public_path($image3))) {
                    unlink(public_path($image3));
                }
            }
        }

        foreach(langs() as $key => $lang) {
            $category->{'text_'.$lang['code']} = $request->get('text_'.$lang['code']);
            $category->{'text2_'.$lang['code']} = $request->get('text2_'.$lang['code']);
            $category->{'link_'.$lang['code']} = $request->get('link_'.$lang['code']);
            $category->{'price_'.$lang['code']} = $request->get('price_'.$lang['code']);
            $category->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
            $category->{'name2_'.$lang['code']} = $request->get('name2_'.$lang['code']);
        }
        $category->parent_id = $request->get('parent_id');
        $category->order = $request->get('order');
        $category->page_id = $request->get('page_id');
        $category->update($request->all());

        // Image
        if($request->hasFile('image'))
        {
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value) {
                $image = $category->path.'_'.$value.'.'.$category->ext;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }

            $image       = $request->file('image');
            $extension   = $image->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image_resize   = Image::make($image->getRealPath());
            $dir_path = public_path('/uploads/category/' .$category->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image_resize->save(public_path('uploads/category/' .$category->id. '/' .$filename));
            $category->image = '/uploads/category/' .$category->id. '/' .$filename;
            $category->path  = '/uploads/category/' .$category->id. '/' .$filenameOrigin;
            $category->ext   = $extension;
            $imageService->generateImages($category,'category',$category->id);
        }

        // Image 2
        if($request->hasFile('image1'))
        {
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value) {
                $image = $category->path1.'_'.$value.'.'.$category->ext1;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
            $image1       = $request->file('image1');
            $extension   = $image1->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image1_resize   = Image::make($image1->getRealPath());
            $dir_path = public_path('/uploads/category/' .$category->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image1_resize->save(public_path('uploads/category/' .$category->id. '/' .$filename));
            $category->image1 = '/uploads/category/' .$category->id. '/' .$filename;
            $category->path1  = '/uploads/category/' .$category->id. '/' .$filenameOrigin;
            $category->ext1   = $extension;
            $imageService->generateImages($category,'category',$category->id,$category->image1);
        }

        // Image 3
        if($request->hasFile('image2'))
        {
            // delte images with extension
            $dimesion = resizeDimensionsCategory();
            foreach ($dimesion as $value) {
                $image = $category->path2.'_'.$value.'.'.$category->ext2;
                if(file_exists(public_path($image))) {
                    unlink(public_path($image));
                }
            }
            $image2       = $request->file('image2');
            $extension   = $image2->extension()?: 'png';
            $filenameOrigin = uniqid();
            $filename       = $filenameOrigin.'.'.$extension;
            $image2_resize   = Image::make($image2->getRealPath());
            $dir_path = public_path('/uploads/category/' .$category->id);
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
            $image2_resize->save(public_path('uploads/category/' .$category->id. '/' .$filename));
            $category->image2 = '/uploads/category/' .$category->id. '/' .$filename;
            $category->path2  = '/uploads/category/' .$category->id. '/' .$filenameOrigin;
            $category->ext2   = $extension;
            $imageService->generateImages($category,'category',$category->id,$category->image2);
        }

        $category->save();

        $updateChildrens = 1;
        if($request->input('parent_id') == 0 && $category->parent_id > 0 ) {
            // reset for all
            $parentcategory     = Category::where('id',$category->parent_id)->first();
            $haveChildrens = Category::where('parent_id',$category->parent_id)->where('id','!=',$category->id)->first();
            $parentcategory->have_childrens = empty($haveChildrens) ? 0 : 1;
            $parentcategory->save();
        }
        if($category->parent_id && $updateChildrens) {
            $parentcategory = Category::where('id',$category->parent_id)->first();
            $parentcategory->have_childrens = 1;
            $parentcategory->save();
        }
        // remove menu from cache
        Cache::forget('navMenu');

        if ($request->input('save_and_exit')) {
            return redirect('admin/category')->with('success', trans('category/message.success.update'));
        } else {
            return redirect('admin/category/'.$category->id.'/edit')->with('success', trans('category/message.success.update'));
        }

    }



    /**
     * Remove category.
     *
     * @param Category $category
     * @return Response
     */
    public function getModalDelete(Category $category)
    {
        $model = 'category';
        $item  = $category;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.category.delete', ['id' => $category->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('category/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        if ($category->delete()) {
            return redirect('admin/category')->with('success', trans('category/message.success.destroy'));
        } else {
            return Redirect::route('admin/category')->withInput()->with('error', trans('category/message.error.delete'));
        }
    }

}
