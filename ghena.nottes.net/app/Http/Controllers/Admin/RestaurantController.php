<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\RestaurantRequest;
use App\Models\Restaurant;
use App\Models\RestaurantGallery;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Redirect;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class RestaurantController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Sentinel::hasAccess('restaurant.view')) {
                Redirect::to(route('admin.permission'))->send();
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.restaurant.index');
    }

    public function data()
    {
        $tables = Restaurant::select(['id', 'name_ro']);

        return DataTables::of($tables)
                ->addColumn('action', function (Restaurant $tables) {
                    $html = '';
                    if(Sentinel::hasAccess('restaurant.edit')) {
                        $html.= '<a href="' . URL::to('admin/restaurant/' . $tables->id . '/edit') . '" class="btn btn btn-primary btn-sm btn-sm-table"><i class="fa fa-edit"></i>' . trans('general.edit') . '</a>&nbsp;&nbsp;&nbsp;';
                    }
                    if(Sentinel::hasAccess('restaurant.delete')) {
                        $html.= '<a href="' . route('admin.restaurant.confirm-delete', $tables->id) . '" 
                                    class="btn btn btn-danger btn-sm delete-modal btn-sm-table"
                                    data-toggle="modal" data-target="#delete_confirm">
                                    <i class="fa fa-trash-o"></i>' . trans('general.delete') . '
                              </a>';
                    }
                    return $html;
                })
                ->removeColumn('id')
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!Sentinel::hasAccess('restaurant.edit')) {
            Redirect::to(route('admin.permission'))->send();
        }
        $restaurants = Restaurant::pluck('name_ro', 'id');
        return view('admin.restaurant.create',compact('restaurants'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RestaurantRequest $request)
    {
        $restaurant = new Restaurant($request->except('files','image'));

        foreach(langs() as $key => $lang) {
            if($key == 0) {
                $first_lang = $lang['code'];
            }
            $restaurant->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }

        $this->generateSlugs($restaurant,$request,$first_lang);

        $restaurant->save();

        $restaurant->update($request->all());

        if ($restaurant->id) {
            if ($request->input('save_and_exit')) {
                return redirect('admin/restaurant')->with('success', trans('restaurant/message.success.create'));
            } else {
                return redirect('admin/restaurant/'.$restaurant->id.'/edit')->with('success', trans('restaurant/message.success.create'));
            }
        } else {
            return Redirect::route('admin/restaurant')->withInput()->with('error', trans('restaurant/message.error.create'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Restaurant $restaurant
     * @return Response
     */
    public function edit(Restaurant $restaurant)
    {
        if(!Sentinel::hasAccess('restaurant.edit')) {
            Redirect::to(route('admin.permission'))->send();
        }
        $gallery = RestaurantGallery::where('restaurant_id',$restaurant->id)->orderBy('order','asc')->get();
        $restaurants  = Restaurant::where('id','!=',$restaurant->id)->pluck('name_ro', 'id');
        return view('admin.restaurant.edit', compact('restaurant','gallery','restaurants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Restaurant $restaurant
     * @return Response
     */
    public function update(RestaurantRequest $request,Restaurant $restaurant)
    {
        foreach(langs() as $key => $lang) {
            $restaurant->{'name_'.$lang['code']} = $request->get('name_'.$lang['code']);
        }
        $restaurant->save();

        $restaurant->update($request->all());

        if ($request->input('save_and_exit')) {
            return redirect('admin/restaurant')->with('success', trans('restaurant/message.success.update'));
        } else {
            return redirect('admin/restaurant/'.$restaurant->id.'/edit')->with('success', trans('restaurant/message.success.update'));
        }

    }


    /**
     * Remove restaurant.
     *
     * @param Restaurant $restaurant
     * @return Response
     */
    public function getModalDelete(Restaurant $restaurant)
    {
        $model = 'restaurant';
        $item  = $restaurant;
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.restaurant.delete', ['id' => $restaurant->id]);
            return view('admin.layouts.modal_confirmation', compact('item','error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {
            $error = trans('restaurant/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error','item_mame', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Restaurant $restaurant
     * @return Response
     */
    public function destroy(Restaurant $restaurant)
    {
        if ($restaurant->delete()) {
            return redirect('admin/restaurant')->with('success', trans('restaurant/message.success.destroy'));
        } else {
            return Redirect::route('admin/restaurant')->withInput()->with('error', trans('restaurant/message.error.delete'));
        }
    }

    /**
     * Generate short unique slug
     *
     * @param $restaurant
     * @param $request
     * @param $first_lang
     */
    public function generateSlugs($restaurant,$request,$first_lang)
    {
        // Save Slug
        $slug_unique   = $request->get('slug') == '' ? str_slug($restaurant->{'name_'.$first_lang}) : $request->get('slug');
        $checkSlug     = Restaurant::where('slug',$slug_unique)->where('id','!=',$restaurant->id)->first();
        $restaurant->slug = empty($checkSlug) ? $slug_unique : $slug_unique.'-'.$checkSlug->id;
        $restaurant->save();
    }

}
