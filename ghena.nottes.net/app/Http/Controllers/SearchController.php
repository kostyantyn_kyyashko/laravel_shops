<?php

namespace App\Http\Controllers;

use App\Services\SearchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SearchController extends AppController
{

    /**
     * Show the application search result from full site.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $s             = $request->input('s');
        $page          = $request->input('page');
        $searchService = new SearchService();
        $productList   = $searchService->search($request,App::getLocale(),$page);
        $totalProducts = !empty($productList) ? count($productList) : 0;
        return view('search.list',compact('productList','s','totalProducts'));
    }

}
