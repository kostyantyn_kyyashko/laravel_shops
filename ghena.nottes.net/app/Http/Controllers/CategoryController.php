<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Color;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Size;
use Illuminate\Http\Request;


class CategoryController extends AppController
{
    /**
     * Show Full List Women/Boy/Grios
     *
     */
    public function list($slug,$slugSubCat = '',Request $request)
    {

        $category    = Category::where('slug',$slug)->first();
        $page        = $category;
        if(empty($category)) {
            return redirect('/');
        }
        // acitve header menu
        $pageActive     = Page::where('category_id',$category->id)->first();
        $pageActive     = empty($pageActive) ? Page::where('slug',$category->slug)->first() : $pageActive;
        $activeMenu     = !empty($pageActive) ? $pageActive->id : '';

        $subCategories  = Category::where('parent_id',$category->id)->get();

        $collection     = collect($subCategories);
        $modelList      = $collection->filter(function ($value) {
                if($value->isCollection != 1) {
                    return $value;
                }
        });
        $collectionList = $collection->filter(function ($value) {
            if($value->isCollection == 1) {
                return $value;
            }
        });
        $subCategory           = !empty($slugSubCat) ? Category::where('slug',$slugSubCat)->first():'';
        $categoryTitle         = !empty($subCategory) ? $subCategory->{'name_'.$this->lng} : $category->{'name_'.$this->lng};
        $CatSubCatSubSubCatIDs = $this->getCategoryIDsForSearch($category,$subCategory);
//        dd($CatSubCatSubSubCatIDs);
        $productList    = Product::SearchFilter($request,['cats' => $CatSubCatSubSubCatIDs,'category' => $category])->paginate(10);
        $productsResult = $productList->toArray();
        $totalProducts  = $productsResult['total'];
        $sizeList       = Size::get();
        $colorList      = Color::get();

        return view('category.list',compact(
                'productList',
                'page',
                'sizeList',
                'modelList',
                'activeMenu',
                'collectionList',
                'colorList',
                'totalProducts',
                'categoryTitle',
                'subCategories',
                'subCategory',
                'category'
            )
        );
    }

    /*
     * Determine selected Category if is Cat ,Sub or Sub Sub Category for serach
     */
    public function getCategoryIDsForSearch($category,$subCategory = '')
    {
        $isCategory       = 0;
        $isSubCategory    = 0;
        $isSubSubCategory = 0;
        // is selected only one category
        if($subCategory == '')
        {
            // then category is like subcategory
            if($category->parent_id > 0) {
                // check it's not sub sub sub Cat
                $parentCategory = Category::select('id','parent_id')->where('id',$category->parent_id)->first();
                if(!empty($parentCategory) && $parentCategory->parent_id > 0) {
                    $isSubSubCategory = $category->id;
                } else {
                    $isSubCategory = $category->id;
                }
            } else {
                $isCategory    = $category->id;
            }
        }
        else
        {
            $parentCategory = Category::select('id','parent_id')->where('id',$subCategory->parent_id)->first();
            if(!empty($parentCategory) && $parentCategory->parent_id > 0) {
                $isSubSubCategory = $subCategory->id;
            } else {
                $isSubCategory    = $subCategory->id;
            }
        }
        return [
            'isCategory'       => $isCategory,
            'isSubCategory'    => $isSubCategory,
            'isSubSubCategory' => $isSubSubCategory,
        ];
    }

    /**
     * Show products like buy the look
     *
     */
    public function productsByTheLook($slug)
    {
        $category    = Category::where('slug',$slug)->first();
        $page        = $category;
        if(empty($category)) {
            return redirect('/');
        }
        $productList = Product::WithSubCats([$category->id])->get();
        return view('category.buy_the_look',compact('productList','page','category'));
    }


    /**
     * Show School Uniform
     *
     */
    public function schoolUniform()
    {
        $category    = Category::find(25);
        $page        = $category;
        if(empty($category)) {
            return redirect('/');
        }
        $categoryList = Category::where('parent_id',$category->id)->get();
        $partners     = Partner::get();
        $activeMenu  = 6;
        return view('category.school_uniform',compact('activeMenu','categoryList','partners','page','category'));
    }

    /**
     * Show School Uniform Detail
     *
     */
    public function schoolUniformDetail($slug)
    {
        $category    = Category::where('slug',$slug)->first();
        $page        = $category;
        if(empty($category)) {
            return redirect('/');
        }
        $partners     = Partner::get();
        $categoryList = Category::where('parent_id',$category->id)->get();
        return view('category.school_uniform_detail',compact('partners','categoryList','page','category'));
    }

}
