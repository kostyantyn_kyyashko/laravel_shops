<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Newsletter;

class SubscribeController extends AppController
{

    /**
     * Abonare la noutati
     *
     * @param Request $request
     * @return array
     */
    public function add(Request $request)
    {
        $email = $request->input('email');
        $validator = Validator::make($request->all(),['email' => 'email']);
        if($validator->fails())
        {
            $message = $this->vars['emailul_trebuie_sa_fie_valid'];
        }
        else
        {
            $isSubscribed = Newsletter::isSubscribed($email);
            if($isSubscribed)
            {
                $message = $this->vars['acest_email_exisa_deja_va_rugam_sa_introduceti_altul'];
            }
            else
            {
                Newsletter::subscribe($email);
                $message = $this->vars['abonarea_a_avut_loc_cu_success'];
            }
        }
        return ['message' => $message];
    }

    public function feed()
    {
        /* create new feed */
        $feed = App::make("feed");

        $articles = News::select('name_ro as title','text_ro as description','text_ro as content','date','slug')->Active()->take(20)->get();
        $feed->title = 'VIVI';
        $feed->description = 'Portalul avocaturii de afaceri din Moldova';
        $feed->link = env('APP_URL');
        $feed->logo = url('assets/images/main-logo.png');
        $feed->date = url('feed');
        $feed->setDateFormat('datetime');
        $feed->pubdate = $articles[0]->date;
        $feed->lang = 'en';
        $feed->setShortening(true);
        $feed->setTextLimit(100);

        foreach ($articles as $article)
        {
            $feed->add($article->title,'DAAC-HERMES', URL::to($article->slug), $article->date, $article->description, $article->content);

        }

        return $feed->render('rss');
    }

}
