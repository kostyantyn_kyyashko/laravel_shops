<?php

namespace App\Http\Controllers;

use App\Models\CollectionGallery;
use App\Models\Page;
use App\Models\Collection;
use Illuminate\Http\Request;

class CollectionController extends AppController
{
    /**
     * Show list of collection
     *
     * @return \Illuminate\View\View
     */
    public function list()
    {
        $page           = Page::find(2);
        $activeMenu     = 2;
        $collectionList = Collection::get();
        $showFooter     = 0;
        return view('collection.list',compact('collectionList','activeMenu','page','showFooter'));
    }

    /**
     * Show Collection detail
     *
     */
    public function detail($slug)
    {
        $collection   = Collection::where('slug',$slug)->first();
        $page         = $collection;
        if(empty($collection)) {
            return redirect(route('collection'));
        }
        $collectionList    = Collection::get();
        $collectionGallery = CollectionGallery::where('collection_id',$collection->id)->paginate(10);
        $activeMenu        = 2;
        return view('collection.detail',compact('collection','page','activeMenu','collectionList','collectionGallery'));
    }
}
