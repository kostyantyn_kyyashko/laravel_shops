<?php

namespace App\Http\Controllers;

use App\Models\Product;

class ProductController extends AppController
{
    /**
     * Show Product detail
     *
     * @return \Illuminate\View\View
     */
    public function detail($slug)
    {
        $product   = Product::where('slug',$slug)->first();
        if(empty($product)) {
            return redirect('/');
        }
        $isDetailProduct = true;
        $value           = $product;
        return view('product.detail',compact('product','value','isDetailProduct'));
    }

}
