<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\AppController;
use App\User;
use Illuminate\Support\Facades\Cache;
use Sentinel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends AppController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
            'terms' => 'accepted'
        ]);
    }

    protected function validatorDadmin(array $data)
    {
        $vars = Cache::get('vars');
        $messages = [
            'first_name.required' => $vars['numele_este_obligatoriu'],
            'last_name.required' => $vars['prenumele_este_obligatoriu'],
            'email.required' => $vars['email_este_obligatoriu'],
            'email.email' => $vars['emailul_trebuie_sa_fie_valid'],
            'email.unique' => $vars['acest_email_exisa_deja_va_rugam_sa_introduceti_altul'],
            'password_reg.required' => $vars['parola_repetata_trebuie_sa_coincida'],
            'password_reg2.required' => $vars['parola_repetata_trebuie_sa_coincida'],
            'password.required' => $vars['parola_este_obligatorie'],
            'password.confirmed' => $vars['parola_repetata_trebuie_sa_coincida'],
            'password.min' => $vars['parola_trebuie_sa_contina_minim_6_caractere'],
            'dob.required' => $vars['data_de_nastere_este_obligatorie'],
            'idnp.required' => $vars['id_personal_este_obligatoriu'],
            'phone.required' => $vars['telefonul_este_obligatoriu'],
            'terms.accepted' => $vars['termeni_si_conditii_sunt_obligatorii'],
            'idnp.digits' => $vars['id_personal_trebuie_sa_contina_13_cifre'],
            'recaptcha' => $vars['recaptcha_este_obligatorie'],
        ];
        $validator = Validator::make($data, [
            'g-recaptcha-response' => 'recaptcha',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'dob' => 'required|string|max:15',
            'idnp' => 'required|integer|digits:13',
            'phone' => 'required|string|max:100',
            'terms' => 'accepted'

        ],$messages);

        return $validator;

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'role_id' => 2,// User
            'person_type' => $data['person_type'],// 1  Persoană fizică , 2 socieatea
            'address1' => $data['address1'],
            'address2' => $data['address2'],
            'postal_code' => $data['postal_code'],
            'location' => $data['location'],
            'district' => $data['district'],
            'country' => $data['country'],
            'phone' => $data['phone'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        // add user to 'User' group
        $role = Sentinel::findRoleById(2);
        if ($role) {
            $role->users()->attach($user);
        }
        return $user;
    }
}
