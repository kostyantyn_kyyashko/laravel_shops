<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkWithUsRequest;
use App\Models\Email;
use App\Models\Page;
use App\Models\Partner;
use App\Models\WorkWithUs;
use Illuminate\Support\Facades\Mail;


class PageController extends AppController
{
    /**
     * Show About Us page
     *
     */
    public function about()
    {
        $page       = Page::find(13);
        $director   = Page::find(8);
        $collection = Page::find(9);
        $activeMenu = $page->id;
        return view('page.about',compact('page','collection','director','activeMenu'));
    }

    /**
     * Show b2b page
     *
     */
    public function b2b()
    {
        $page       = Page::find(7);
        $category   = $page;
        $b2bList    = Page::where('parent_id',$page->id)->get();
        $activeMenu = $page->id;
        $partners   = Partner::get();
        return view('page.b2b',compact('page','partners','category','b2bList','activeMenu'));
    }


    /**
     * Detail Page
     *
     * @return \Illuminate\View\View
     */
    public function detail($slug)
    {
        $page   = Page::where('slug',$slug)->first();
        if(empty($page)) {
            return redirect('/');
        }
        $activeMenu = $page->id;
        return view('page.detail',compact('page','activeMenu'));
    }

    /**
     * Send work with us
     *
     */
    public function sendWorkWithUs(WorkWithUsRequest $request)
    {
        $data = [
            'name'  =>  $request->input('name'),
            'company'  =>  $request->input('company'),
            'subject'  =>  $request->input('subject'),
            'message'  =>  $request->input('message'),
            'msg'  =>  $request->input('message'),
            'email'  =>  $request->input('email')
        ];
        WorkWithUs::create($data);
        $email_tpl  = $data;
        $emails = Email::get();
        $email_tpl['subject']  = $this->vars['wanna_work_with_us'];
        $email_tpl['to_email'] = $emails[0]->name_ro;
        $email_tpl['cc']       =  Email::where('id','!=',$emails[0]->id)->pluck('name_ro')->toArray();
        Mail::send('emails.work_with_us', $email_tpl, function ($message) use ($email_tpl) {
            $message->to([$email_tpl['to_email']])->subject($email_tpl['subject']);
            if($email_tpl['cc']) {
                $message->cc($email_tpl['cc']);
            }
        });
        return redirect()->back()->with('success', $this->vars['transmis_cu_success']);

    }

}
