<?php

namespace App\Http\Controllers;


use App\Models\Booking;
use App\Models\Email;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class BookController extends AppController
{
    /**
     * Send form afer pres Book now
     *
     * @return \Illuminate\View\View
     */
    public function sendBookForm($location = '',Request $request)
    {
        $vars = Cache::get('vars');
        $messages = [
            'first_name.required' => $vars['numele_este_obligatoriu'],
            'phone.required' => $vars['telefonul_este_obligatoriu'],
            'email.required' => $vars['email_este_obligatoriu'],
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'first_name' => 'required',
            'phone' => 'required',
        ],$messages);

        if ($validator->fails())
        {
            return Response::json(['errors' => $validator->errors()]);
        }
        else
        {
            $data = [
                'location_id'  =>  $request->input('location_id'),
                'page_url'  =>  $request->input('current_page'),
                'email'  =>  $request->input('email'),
                'first_name'  =>  $request->input('first_name'),
                'services'  =>  ($request->input('services')),
                'service_id'  =>  $request->input('service_id'),
                'phone'  =>  $request->input('phone')
            ];
            Booking::create($data);
            $emails = Email::get();
            $email_tpl             = $data;
            $email_tpl['subject']  = $vars['rezervari_prealabile'].' '.env('site_name');
            $email_tpl['location'] = $request->input('location');
            $email_tpl['to_email'] = $emails[0]->name_ro;
            $email_tpl['cc']       = Email::where('id','!=',$emails[0]->id)->pluck('name_ro')->toArray();
            // services
            $email_tpl['service']  = '';
            if($data['service_id']) {
                $service = Service::find($data['service_id']);
                $email_tpl['service']    = !empty($service) ? $service->name_ro : '';
            }
            $email_tpl['serviceList'] = !empty($data['services']) ? Service::whereIN('id',[$data['services']])->get()->toArray() : '';

            Mail::send('emails.book_now', $email_tpl, function ($message) use ($email_tpl) {
                $message->to([$email_tpl['to_email']])->subject($email_tpl['subject']);
                if($email_tpl['cc']) {
                    $message->cc($email_tpl['cc']);
                }
            });
            return Response::json(['success' => '1']);
        }
    }

    /**
     * Show modal for servies
     *
     * @return \Illuminate\View\View
     */
    public function bookModal(Request $request)
    {
        $data = [
          'service_id' =>  $request->input('service_id'),
        ];
        return View::make('modals.book-content')->with($data)->render();
    }

}
