<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Page;
use App\Models\Product;
use App\Models\Vars;
use App\Models\Wishlist;
use App\Services\CursService;
use App\Services\NavMenuService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;

use App\Models\Compare;

class AppController extends Controller
{
    public $vars;
    public $lng;
    public function __construct()
    {
        // nav menu
        $navService = new NavMenuService();
        $menu = $navService->getNav();
        View::share('navMenu', $menu['navMenu']);
        View::share('privacyMenu', $menu['privacyMenu']);
        View::share('navFooterShop', $menu['navFooterShop']);
        View::share('navFooterHelpInformation', $menu['navFooterHelpInformation']);
        // locations
        View::share('languages', $menu['locations']);
        // Colors
        View::share('colors', $menu['colors']);

        View::share('showFooter',1);
        View::share('showHeader',1);

        $this->lng = App::getLocale();
        View::share('lng',$this->lng);

        // valuta
        View::share('valuta',valutaValue($this->lng));

        // vars
        $this->vars = Vars::getList($this->lng);
        View::share('vars',$this->vars);

        $this->middleware(function ($request, $next) {
            // Wishlist
            View::share('wishlistIDs', Wishlist::getWishlistIDs());
            return $next($request);
        });

    }

}
