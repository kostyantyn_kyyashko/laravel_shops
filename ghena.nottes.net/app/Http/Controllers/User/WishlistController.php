<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\UserProfileSaveRequest;
use App\Models\Order;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;

class WishlistController extends AppUserController
{
    /**
     * List
     */
    public function list(Request $request)
    {
        $wishlists = Wishlist::where('user_id',Auth::user()->id)->with(['product'])->paginate(10);
        return view('user.wishlist.index',compact('wishlists'));
    }
}
