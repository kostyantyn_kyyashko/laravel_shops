<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use View;

class UserController extends AppUserController
{
    /**
     * User Account/Profile
     */
    public function profile(Request $request)
    {
        $user = Auth::user();
        return view('user.account.index',compact('user'));
    }

    /**
     * Update User Profile
     *
     */
    public function updateProfile(Request $request)
    {
        $vars     = $this->vars;
        $fields   = $request->all();
        $user     = User::where('id',Auth::user()->id)->first();
        $exitUser = User::where('email',$fields['email'])->where('id','!=',$user->id)->first();
        if($exitUser) {
            return redirect()->back()->withError($vars['email_exits']);
        }
        $user->fill($fields);
        $user->save();
        return redirect(route('user.profile'))->withSuccess($vars['save_with_success']);
    }

    /**
     * Change User password view
     *
     */
    public function changePassword(Request $request)
    {
        $user = Auth::user();
        return view('user.account.change_password',compact('user'));
    }

    /**
     * Update Change User password
     *
     */
    public function updateChangePasswod(Request $request)
    {
        $vars = Cache::get('vars');
        $messages = [
            'password.required' => $vars['parola_este_obligatorie'],
            'password_confirmation.required' => $vars['parola_este_obligatorie'],
            'password_confirmation.min' => $vars['parola_trebuie_sa_contina_minim_6_caractere'],
            'password_confirmation.same' => $vars['parola_repetata_trebuie_sa_coincida'],
            'password.confirmed' => $vars['parola_repetata_trebuie_sa_coincida'],
            'password.min' => $vars['parola_trebuie_sa_contina_minim_6_caractere'],
        ];
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ],$messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $user   = User::where('id',Auth::user()->id)->first();
            $user->password = bcrypt($request->password);
            $user->save();
            return redirect()->back()->withSuccess($vars['parola_a_fost_schimbata_cu_scucces']);
        }
    }
    /**
     * Logout user
     *
     * @return \Illuminate\View\View
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
