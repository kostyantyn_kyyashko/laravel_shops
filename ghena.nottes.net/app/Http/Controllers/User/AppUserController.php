<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Download;
use App\Models\Vars;
use App\Services\NavMenuService;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AppUserController extends Controller
{
    public $vars;
    public $lng;
    public $user;
    public function __construct()
    {
        // nav menu
        $navService = new NavMenuService();
        $menu = $navService->getNav();
        View::share('navMenu', $menu['navMenu']);

        $this->lng = App::getLocale();
        View::share('lng',$this->lng);
        // Colors
        View::share('colors', $menu['colors']);

        View::share('showFooter',1);
        View::share('showHeader',1);
        // vars
        $this->vars = Vars::getList($this->lng);
        View::share('vars',$this->vars);
        // valuta
        View::share('valuta',valutaValue($this->lng));
        $this->middleware(function ($request, $next){

            $this->user = Auth::user();
            View::share('userLogged',$this->user);

            return $next($request);
        });


    }

}
