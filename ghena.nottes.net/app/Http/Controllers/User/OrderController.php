<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\UserProfileSaveRequest;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;

class OrderController extends AppUserController
{
    /**
     * List
     */
    public function list(Request $request)
    {
        $orders = Order::where('user_id',Auth::user()->id)->orderBy('delivery_date','DESC')->paginate(10);
        return view('user.order.index',compact('orders'));
    }

    /**
     * Show Order detail
     *
     * @return \Illuminate\View\View
     */
    public function detail($invoiceID)
    {
        $order   = Order::where('invoiceID',$invoiceID)->with(['products'])->first();
        if(empty($order)) {
            return redirect('/');
        }

        return view('order.detail',compact('order'));
    }
}
