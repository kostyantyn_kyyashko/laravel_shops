<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Models\Card;
use App\Models\Email;
use App\Models\Location;
use App\Models\MoneyLog;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\Shipping;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CartController extends AppController
{
    /**
     * Add products from category type like buy the look
     *
     */
    public function addLookListToCart(Request $request)
    {
        $products = $request->input('products');
        $lng      = $request->input('lng');

        foreach ($products as $id) {
            $product = Product::find($id);
            $options = [
                'color_id' => $request->input('color_'.$id),
                'size_id' => $request->input('size_'.$id),
                'desc' => $product->{'desc_'.$lng},
                'image' => $product->image,
                'slug' => $product->slug
            ];

            Cart::add([
                'id' => $id,
                'price' => $product->{'price_'.$lng},
                'name' => $product->{'name_'.$lng},
                'qty' => $request->input('qt_'.$id),
                'options' => $options
            ]);
        }
        return redirect(route('cart'));
    }

    /**
     * Show Cart list
     *
     */
    public function getList()
    {
        $cart       = Cart::content();
        $showFooter = false;
        return view('cart.list',compact('cart','showFooter'));
    }

    /**
     * Cart Stept Delivery Info
     *
     */
    public function delivery()
    {
        $showHeader = false;
        $showFooter = false;
        $step1      = true;
        return view('cart.delivery',compact('showHeader','showFooter','step1'));
    }

    /**
     * Checkout Page
     *
     */
    public function checkout()
    {
        $cart         = Cart::content();
        $showHeader   = false;
        $showFooter   = false;
        $step2        = true;
        $shippingList = Shipping::where('name_'.$this->lng,'!=','')->orderBy('name_'.$this->lng,'DESC')->get();
        return view('cart.checkout',compact('cart','showHeader','shippingList','showFooter','step2'));
    }


    /**
     * Do Checkout
     *
     * @return \Illuminate\View\View
     */
    public function doCheckout(Request $request)
    {
        $cart   = Cart::content();
        if($cart) {
            $shipping       = Shipping::find($request->input('shipping_id'));
            $shipping_cost  = !empty($shipping) ? (int)$shipping->{'price_'.$this->lng} : 0;
            $cart           = Cart::content();
            $subtotal       = Cart::total();// without shipping
            $total          = $subtotal+$shipping_cost;
            $payType = $request->input('payType');
            $payType = 2;// 1 - CASH on delivery , 2 - CARD on delivery
            $order = Order::create([
                'date' => date('Y-m-d H:i:s'),
                'shipping_date' => date('Y-m-d H:i:s'),
                'delivery_date' => date('Y-m-d H:i:s'),
                'discount' => 0,
                'shipping_cost' => $shipping_cost,
                'user_id' => $request->input('user_id'),
                'shipping_id' => $request->input('shipping_id'),
                'location' => $request->input('location'),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'phone' => $request->input('phone'),
                'address1' => $request->input('address1'),
                'address2' => $request->input('address2'),
                'postal_code' => $request->input('postal_code'),
                'location' => $request->input('location'),
                'district' => $request->input('district'),
                'country' => $request->input('country'),
                'email' => $request->input('email'),
                'status' =>2, // 1 - achitat , 2 - neachitat
                'payType' => $payType, // 1 - CASH on delivery , 2 - CARD on delivery
                'subtotal' => $subtotal,
                'total' =>$total,
                'invoiceID' => generateUID(),
            ]);
            $totalQt = 0;
            foreach ($cart as $row) {
                // remove from value from product Qt
                $productSieze = ProductSize::where(['product_id' => $row->id,'size_id' => $row->options->size_id])->first();
                if($productSieze) {
                    $productSieze->qt = $productSieze->qt-$row->qty;
                    $productSieze->save();
                }
                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $row->id,
                    'size_id' => $row->options->size_id,
                    'color_id' => $row->options->color_id,
                    'price' => $row->price,
                    'qt' => $row->qty,
                ]);
                $totalQt = $totalQt + $row->qty;
            }
            Cart::destroy();
            $order->qt = $totalQt;
            $order->save();

            // Send email
            $emails = Email::get();
            if($emails) {
                $email_tpl['subject']  = 'DELIVERY ID#'.$order->id;
                $email_tpl['order']    = Order::where('id',$order->id)->with(['products'])->first();;
                $email_tpl['to_email'] = $emails[0]->name_ro;
                $email_tpl['cc']       = Email::where('id','!=',$emails[0]->id)->pluck('name_ro')->toArray();
                Mail::send('emails.order', $email_tpl, function ($message) use ($email_tpl) {
                    $message->to([$email_tpl['to_email']]);
                    if($email_tpl['cc']) {
                        $message->cc($email_tpl['cc']);
                    }
                    $message->subject($email_tpl['subject']);
                });
            }
            return redirect(route('orderDetail', ['invoiceID' => $order->invoiceID]));
        } else {
            return redirect('/');
        }

    }
}
