<?php

namespace App\Http\Controllers;

use App\Models\Collection;
use App\Models\News;
use App\Models\Page;
use Illuminate\Http\Request;

class BlogController extends AppController
{
    /**
     * Show list of blog
     *
     * @return \Illuminate\View\View
     */
    public function list()
    {
        $blog       = Page::find(10);
        $newsList   = News::paginate(6);
        $activeMenu = 10;
        $page       = $blog;
        $collection = Collection::where('onNewsFooter',1)->first();
        return view('blog.list',compact('newsList','blog','activeMenu','page','collection'));
    }

    /**
     * Show Blog detail
     *
     * @return \Illuminate\View\View
     */
    public function detail($slug)
    {
        $blog = News::where('slug',$slug)->first();
        $page = $blog;
        if(empty($blog)) {
            return redirect(route('blog'));
        }
        $mostPopularList = News::where('onPopularNews',1)->paginate(3);
        $activeMenu      = 10;
        return view('blog.detail',compact('blog','page','activeMenu','mostPopularList'));
    }
}
