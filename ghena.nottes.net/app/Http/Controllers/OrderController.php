<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\Order;
use App\Models\Page;
use App\Models\Product;
use App\Models\Shipping;
use App\Models\Vars;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class OrderController extends AppController
{

    /**
     * Show Order detail
     *
     */
    public function detail($invoiceID)
    {
        $order   = Order::where('invoiceID',$invoiceID)->with(['products'])->first();
        if(empty($order)) {
            return redirect('/');
        }
        $showHeader   = false;
        $showFooter   = false;
        $step3        = true;
        $shipping     = Shipping::find($order->shipping_id);
        return view('order.detail',compact('order','showHeader','showFooter','step3','shipping'));
    }



}
