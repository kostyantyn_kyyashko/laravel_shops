require("./bootstrap");

import Vue from "vue";

import Paginate from 'vuejs-paginate';

Vue.component('paginate', Paginate)

Vue.component('products-list', {
    data: function () {
        return {
            product_id: '',
            products: [],
            sizeList: [],
            filterSizes: [],
            sizes: 0,
            totalPages: 0,
            totalPagesTest: 3333,
            totalFoundPages: 0,
            pagination : 'pagination',
            selectedPage : 1,
            prev : '',
            next : '',
            loading: true
        }
    },
    methods:{
        addRemovetoFilterSizes: function(item) {
            if(this.filterSizes.includes(item)) {
                this.filterSizes.splice(this.filterSizes.indexOf(item), 1)
            } else {
                this.filterSizes.push(item);
            }
            this.getProductsList();
        },
        isSizeActive: function(id) {
            return this.filterSizes.indexOf(id) != -1;
        },
        paginationCallback: function(pageNumber) {
            this.getProductsList(pageNumber);
        },
        newFunc: function() {
           alert('Call From js')
        },
        getUrlVars: function(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
            }
            return '';
        },
        addToCompare: function(product) {
            var params = {};
            params.product_id = product.id
            axios({
                method: 'post',
                url: '/api/v1/product/addCompare',
                data: params,
                responseType:'json'
            })
                .then(function(resp) {
                    console.log(resp);
                    $('.compare_class_'+product.id).html('<i class="fa fa-check"></i> '+resp.data.message);
                });
        },
        getProductsList: function(pageNumber) {
            var self     = this;
            console.log('here');
            //

            // update up url on link
            // sizeList list
            // var  upUrl = new URL($('.products_url').val());
            // upUrl.searchParams.delete('sizes[]');
            //
            // this.filterSizes.forEach(function(value){
            //     upUrl.searchParams.append('sizes[]', value);
            // });
            //
            // history.pushState(null,null,upUrl.href);
            // end update up url on link

            var params = pageNumber != undefined ? pageNumber : self.getUrlVars('page');
            // var page  = pageNumber != undefined ? pageNumber : self.getUrlVars('page'),
            //     model = self.getUrlVars('model_id'),
            //     color = self.getUrlVars('color_id'),
            //     sizes = self.getUrlVars('sizes'),
            //     price_from = self.getUrlVars('price_from'),
            //     price_to = self.getUrlVars('price_to');
            //
            // self.page = page;
            //
            // if(color) { params.color = color;}
            // if(sizes) { params.sizes = sizes; }
            // if(price_from) { params.price_from = price_from; }
            // if(price_to) { params.price_to = price_to; }
            // if(page) { params.page = page; }
            //
            //
            // // generate push url
            // this.upUrl = new URL($('.products_url').val());
            // if(upUrl) {
            //     upUrl = upUrl+'#goTo';
            //     history.pushState(null,null,$('.products_url').val()+'?'+upUrl);
            // }
            //
            // // add other fields to params

            // params._token = $('.csrf_token').val();
            axios({
                method: 'post',
                url: '/api/v1/product/list',
                data: params,
                responseType:'json'
            })
            .then(function(resp) {
                self.products = resp.data.products;
                self.sizeList = resp.data.sizeList;
                self.totalPages = resp.data.totalPages;
                self.totalPagesTest = resp.data.totalPagesTest;
                self.totalFoundPages = resp.data.totalFoundPages;
                self.loading = false;
                self.selectedPage = parseInt(resp.data.currentPage);
                console.log(self.products);
                console.log('totalPagesTest='+self.totalPagesTest);
            });


        }
    },
    mounted: function () {
        this.getProductsList(2);

        // axios({
        //     method: 'post',
        //     url: '/api/v1/product/list',
        //     responseType:'json'
        // }).then(response => (this.products = response.data.products));

    }
})

Vue.component('wishlist', {
    computed: {
        'fav_class' : function () {
            if(this.icon == 'home_last_collection') {
                return 'home__card-like';
            }
            if(this.icon == 'home_popular') {
                return 'home__popular-card-like';
            }
        },
        'fav_class_active' : function () {
            if(this.icon == 'home_last_collection') {
                return 'home__card-like home__card-like_active';
            }
            if(this.icon == 'home_popular') {
                return 'home__popular-card-like home__popular-card-like_active';
            }
        },
        isFavorited1 : function () {
            return this.localFavorited;
        }

    },
    props: ['product', 'favorited','icon'],
    template: '' +
    '        <a href="#" v-if="isFavorited1" :class="fav_class_active" @click.prevent="unFavorite(product)">\n' +
    '        </a>\n' +
    '        <a href="#" v-else @click.prevent="favorite(product)" :class="fav_class"  >\n' +
    '        </a>\n'
    ,
    data: function() {
        return {
            localFavorited : this.favorited
        }
    },
    methods: {
        favorite: function(product_id) {
            var self = this;
            axios({
                method: 'post',
                url: '/api/v1/product/addWishlist',
                data: {product_id:product_id},
                responseType:'json'
            })
                .then(function(resp) {
                    self.localFavorited = true;
                });
        },
        unFavorite: function(product_id) {
            var self = this;
            axios({
                method: 'post',
                url: '/api/v1/product/removeWishlist',
                data: {product_id:product_id},
                responseType:'json'
            })
                .then(function(resp) {
                    self.localFavorited = false;
                });
        }
    },
    mounted: function () {
        // this.isFavorited = this.isFavorite ? true : false;
    }
});

Vue.component('wishlist-button', {
    computed: {
        'fav_class' : function () {
            return 'button__wishlist--add';
        },
        'fav_class_active' : function () {
            return 'button__wishlist--add button__wishlist--added';
        },
        isFavorited1 : function () {
            return this.localFavorited;
        }

    },
    props: ['product', 'favorited','title_add','title_remove'],
    template: '' +
    '        <button v-if="isFavorited1"  @click.prevent="unFavorite(product)" type="submit" class="button button--fill-white">\n'+
    '        <span class="button__wishlist button__wishlist--add" v-html="title_add" ></span>\n'+
    '        </button>\n' +
    '        <button v-else  @click.prevent="favorite(product)" type="submit" class="button button--fill-white">\n'+
    '        <span class="button__wishlist button__wishlist--add button__wishlist--added" v-html="title_remove" ></span>\n'+
    '        </button>\n'
    ,
    data: function() {
        return {
            localFavorited : this.favorited
        }
    },
    methods: {
        favorite: function(product_id) {
            var self = this;
            axios({
                method: 'post',
                url: '/api/v1/product/addWishlist',
                data: {product_id:product_id},
                responseType:'json'
            })
                .then(function(resp) {
                    self.localFavorited = true;
                });
        },
        unFavorite: function(product_id) {
            var self = this;
            axios({
                method: 'post',
                url: '/api/v1/product/removeWishlist',
                data: {product_id:product_id},
                responseType:'json'
            })
                .then(function(resp) {
                    self.localFavorited = false;
                });
        }
    },
    mounted: function () {
    }
});


// Vue.component('favorite', require('./components/Favorite.vue'));

const app = new Vue({
    el: "#app"
});
