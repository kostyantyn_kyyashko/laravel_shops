<div id="restore-password" class="modal mfp-hide">
    <div class="title">{{ $vars['resetare_parola'] }}</div>
    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <div class="form-item">
            <div class="label">{{ $vars['email'] }}</div>
            <input type="text" name="email" required="required">
        </div>
        <button type="submit" class="bt-dark-green">{{ $vars['reseteaza_parola'] }}</button>
    </form>
    <p class="note">{{ $vars['instructiunele_de_resetare_au_fost_trimise_pe_mail'] }}</p>
</div>