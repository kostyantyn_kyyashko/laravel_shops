<div id="modal-login" class="modal mfp-hide">
    <div class="title">{{ $vars['logare'] }}</div>
    <form class="auth_form login-form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="form-item">
            <div class="label">{{ $vars['email'] }}</div>
            <input type="text" name="email">
            <span class="red hidden email-error"></span>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['parola'] }}</div>
            <input type="password" name="password">
            <span class="red hidden password-error"></span>
        </div>
        <div class="restore-password">
            <a href="#restore-password" class="magnificPopup">{{ $vars['am_uitat_parola'] }}</a>
        </div>
        <div class="enter-social">
            <a href="{{ url('auth/facebook') }}" class="enter-social__link">
                <span><span class="icon"><img src="i/fb2.svg" alt=""></span>LOG IN WITH FACEBOOK</span>
            </a>
            <a href="{{ url('auth/google') }}" class="enter-social__link">
                <span><span class="icon"><img src="i/gmail.svg" alt=""></span>LOG IN WITH GMAIL</span>
            </a>
        </div>
        <button type="button" class="bt-dark-green login-now">{{ $vars['intra'] }}</button>
        <div align="center">
            <span class="red hidden errors_login-error"></span>
        </div>
    </form>
</div>