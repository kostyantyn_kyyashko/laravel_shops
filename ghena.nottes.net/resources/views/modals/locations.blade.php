
<div id="modal-location" class="modal mfp-hide">
    <form class="auth_form login-form" method="POST" action="{{ route('login') }}">
        <div class="title">{{ $vars['alege_locatia'] }}</div>
        <ul>
            @foreach($locations as $value)
                <li>
                    <a href="/{{ $lng }}/{{ $value->slug }}/setLocation" onclick="window.location.href='/{{ $lng }}/{{ $value->slug }}/setLocation'" >
                        {{ $value->{'name_'.$lng} }}
                    </a>
                </li>
            @endforeach
        </ul>
    </form>
</div>