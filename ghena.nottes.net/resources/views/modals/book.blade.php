<div id="book-now-modal" class="modal mfp-hide">
    <div class="book-modal-content">
        @include("modals/book-content")
    </div>
</div>