<div id="modal-reg" class="modal mfp-hide">
    <div class="title">{{ $vars['creare_cont'] }}</div>
    <form class="form-horizontal register-form" method="POST" action="{{ route('register',$location->slug) }}">
        {{ csrf_field() }}
        <div class="form-item">
            <div class="label">{{ $vars['nume'] }}</div>
            <input type="text" name="first_name" required="required" >
            <span class="red hidden first_name-error"></span>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['prenume'] }}</div>
            <input type="text" name="last_name" required="required" >
            <span class="red hidden last_name-error"></span>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['data_nasterii'] }}</div>
            <div class="group-row">
                <div class="input">
                    <input type="text" placeholder="02" name="dday" required="required">
                </div>
                <div class="select" >
                    <select name="dmonth" required="required" >
                        @for ($i = 0; $i < 12; $i++)
                            <option value="{{ $i }}">{{ strtoupper(date('F',strtotime(date('Y-'.$i.'-d')))) }}</option>
                        @endfor
                    </select>
                </div>
                <div class="select">
                    <select  name="dyear" required="required" >
                        @for ($i = date('Y')-10; $i >= 1900; $i--)
                             <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['adresa'] }}</div>
            <select name="location_id" required="required">
                @foreach($locations as $value)
                     <option value="{{ $value->id }}">{{ $value->{'name_'.$lng} }}</option>
                @endforeach
            </select>
            <div class="group-row">
                <div class="input">
                    <input type="text" name="street" placeholder="{{ $vars['str_dacia_44'] }}" required="required">
                </div>
                <div class="input">
                    <input type="text" name="nrApartment" placeholder="{{ $vars['ap_99'] }}" required="required">
                </div>
                <div class="input">
                    <input type="text" name="scaleApartment" placeholder="{{ $vars['scara_1'] }}" required="required">
                </div>
            </div>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['telefon'] }}</div>
            <input type="tel" name="phone" required="required">
            <span class="red hidden phone-error"></span>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['email'] }}</div>
            <input type="text" name="email" required="required" >
            <span class="red hidden email-error"></span>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['parola'] }}</div>
            <input type="password" name="password" required="required">
            <span class="red hidden password-error"></span>
        </div>
        <div class="form-item">
            <div class="label">{{ $vars['repeta_parola'] }}</div>
            <input type="password" name="password_confirmation" required="required">
            <span class="red hidden password_confirmation-error"></span>
        </div>
        {{--<div class="enter-social">--}}
        {{--<a href="" class="enter-social__link">--}}
        {{--<span><span class="icon"><img src="i/fb2.svg" alt=""></span>LOG IN WITH FACEBOOK</span>--}}
        {{--</a>--}}
        {{--<a href="" class="enter-social__link">--}}
        {{--<span><span class="icon"><img src="i/gmail.svg" alt=""></span>LOG IN WITH GMAIL</span>--}}
        {{--</a>--}}
        {{--</div>--}}
        <input type="hidden" value="{{ $location->slug }}" value="location" >
        <button type="button" class="bt-dark-green register-now">{{ $vars['intregistreaza_te'] }}</button>
    </form>
</div>
