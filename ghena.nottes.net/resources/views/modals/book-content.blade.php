<div class="title">{{ $vars['rezerveaza'] }}</div>
<form class="auth_form book-now-form" method="POST" action="{{ route('bookNow',$location->slug) }}">
    {{ csrf_field() }}
    <div class="form-item">
        <div class="label">{{ $vars['nume'] }}</div>
        <input type="text" name="first_name" required="required" >
        <span class="red hidden first_name-error"></span>
    </div>
    <div class="form-item">
        <div class="label">{{ $vars['email'] }}</div>
        <input type="text" name="email" required="required">
        <span class="red hidden email-error"></span>
    </div>
    <div class="form-item">
        <div class="label">{{ $vars['telefon'] }}</div>
        <input type="tel" name="phone" required="required">
        <span class="red hidden phone-error"></span>
    </div>
    <input type="hidden" name="current_page" value="{{ url()->current() }}" >
    <input type="hidden" name="location_id" value="{{ $location->id }}" >
    <input type="hidden" name="location" value="{{ $location->{'name_'.$lng} }}" >
    <button type="button" class="bt-dark-green book-now">{{ $vars['trimite'] }}</button>
    <input type="hidden" name="service_id" class="service_id" value="{{ isset($service_id) ? $service_id : '' }}">
    <div align="center">
        <p class="text-success bold text-center hidden sent-with-success">
            {{ $vars['transmis_cu_success'] }}
        </p>
    </div>
</form>