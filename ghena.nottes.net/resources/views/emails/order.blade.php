<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }


        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

        h3.succes_p {
            text-align: center;
        }

    </style>
</head>

<body>
<div class="invoice-box">
    <h3 class="succes_p">
        Comanda dvs urmeaza a fi procesata. Asteptati apelul nostru in scurt timp.
    </h3>
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table style=" width: 100%;">
                    <tr>
                        <td class="title">
                            <img src="{{ env('APP_URL') }}/assets/img/logo.png" style="width:100%; max-width:150px;">
                        </td>

                        <td style="text-align: right">
                           <span style="float: right;text-align: right">
                                Metoda de Plata : {{ $order->payType == 1 ? env('cash_on_delivery') : env('card_on_delivery') }} <br>
                                Invoice #: {{ $order->id }}<br>
                                Data: {{ date('d F, Y',strtotime($order->date)) }}<br>
                           </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <strong>Adresa:</strong> {{ $order->address }}<br>
                            <strong>Telefon:</strong> {{ $order->phone }}<br>
                        </td>
                        <td>
                            <span style="float: right;text-align: right">
                                  {{ $order->first_name }}<br>
                                  {{ $order->last_name }}<br>
                                {{ $order->email }}
                            </span>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr class="heading">
            <td>
                Articol
            </td>
            <td style="text-align: right">Pret</td>

        </tr>
        @if($order->products)
            @foreach($order->products as  $key => $value)
                <tr class="item {{ $key+1 == count($order->products) ? 'last' : '' }}">
                    <td>{{ $value->name_ro }}({{ $value->pivot->qt }})</td>
                    <td style="text-align: right">{{ $value->pivot->qt*$value->price }} lei</td>
                </tr>
            @endforeach
        @endif

        <tr class="total">
            <td></td>
            <td style="text-align: right">
                @if( $order->shipping>0)
                    Livrare: {{ $order->shipping}} lei <br>
                @endif
                @if($order->discount)
                    Discount : {{ $order->discount }} % <br>
                @endif
                Total: {{ $order->total}} lei
            </td>
        </tr>
    </table>
</div>
</body>
</html>
