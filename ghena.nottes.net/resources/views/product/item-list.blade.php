<div class="products__item">

    <div class="products-card">
        <a href="{{ $value->formattedSlug() }}" class="products-card__slider">
            @if($value->video)
                <video width="100%" autoplay loop muted>
                    <source src="{{ $value->video }}" type="video/mp4">
                </video>
            @else
                <img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                {{--@if(!isset($isAjax))--}}
                    <img src="{{ !empty($value->image1) ? $value->image1 : $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                    <img src="{{ !empty($value->image2) ? $value->image2 : $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                    <img src="{{ !empty($value->image3) ? $value->image3 : $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                    <img src="{{ !empty($value->image4) ? $value->image4 : $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                {{--@endif--}}
            @endif

        </a>
        <h3>
            <a href="{{ $value->formattedSlug() }}">
                {{ $value->{'name_'.$lng} }}
            </a>
        </h3>
        <span>{{ $value->{'price_'.$lng} }} {{ $valuta }}</span>
        <a href="#" class="products-card-like @if(in_array($value->id,$wishlistIDs)) products-card-like_active @endif"></a>
    </div>

</div>