<div class="products-card">
    <a :href="product.link" class="products-card__slider">
        <img :src="product.image" alt="Vivi">
    </a>
    <h3><a href="#">@{{ product.name }}</a></h3>
    <span>67 {{ $valuta }}</span>
    <a href="#" class="products-card-like products-card-like_active"></a>
</div>