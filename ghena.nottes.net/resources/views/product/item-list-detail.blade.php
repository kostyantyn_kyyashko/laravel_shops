<div class="cart__row">

    <div class="view-cart hidden">
        <div class="view-cart__img">
            <img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
            <div class="view-cart__price">{{ $value->{'price_'.$lng} }} {{ $valuta }}</div>
        </div>
        <a href="{{ route('cart') }}" class="view-cart__btn">view cart </a>
        <button type="button" class="view-cart__close"></button>
    </div>

    <div class="cart__left">
            <div class="cart__product">
                <a href="{{ $value->slug() }}">
                    <img src="{{ $value->path.'_'.'570x820'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}">
                </a>
            </div>
            @if($value->image1 )
                <div class="cart__product">
                    <a href="{{ $value->slug() }}">
                        <img src="{{ $value->path1.'_'.'570x820'.'.'.$value->ext1 }}" alt="{{ $value->{'name_'.$lng} }}">
                    </a>
                </div>
            @endif
    </div>
    <div class="cart__right">
        <div action="#" class="cart__form">
            <div class="cart__form-title">
                <div class="cart__form-title-text">{{ $value->{'name_'.$lng} }}</div>
                <div class="cart__form-title-price">{{ $value->{'price_'.$lng} }} {{ $valuta }}</div>
            </div>
            <div class="cart__form-color-wrap">
                <div class="radio-color">
                    @php $defaultColorID = 0; @endphp
                    @foreach($value->colors as $kcolor => $color)
                        <div class="radio-color__item radio-color__item__{{ $color->id }} ch_label_radio_look ch_label_{{ $value->id }}"
                             data-id="{{ $color->id }}" data-product_id="{{ $value->id }}"
                        >
                            <input id="radio-color-{{ $value->id }}_{{ $color->id }}"
                                   type="radio"
                                   name="color_{{ $value->id }}"
                                   data-id="{{ $color->id }}"
                                   value="{{ $color->id }}"
                                   data-product_id="{{ $value->id }}"
                                   class="product_color"
                                   @if($kcolor== 0)
                                     @php $defaultColorID = $color->id; @endphp
                                     checked
                                    @endif
                            >
                            <label for="radio-color-{{ $value->id }}_{{ $color->id }}"></label>
                        </div>
                    @endforeach
                    <input type="hidden" class="select_size_{{ $value->id }}" name="size_{{ $value->id }}" value="{{ $defaultColorID }}">
                </div>
            </div>
            <div class="cart__select-size">
                <div class="select-size">
                    <div class="select-size__value">
                        {{ isset($value->sizes[0]->{'name_'.$lng}) ? $value->sizes[0]->{'name_'.$lng} : $vars['choose_a_value'] }}
                    </div>
                    <ul class="select-size__list">
                        @php $defaultSizeID = 0; @endphp
                        @foreach($value->sizes as $ksize => $size)

                            <li>
                                <span class="select-size__item-value" data-product_id="{{ $value->id }}"  data-id="{{ $size->id }}" >
                                    {{ $size->{'name_'.$lng} }}
                                </span>
                                @if($size->pivot->qt == 0)
                                    <span class="select-size__stock">Out of stock</span>
                                @endif
                                @if($size->pivot->qt > 0 && $size->pivot->qt < 5)
                                    <span class="select-size__left">{{  $size->pivot->qt }} models left</span>
                                @endif

                            </li>
                            @if($ksize== 0) @php $defaultSizeID = $size->id; @endphp @endif
                        @endforeach
                    </ul>
                    <input type="hidden" class="select_size_{{ $value->id }}" name="size_{{ $value->id }}" value="{{ $defaultSizeID }}">
                </div>
            </div>

            <div class="cart__quantity">
                <div class="product-quantity">
                    <button type="button" class="product-quantity__bnt product-quantity__bnt--minus" data-product_id="{{ $value->id }}" >-</button>
                    <input class="product-quantity__input" name="qt_{{ $value->id }}" type="text" value="1">
                    <button type="button" class="product-quantity__bnt product-quantity__bnt--plus" data-product_id="{{ $value->id }}" >+</button>
                </div>
            </div>
            <div class="cart__size-link">
                <a href="#">size guide</a>
            </div>
            @if(isset($isDetailProduct))
                <div class="cart__add-cart">
                    <button type="submit"
                            class="button button--fill-black ajax_add_to_cart add_to_cart_{{ $value->id }}"
                            data-product_id="{{ $value->id }}"
                            data-page="item-list"
                            data-qt="1"
                            data-color="{{ $defaultColorID }}"
                            data-size="{{ $defaultSizeID }}"
                    >
                        Add to cart
                    </button>
                </div>
                <div class="cart__add-wishlist">
                    <wishlist-button :product='{{ $value->id }}' title_remove="{{ $vars['remove_from_wishlist'] }}" :title_add="'{{ $vars['add_to_wishlist'] }}'" :favorited={{ in_array($value->id,$wishlistIDs) ? 'true' : 'false' }} ></wishlist-button>
                </div>
            @endif
        </div>

        <div class="cart__details-row">
            <div class="cart__details-left">
                <div class="cart__details-title">details</div>
                <div class="cart__details-text">
                    {!! $value->{'text_'.$lng} !!}
                </div>
            </div>
            <div class="cart__details-right">
                <div class="cart__details-material">  {!! $value->{'name2_'.$lng} !!}</div>
                <div class="cart__details-dry">  {!! $value->{'name3_'.$lng} !!}</div>
            </div>
        </div>
        <input type="hidden" name="products[]" value="{{ $value->id }}" >
        <input type="hidden" name="lng" value="{{ $lng }}" >
    </div>
</div>
@section('header_styles')
@foreach($colors as $kcolor => $color)
    <style>
        .radio-color__item__<?=$color->id?> label::before,
        .radio-color__item__<?=$color->id?> span::before
        {
            background-color: {{ $color->code }} !important;
        }
    </style>
@endforeach
@stop