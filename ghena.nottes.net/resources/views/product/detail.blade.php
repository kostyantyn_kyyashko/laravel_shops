@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $product->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <div class="container" id="app" >
        @include('product/item-list-detail')
    </div>

@stop

@section('footer_scripts')
    <script src="{{ asset('assets/js/manifest.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
@stop

