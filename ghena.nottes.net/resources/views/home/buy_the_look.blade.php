@if($byTheLookList)
    <section class="home__look">
    <div class="container">
        <h2 class="home__look-title">{{ $byTheLook->{'name_'.$lng} }}</h2>
        <div class="home__look-slider-wrap">

            <div class="home__look-slider-arrows">
                <div class="slider-arrows">
                    <div class="slider-arrows__prev">
                        <span class="slider-arrows__prev-text">{{ $vars['next'] }}</span>
                        <span class="slider-arrows__prev-arrow"></span>
                    </div>
                    <div class="slider-arrows__next">
                        <span class="slider-arrows__next-text">{{ $vars['prev'] }}</span>
                        <span class="slider-arrows__next-arrow"></span>
                    </div>
                </div>
            </div>

            <div class="home__look-slider">
                @foreach($byTheLookList as $value)
                    <div class="home__look-item">
                        <a href><img src="{{ $value->path.'_'.'405x606'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}"></a>
                        <h3><a href="{{ route('productsByTheLook',$value->slug) }}">{{ $value->{'name_'.$lng} }}</a></h3>
                        <div class="home__look-btn">
                            <a href="{{ route('productsByTheLook',$value->slug) }}" class="button button--black">buy the look</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
