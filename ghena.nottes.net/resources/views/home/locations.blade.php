@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <div class="country">
        <div class="country__row">
            <div class="country__col-1">
                <a href="{{ route('home') }}" class="header__logo"><img src="/assets/img/logo.png" alt="Vivi"></a>
                <div class="country__title">Select your country</div>
                <form class="country__form" action="{{ route('setLocation') }}" method="post">
                    {{ csrf_field() }}
                    <div class="country__text-wrap">

                        <div class="cart__select-size">
                            <div class="select-size">
                                <div class="select-size__value">Your country</div>
                                <ul class="select-size__list">
                                    @foreach($languages as $value)
                                        <li>
                                            <span class="select-size__item-value"  data-code="{{ $value->code }}">
                                                {{ $value->{'name_ro'} }}
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                    <input type="hidden" class="selected_location_code" name="location_code" value="" />
                    <button type="submit" class="button button--fill-black">go</button>
                </form>
                <div class="col-sm-12 col-md-12 clearfix row help-block mtop20">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-feedback">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

            </div>
            <div class="country__col-2">
                <img src="/assets/img/country/country-1.jpg" alt="Vivi">
            </div>
        </div>
    </div>

@stop
