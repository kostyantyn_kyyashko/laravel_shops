<section class="home__sale container">
    <img src="{{ $getCoupon->image }}" alt="{{ $getCoupon->{'name_'.$lng} }}">
    <div class="home__sale-text">
        <div class="home__sale-get">{{ $getCoupon->{'name_'.$lng} }}</div>
        <div class="home__sale-code">{{ $getCoupon->{'text_'.$lng} }}</div>
    </div>
</section>