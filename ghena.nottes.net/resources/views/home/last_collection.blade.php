<section class="home__collections">
    <h2 class="home__collections-title">{{ $vars['last_collections'] }}</h2>
    <div class="container">
        <span class="home__collections-text">{{ $vars['stylish_and_warm'] }}</span>
        <div class="home__collections-slider-wrap">

            <div class="home__card-wide-view hoja">
                <a href="{{ $vars['view_collection_last_collection_home_link'] }}" class="view view--princess view--black">
                    <span class="view__text">{{ $vars['view_collection'] }}</span>
                </a>
            </div>

            <div class="home__collections-slider-arrows">
                <div class="slider-arrows">
                    <div class="slider-arrows__prev">
                        <span class="slider-arrows__prev-text">{{ $vars['prev'] }}</span>
                        <span class="slider-arrows__prev-arrow"></span>
                    </div>
                    <div class="slider-arrows__next">
                        <span class="slider-arrows__next-text">{{ $vars['next'] }}</span>
                        <span class="slider-arrows__next-arrow"></span>
                    </div>
                </div>
            </div>

            <div class="home__collections-slider">
                @php $wide = 1; @endphp
                @foreach($lastCollectionList as $key => $value)

                    <div class="home__collections-item">
                        @if($wide == 3)
                            @php $wide = 0; @endphp
                            <div class="home__card-wide">
                                <a href="{{ route('product',$value->slug) }}">
                                    <img src="{{ $value->path.'_'.'550x370'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}" class="home__card-wide-img">
                                </a>
                            </div>
                        @else
                            <div class="home__card">
                                <a href="{{ route('product',$value->slug) }}">
                                    <img src="{{ $value->path.'_'.'260x370'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}" class="{{ $key == 2 ? 'home__card-wide-img' : 'home__card-img' }}">
                                </a>
                                <h3 class="home__card-title">

                                    <a href="{{ route('product',$value->slug) }}">
                                        {{ $value->{'name_'.$lng} }}
                                    </a>
                                </h3>
                                <span class="home__card-price">{{ $value->{'price_'.$lng} }} {{ $valuta }}</span>

                                <wishlist :product={{ $value->id }} icon="home_last_collection" :favorited={{ in_array($value->id,$wishlistIDs) ? 'true' : 'false' }}></wishlist>

                            </div>
                        @endif
                    </div>
                    @php $wide++; @endphp
                @endforeach

            </div>
        </div>
    </div>
</section>