@if(!$mostPopularList->isEmpty())
    <section class="home__popular">
    <div class="container">
        <h2 class="home__popular-title">{{ $vars['most_popular'] }}</h2>
        <div class="home__popular-slider-wrap">

            <div class="home__popular-slider-arrows">
                <div class="slider-arrows">
                    <div class="slider-arrows__prev">
                        <span class="slider-arrows__prev-text">{{ $vars['prev'] }}</span>
                        <span class="slider-arrows__prev-arrow"></span>
                    </div>
                    <div class="slider-arrows__next">
                        <span class="slider-arrows__next-text">{{ $vars['next'] }}</span>
                        <span class="slider-arrows__next-arrow"></span>
                    </div>
                </div>
            </div>
            <div class="home__popular-slider">
                @foreach($mostPopularList as $value)
                    <div class="home__popular-card">
                        <a href="{{ $value->slug() }}"><img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}"></a>
                        <h3><a href="{{ $value->slug() }}">{{ $value->{'name_'.$lng} }}</a></h3>
                        <span>{{ $value->{'price_'.$lng} }} {{ $valuta }}</span>
                        <wishlist :product={{ $value->id }} icon="home_popular" :favorited={{ in_array($value->id,$wishlistIDs) ? 'true' : 'false' }} ></wishlist>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif