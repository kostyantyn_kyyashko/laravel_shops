<section class="home__catalog">
    <div class="container">
        <div class="home__catalog-row">
            <div class="home__catalog-col-1">
                <div class="home__catalog-collaction-slider">

                    @foreach($shopByCatList as $key => $value)
                        <div class="home__catalog-collaction">
                            <div class="home__catalog-text" data-animation="slideInLeft"  data-delay=".3s" data-animation-out="slideOutLeft">
                                <div class="home__catalog-text-title">{{ $vars['shop_by'] }}</div>
                                {!! $value->product2->{'text_'.$lng} !!}
                            </div>
                            <div class="home__catalog-collaction-img" data-animation="slideInLeft"  data-delay=".3s" data-animation-out="slideOutLeft">
                                <img src="{{ $value->image1 }}" alt="{{ $value->product2->{'name_'.$lng} }}">
                            </div>
                            <div class="home__catalog-collaction-card" data-animation="slideInLeft"  data-delay="0s" data-animation-out="slideOutLeft">
                                <a href="#"><img src="/assets/img/home/catalog-slider-2.jpg" alt="{{ $value->product2->{'name_'.$lng} }}"></a>
                                <h3><a href="#"> {{ $value->product2->{'name_'.$lng} }}</a></h3>
                                <span>{{ $valuta }} {{ $value->product2->{'price_'.$lng} }}</span>
                            </div>
                            <div class="home__catalog-collaction-shtamp hoja">
                                <a href="#" class="view view--princess view--black">
                                    <span class="view__text">View collection</span>
                                </a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="home__catalog-col-2">
                <ul class="home__catalog-list">
                    @foreach($shopByCatList as $key => $value)
                        <li>
                            <a class="{{ $key ==0 ? 'active' : '' }}">
                                {{ $value->{'name_'.$lng} }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="home__catalog-col-3">
                <div class="home__catalog-collaction-slider-right">
                    @foreach($shopByCatList as $key => $value)
                        <div>
                            <div class="home__catalog-card"  data-animation="slideInLeft"  data-delay="0s" data-animation-out="slideOutLeft">
                                <a href="{{ route('product',$value->product1->slug) }}"><img src="{{ $value->product1->image }}" alt="Vivi"></a>
                                <h3>
                                    <a href="{{ $value->product1->slug }}">
                                        {{ $value->product1->{'name_'.$lng} }}
                                    </a>
                                </h3>
                                <span> {{ $valuta }} {{ $value->product1->{'price_'.$lng} }} </span>
                            </div>
                            <div class="home__catalog-img">
                                <img src="{{ $value->image }}" alt="Vivi">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>