<section class="home__welcome-slider">

    @foreach($sliderHomeList as $key => $value)
        <div class="home__welcome">
            <div class="home__welcome-bg">
                <div class="home__welcome-bg-img" data-animation="slideInLeft"  data-delay=".2s" data-animation-out="slideOutRight">
                    <img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                </div>
                <div class="home__welcome-bg-img" data-animation="slideInLeft" data-delay=".1s" data-animation-out="slideOutRight">
                    <img src="{{ $value->image1 }}" alt="{{ $value->{'name_'.$lng} }}">
                </div>
                <div class="home__welcome-bg-img" data-animation="slideInLeft" data-delay=".0s" data-animation-out="slideOutRight">
                    <img src="{{ $value->image2 }}" alt="{{ $value->{'name_'.$lng} }}">
                </div>
            </div>
            <div class="home__welcome-row container">
                <div class="home__welcome-left" data-animation="fadeIn" data-delay="1s">
                    <h1 class="home__welcome-title">{{ $value->{'name_'.$lng} }}</h1>
                    <p class="home__welcome-text">
                        {!! $value->{'text_'.$lng} !!}
                    </p>
                    <div class="home__welcome-number">
                        <span class="home__welcome-prev">0{{ $key }}</span>
                        <span class="home__welcome-next">0{{ $key+1 }}</span>
                        <div class="home__welcome-number-line" data-animation="slideInRight" data-delay="1s"></div>
                    </div>
                </div>
                <div class="home__welcome-right">
                    <div class="home__welcome-view hoja">
                        <a href="{{ $value->{'link_'.$lng} }}" class="view">
                            <span class="view__text">{{ $vars['view_collection'] }}</span>
                        </a>
                    </div>
                    @if($value->product)
                        <div class="home__welcome-img slow" data-animation="fadeInLeft" data-delay="1s">
                            <img src="{{ $value->product->image }}" style="max-height: 500px" alt="{{ $value->{'name_'.$lng} }}">
                        </div>
                        <div class="home__welcome-product" data-animation="fadeIn" data-delay="1s">
                            {{ $value->product->{'name_'.$lng} }}
                        </div>
                        <div class="home__welcome-price" data-animation="fadeIn" data-delay="1s">
                            <span class="home__welcome-full">{{ $value->product->{'price_'.$lng} }} {{ $valuta }}</span>
                            <span class="home__welcome-sale">{{ $value->product->{'price_'.$lng} }} {{ $valuta }}</span>
                        </div>
                        <div class="home__welcome-buttons" data-animation="fadeIn" data-delay="1s">
                            <a class="button" href="{{ route('product',$value->product->slug) }}">{{ $vars['more_about'] }}</a>
                            <a class="button button--fill-white" href="{{ route('product',$value->product->slug) }}">{{ $vars['checkout'] }}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach

</section>