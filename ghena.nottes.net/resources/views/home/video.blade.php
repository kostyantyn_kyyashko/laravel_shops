<section class="home__video container">
    <img src="{{ $playVideo->image }}" alt="Vivi">
    <a href="{{ $playVideo->{'video_'.$lng} }}" data-fancybox class="home__video-play hoja">
        <span>{{ $playVideo->{'name_'.$lng} }}</span>
    </a>
</section>