@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <div id="app">
        <div>
            @include('home/slider_header')

            @include('home/last_collection')

            @include('home/most_popular_categories')

            @include('home/buy_the_look')

            @include('home/shop_buy_categories')

            @include('home/most_popular_products')

            @include('home/video')

            @include('home/discount')
        </div>
    </div>

@stop
@section('footer_scripts')
    <script src="{{ asset('assets/js/manifest.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
@stop