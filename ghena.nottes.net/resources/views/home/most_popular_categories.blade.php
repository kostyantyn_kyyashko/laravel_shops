<section class="home__boy">
    <div class="container">
        <div class="home__boy-row">
            <div class="home__boy-left">
                <img src="{{ $mostPopularCat->image }}" alt="{!! $mostPopularCat->{'name_'.$lng}  !!}">
            </div>
            <div class="home__boy-right">
                <div class="home__boy-title-wrap">
                    <h2 class="home__boy-title">
                        {!! $mostPopularCat->{'name_'.$lng}  !!}
                    </h2>
                </div>
                <div class="home__boy-new">{!! $mostPopularCat->{'name2_'.$lng}  !!}</div>
                <p class="home__boy-text">
                    {!! $mostPopularCat->{'text_'.$lng}  !!}
                </p>
                <div class="home__boy-btn">
                    <a href="{{ $mostPopularCat->{'link_'.$lng} }}" class="button button--fill-black">{{ $vars['view_more'] }}</a>
                </div>
            </div>
        </div>
    </div>
</section>