@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')
    <form action="{{ route('delivery') }}" method="get" enctype="multipart/form-data">
        <div class="container" id="app">
            <div class="cart-2__top-row">
                <div class="cart-2__top-items">{{ count($cart) }} items</div>
                <div class="cart__top-title">cart</div>
                <div class="cart-2__top-widhlist"><a href="@guest {{ route('login') }} @else {{ route('user.wishlist') }}  @endif">wishlist</a></div>
            </div>
            <div class="cart-2__row">
                    @if($cart->isNotEmpty())
                            {{ csrf_field() }}
                            <input type="hidden" class="current_url" value="{{ url()->current() }}">
                            @foreach($cart as $key => $row)
                                <?php
                                $product = \App\Models\Product::find($row->id);
                                $value = $product;
                                ?>
                                <div class="cart-2__item">
                                    <div class="cart-card">
                                        <div class="cart-card__img">
                                            <a href="{{ $value->slug() }}">
                                                <img src="{{ $value->path.'_'.'260x370'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}">
                                            </a>
                                            <button type="button" class="cart-card__close ajax_remove_from_cart" data-rowId="{{ $row->rowId }}" ></button>
                                        </div>
                                        <div class="cart-card__title"><a href="{{ $value->slug() }}">{{ $value->{'name_'.$lng} }}</a></div>
                                        <div class="cart-card__price"><span class="price_item_{{ $row->id }}">{{ $row->price*$row->qty }}</span> {{ $valuta }}</div>
                                        <div class="cart-card__color">
                                            <div class="radio-color">
                                                @foreach($value->colors as $kcolor => $color)
                                                    <div class="radio-color__item radio-color__item__{{ $color->id }} ch_label_radio_look ch_label_{{ $value->id }}"
                                                         data-id="{{ $color->id }}" data-product_id="{{ $value->id }}"
                                                    >
                                                        <input id="radio-color-{{ $value->id }}_{{ $color->id }}"
                                                               type="radio"
                                                               name="color_{{ $value->id }}"
                                                               data-id="{{ $color->id }}"
                                                               value="{{ $value->id+$color->id }}"
                                                               data-value="{{ $color->id }}"
                                                               data-product_id="{{ $value->id }}"
                                                               data-rowId="{{ $row->rowId }}"
                                                               class="product_color cart_product_color product_rowId_{{ $value->id }}"
                                                               @if($color->id== $row->options->color_id)
                                                                   checked
                                                                @endif
                                                        >
                                                        <label for="radio-color-{{ $value->id }}_{{ $color->id }}"></label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="cart-card__size">
                                            <div class="select-size">
                                                <div class="select-size__value active">
                                                    @foreach($value->sizes as $ksize => $size)
                                                       @if($size->id == $row->options->size_id ) {{ $size->{'name_'.$lng} }} @endif
                                                    @endforeach
                                                </div>
                                                <ul class="select-size__list">
                                                    @foreach($value->sizes as $ksize => $size)
                                                        <li>
                                                            <span
                                                                    data-rowId="{{ $row->rowId }}"
                                                                    class="select-size__item-value cart_product_size product_rowId_{{ $value->id }}"
                                                                    data-product_id="{{ $value->id }}"
                                                                    data-value="{{ $size->id }}"
                                                                    data-id="{{ $size->id }}"
                                                            >
                                                                {{ $size->{'name_'.$lng} }}
                                                            </span>
                                                            @if($size->pivot->qt == 0)
                                                                <span class="select-size__stock">Out of stock</span>
                                                            @endif
                                                            @if($size->pivot->qt > 0 && $size->pivot->qt < 5)
                                                                <span class="select-size__left">{{  $size->pivot->qt }} models left</span>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="cart-card__quantity">
                                            <div class="product-quantity">
                                                <button type="button"
                                                        data-price="{{ $row->price }}"
                                                        data-id="{{ $row->id }}"
                                                        data-rowId="{{ $row->rowId }}"
                                                        class="product-quantity__bnt product-quantity__bnt--minus product_rowId_{{ $value->id }}"
                                                >-</button>
                                                <input class="product-quantity__input" type="text" value="{{ $row->qty }}">
                                                <button type="button"
                                                        class="product-quantity__bnt product-quantity__bnt--plus product_rowId_{{ $value->id }}"
                                                        data-price="{{ $row->price }}"
                                                        data-id="{{ $row->id }}"
                                                        data-rowId="{{ $row->rowId }}"
                                                >+</button>
                                            </div>
                                        </div>
                                        <div class="cart-card__wishlist-btn">
                                            <wishlist-button :product='{{ $value->id }}' title_remove="{{ $vars['remove_from_wishlist'] }}" :title_add="'{{ $vars['add_to_wishlist'] }}'" :favorited={{ in_array($value->id,$wishlistIDs) ? 'true' : 'false' }} ></wishlist-button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                    @else
                        <div class="order-row last tex-center">
                            <div class="text-success bold">
                                Empty cart
                            </div>
                        </div>
                    @endif

                </div>
        </div>

        <footer class="cart-footer">
        <div class="cart-footer-wrap">
            <div class="cart-footer__text">
                <span class="cart-footer__total">Total</span>
                <span class="cart-footer__price">
                    <b class="cart_total">{{ Cart::total() }}</b> {{ $valuta }}
                </span>
            </div>
            <div class="cart-footer__btn">
                <a href="{{ route('delivery') }}" class="button button--fill-black">continue</a>
            </div>
        </div>
    </footer>

    </form>
@stop
@section('footer_scripts')
    <script src="{{ asset('assets/js/manifest.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
@stop
@section('header_styles')
    @foreach($colors as $kcolor => $color)
        <style>
            .radio-color__item__<?=$color->id?> label::before,
            .radio-color__item__<?=$color->id?> span::before
            {
                background-color: {{ $color->code }} !important;
            }
        </style>
    @endforeach
@stop
