@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('cart/header_steps')

    <form action="{{ route('doCheckout') }}" class="delivery-info-form" method="post" >
        {{ csrf_field() }}
        <div class="container cart-4-container">
            <h2 class="cart-4__title">ALEGEŢI METODA DE LIVRARE</h2>
            <div class="cart-4__wrap">
                @php $shipping =0 @endphp
                @if($shippingList)
                    <div class="cart-4__radio-wrap">
                    @foreach($shippingList as $key => $value)
                        <div class="cart-4__radio-item">
                            <div class="cart-4__radio-item-wrap shipping_method" data-price="{{ $value->{'price_'.$lng} > 0 ? $value->{'price_'.$lng} : 0 }}" >
                                <input id="cart-4__radio-item-{{ $value->id }}"
                                       type="radio"
                                       name="shipping_id"
                                       value="{{ $value->id }}"
                                       @if($key == 0) checked @php $shipping = $value->{'price_'.$lng} > 0 ? $value->{'price_'.$lng} : 0; @endphp @endif>
                                <label for="cart-4__radio-item-{{ $value->id }}">{{ $value->{'name_'.$lng} }}</label>
                            </div>
                            <span class="cart-4__radio-item-gratis">
                                {{ $value->{'price_'.$lng} > 0 ? $value->{'price_'.$lng} : $vars['gratis'] }} {{ $valuta }}
                            </span>
                        </div>
                    @endforeach
                </div>
                @endif
                <div class="cart-4__products-quantity">{{ count($cart) }} items</div>
                @if($cart->isNotEmpty())
                    <div class="cart-4__products-wrap">
                        @foreach($cart as $key => $row)
                            <div class="cart-4__products-item">
                                <img src="{{ $row->options->image }}" alt="Vivi">
                            </div>
                        @endforeach
                    </div>
                @endif

            </div>


        </div>

        <footer class="cart-footer">
            <div class="cart-footer-wrap">
                <div class="cart-footer__text">
                    <span class="cart-footer__total">Total</span>
                    <span class="cart-footer__price">
                       <b class="cart_total">{{ Cart::total()+$shipping }}</b> {{ $valuta }}
                    </span>
                </div>
                <input type="hidden" class="cart_total_value" value="{{ Cart::total() }}" >
                <div class="cart-footer__btn">
                    <button type="submit" class="button button--fill-black btn-delivery-info">continue</button>
                </div>
                <input type="hidden" name="user_id"  value="{{ @Auth::user()->id }}" >
                <input type="hidden" name="first_name" value="{{ isset($_GET['first_name']) ? $_GET['first_name'] : '' }}" >
                <input type="hidden" name="last_name" value="{{ isset($_GET['last_name']) ? $_GET['last_name'] : '' }}" >
                <input type="hidden" name="address1" value="{{ isset($_GET['address1']) ? $_GET['address1'] : '' }}" >
                <input type="hidden" name="address2" value="{{ isset($_GET['address2']) ? $_GET['address2'] : '' }}" >
                <input type="hidden" name="postal_code" value="{{ isset($_GET['postal_code']) ? $_GET['postal_code'] : '' }}" >
                <input type="hidden" name="location" value="{{ isset($_GET['location']) ? $_GET['location'] : '' }}" >
                <input type="hidden" name="district" value="{{ isset($_GET['district']) ? $_GET['district'] : '' }}" >
                <input type="hidden" name="country" value="{{ isset($_GET['country']) ? $_GET['country'] : '' }}" >
                <input type="hidden" name="email" value="{{ isset($_GET['email']) ? $_GET['email'] : '' }}" >
                <input type="hidden" name="phone" value="{{ isset($_GET['phone']) ? $_GET['phone'] : '' }}" >
            </div>
        </footer>

    </form>

@stop
