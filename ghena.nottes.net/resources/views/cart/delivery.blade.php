@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('cart/header_steps')

    <form action="{{ route('checkout') }}" class="delivery-info-form" method="get" >

        <div class="container cart-3-container">
            <h2 class="cart-3__title">SCRIEŢI DETALIILE LIVRĂRII</h2>
            <div class="registration-form-wrap">
                <div action="#" class="registration-form">

                    <div class="registration-form__radio-wrap">
                        <div class="registration-form__radio-societate">
                            <input id="registration-form-societate" type="radio" name="person_type" value="1" @guest checked @else {{ route('user.profile') }} @if(Auth::user()->person_type == 1) checked @endif  @endif >
                            <label for="registration-form-societate">Persoană fizică</label>
                        </div>

                        <div class="registration-form__radio-person">
                            <input id="registration-form-person" type="radio" name="person_type" value="2"  @auth @if(Auth::user()->person_type == 2) checked @endif @endauth>
                            <label for="registration-form-person">Societate</label>
                        </div>
                    </div>
                    <div class="registration-form__inputs-row">
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Nume" value="{{ @Auth::user()->first_name }}" name="first_name" required="required">
                            <div class="registration-form__item-placeholder">Nume</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Prenume" name="last_name" required="required">
                            <div class="registration-form__item-placeholder">Prenume</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Adressa1" name="address1">
                            <div class="registration-form__item-placeholder">Adressa1</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Adressa2 (optional)" name="address2">
                            <div class="registration-form__item-placeholder">Adressa2 (optional)</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Post code" name="postal_code">
                            <div class="registration-form__item-placeholder">Post code</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Localitatea" name="location">
                            <div class="registration-form__item-placeholder">Localitatea</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Judet" name="district">
                            <div class="registration-form__item-placeholder">Judet</div>
                        </div>
                        <div class="registration-form__item-wrap registration-form__item-wrap--country">
                            <input class="registration-form__item" type="text" placeholder="Country" name="country">
                            <div class="registration-form__item-placeholder">Country</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="E-mail" name="email" value="{{ @Auth::user()->email }}" required="required">
                            <div class="registration-form__item-placeholder">E-mail</div>
                        </div>
                        <div class="registration-form__item-wrap registration-form__item-wrap--number" >
                            <div class="registration-form__item-prefix">+40</div>
                            <input class="registration-form__item" type="text" placeholder="Number" name="phone" value="{{ @Auth::user()->phone }}" required="required">
                        </div>
                    </div>
                    <div class="registration-form__checkbox-wrap">
                        <input id="registration-form-checkbox" type="checkbox" name="term1" >
                        <label for="registration-form-checkbox" class="checkmark">
                            Am citit si înţeleg <a href="{{ route('info',['privacy-policy']) }}">Politica de confidenţialitate</a> şi privind Cookie-urile.
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <footer class="cart-footer">
            <div class="cart-footer-wrap">
                <div class="cart-footer__text">
                    <span class="cart-footer__total">Total</span>
                    <span class="cart-footer__price"><b>{{ Cart::total() }}</b> {{ $valuta }}</span>
                </div>
                <div class="cart-footer__btn">
                    <button type="button" class="button button--fill-black btn-delivery-info">continue</button>
                </div>
            </div>
        </footer>

    </form>

@stop
