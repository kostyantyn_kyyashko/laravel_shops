<header class="header header--cart-header">
    <div class="container header__row-wrap">
        <div class="header__row">
            <a href="/{{ $lng }}" class="header__logo"><img src="/assets/img/logo.png" alt="Vivi"></a>
        </div>

        <div class="cart-header">
            <div class="cart-header__stage">
                <div class="cart-header__indicator cart-header__indicator--1 {{ isset($step1) ? 'active' : '' }}"></div>
                <div class="cart-header__indicator cart-header__indicator--2 {{ isset($step2) ? 'active' : '' }}"></div>
            </div>
            <div class="cart-header__stage">
                <div class="cart-header__indicator cart-header__indicator--3 {{ isset($step3) ? 'active' : '' }}"></div>
            </div>
        </div>
    </div>

</header>