@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $vars['cautare'] }}| {{ ENV('APP_NAME') }}
@stop


{{-- content --}}
@section('content')

    <main class="products" id="app" >
        <input type="hidden" class="products_url" value="{{ url()->current() }}">

        <div class="container">
            <h2 class="products__title">
                {{ $vars['cuvantul_cheie'] }}: <strong>{{ $s }}</strong>
            </h2>
            <div class="products__top-row">

                {{--PAGINATION--}}
                <div class="products__top-col-3 product_pagination_area">
                    @include('category/_common_list/product_pagination')
                </div>

            </div>

            <div class="products__row">

            </div>

            <div class="product_list_area products__row">
                @if(!empty($productList))
                    @include('category/_common_list/product_list')
                @else
                    <div class="clearfix info">
                        {{ $vars['nu_a_fost_gasit_nici_un_rezultat'] }}
                    </div>
                @endif
            </div>


        </div>

    </main>

@stop

