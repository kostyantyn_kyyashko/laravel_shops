@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('cart/header_steps')

    <form action="{{ route('checkout') }}" class="delivery-info-form" method="get" >

        <div class="container cart-6-container">
            <div class="cart-6__wrap">

                <div class="cart-6__order-number">order Nr : {{ $order->invoiceID }}</div>
                <div class="cart-6__row">

                    <div class="cart-6__item">
                        <div class="cart-6__item-title">Main information</div>
                        <div class="cart-6__item-value-wrap">
                            <span class="cart-6__item-value">{{ $order->first_name.' '.$order->last_name }}</span>
                            @if($order->address1)
                                <span class="cart-6__item-value">{{ $order->address1 }}</span>
                            @endif
                            @if($order->address2)
                                <span class="cart-6__item-value">{{ $order->address2 }}</span>
                            @endif
                            @if($order->postal_code)
                                <span class="cart-6__item-value">{{ $order->postal_code }}</span>
                            @endif
                            @if($order->phone)
                                <span class="cart-6__item-value">{{ $order->phone }}</span>
                            @endif
                            @if($order->email)
                                <span class="cart-6__item-value">{{ $order->email }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="cart-6__item">
                        <div class="cart-6__item-title">Delivery</div>
                        <div class="cart-6__item-value-wrap">
                            @if($order->country)
                                <span class="cart-6__item-value">{{ $order->country }}</span>
                            @endif
                            @if($order->location)
                                <span class="cart-6__item-value">{{ $order->location }}</span>
                            @endif
                            @if($order->district)
                                <span class="cart-6__item-value">{{ $order->district }}</span>
                            @endif
                            <span class="cart-6__item-value">{{ $shipping->{'name_'.$lng} }}: {{ $shipping->{'price_'.$lng} }} {{ $valuta }}</span>
                            <span class="cart-6__item-value">Arrival date :{{ date('d.m.Y',strtotime($order->delivery_date)) }} </span>
                            <span class="cart-6__item-value">Departure date : {{ date('d.m.Y',strtotime($order->shipping_date)) }}</span>
                        </div>
                    </div>

                </div>
                @if($order->products)
                    <div class="cart-6__items-quantity">{{ count($order->products) }} Items</div>
                    <div class="cart-6__product-row">
                        @foreach($order->products as $value)
                            <div class="cart-6__product-item">
                            <div class="cart-card">
                                <div class="cart-card__img">
                                    <a href="{{ $value->slug() }}"><img src="{{ $value->path.'_'.'260x370'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}"></a>
                                </div>
                                <div class="cart-card__title">
                                    <a href="{{ $value->slug() }}">
                                        {{ $value->{'name_'.$lng} }}
                                    </a>
                                </div>
                                <div class="cart-card__price">{{ $value->{'price_'.$lng} }} {{ $valuta }}</div>
                                @if($value->colors)
                                    <div class="cart-card__color">
                                        <div class="radio-color">
                                            <div class="radio-color__item radio-color__item__{{ $value->pivot->color_id }} radio-color__item--1">
                                                <span class="chosen"></span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($value->sizes)
                                    <div class="cart-card__size cart-card__size--chosen">
                                        @foreach($value->sizes as $ksize)
                                            @if($ksize->id == $value->pivot->size_id) {{ $ksize->{'name_'.$lng} }} @endif
                                        @endforeach
                                    </div>
                                @endif
                                <div class="cart-card__quantity cart-card__quantity--chosen">QTY: {{ $value->pivot->qt }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                @endif

            </div>


        </div>

        <footer class="cart-footer">
            <div class="cart-footer-wrap">
                <div class="cart-footer__text">
                    <span class="cart-footer__total">Total</span>
                    <span class="cart-footer__price">{{ $order->total }}</b> {{ $valuta }}</span>
                </div>
            </div>
        </footer>

    </form>

@stop
@section('header_styles')
    @foreach($colors as $kcolor => $color)
        <style>
            .radio-color__item__<?=$color->id?> label::before,
            .radio-color__item__<?=$color->id?> span::before
            {
                background-color: {{ $color->code }} !important;
            }
        </style>
    @endforeach
@stop