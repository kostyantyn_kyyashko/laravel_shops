@extends('layouts/auth')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <div class="login">
        <div class="login__row">
            <div class="login__col-1">
                <a href="/" class="header__logo"><img src="/assets/img/logo.png" alt="Vivi"></a>
                <div class="login__title">Forgot Password</div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="login__form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input class="login__form-email" type="text" name="email" value="{{ old('email') }}" required="required" placeholder="E-mail">


                    <div class="login-wrap">
                        <button type="submit" class="button button--fill-black"> Send Password Reset Link</button>
                    </div>
                </form>

            </div>
            <div class="login__col-2">
                <img src="/assets/img/country/log-in.jpg" alt="Vivi">
            </div>
        </div>
    </div>

@stop
