@extends('layouts/auth')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <div class="login">
        <div class="login__row">
            <div class="login__col-1">
                <a href="/" class="header__logo"><img src="/assets/img/logo.png" alt="Vivi"></a>
                <div class="login__title">AUTENTIFICARE</div>
                <a href="{{ route('register') }}" class="login__no-account">Don’t have an account?</a>
                <form class="login__form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input class="login__form-email" type="text" name="email" required="required" placeholder="E-mail">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                    <input class="login__password" type="password" name="password" required="required" placeholder="Password">

                    <div class="login-wrap">
                        <a href="{{ route('password.request') }}" class="login__forgot">Forgot Password?</a>
                        <button type="submit" class="button button--fill-black">go</button>
                    </div>
                </form>

            </div>
            <div class="login__col-2">
                <img src="/assets/img/country/log-in.jpg" alt="Vivi">
            </div>
        </div>
    </div>

@stop
