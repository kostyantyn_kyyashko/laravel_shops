@extends('layouts/auth')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <a href="/" class="registration__logo">
        <img src="/assets/img/logo.png" alt="Vivi">
    </a>
    <h2 class="registration__title">Introduceţi-vă datele personale</h2>
    <div class="registration-form-wrap">
        <form action="{{ route('register') }}" method="post" class="registration-form">
            {{ csrf_field() }}

            <div class="registration-form__radio-wrap">
                <div class="registration-form__radio-societate">
                    <input id="registration-form-societate" type="radio" name="person_type" value="1" checked>
                    <label for="registration-form-societate">Persoană fizică</label>
                </div>

                <div class="registration-form__radio-person">
                    <input id="registration-form-person" type="radio" name="person_type" value="2">
                    <label for="registration-form-person">Societate</label>
                </div>
            </div>
            <div class="registration-form__inputs-row">
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text" name="first_name" placeholder="Nume" required="required" value="{{ old('first_name') }}">
                    <div class="registration-form__item-placeholder">Nume</div>
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text" name="last_name" placeholder="Prenume" required="required" value="{{ old('last_name') }}" >
                    <div class="registration-form__item-placeholder">Prenume</div>
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text" name="address1" placeholder="Adressa1" value="{{ old('address1') }}">
                    <div class="registration-form__item-placeholder">Adressa1</div>
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text" name="address2" placeholder="Adressa2 (optional)" value="{{ old('address2') }}">
                    <div class="registration-form__item-placeholder">Adressa2 (optional)</div>
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text" name="postal_code"  placeholder="Post code" value="{{ old('postal_code') }}">
                    <div class="registration-form__item-placeholder">Post code</div>
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text" name="location" placeholder="Localitatea" value="{{ old('location') }}">
                    <div class="registration-form__item-placeholder">Localitatea</div>
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="text"  name="district" placeholder="Judet" value="{{ old('district') }}">
                    <div class="registration-form__item-placeholder">Judet</div>
                </div>
                <div class="registration-form__item-wrap registration-form__item-wrap--country">
                    <input class="registration-form__item" type="text"  name="country" placeholder="Country" value="{{ old('country') }}">
                    <div class="registration-form__item-placeholder">Country</div>
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="email" name="email" value="{{ old('email') }}" required="required" placeholder="E-mail">
                    <div class="registration-form__item-placeholder">E-mail</div>
                </div>
                <div class="registration-form__item-wrap registration-form__item-wrap--number">
                    <div class="registration-form__item-prefix">+40</div>
                    <input class="registration-form__item" type="text"  name="phone" placeholder="Number" value="{{ old('phone') }}">
                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="password" name="password" value="{{ old('password') }}" required="required" placeholder="Password">
                    <div class="registration-form__item-placeholder">Password</div>

                </div>
                <div class="registration-form__item-wrap">
                    <input class="registration-form__item" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" required="required" placeholder="Confirm Password">
                    <div class="registration-form__item-placeholder">Confirm Password</div>
                </div>

            </div>
            <div class="registration-form__checkbox-wrap">
                <input id="registration-form-checkbox" type="checkbox" name="terms" >
                <label for="registration-form-checkbox">Am citit si înţeleg <a href="{{ route('info',['privacy-policy']) }}">Politica de confidenţialitate</a> şi privind Cookie-urile.</label>
            </div>
            <div class="registration-form__submit-wrap">
                <button type="submit" class="button button--fill-black">register</button>
            </div>
            <div class="col-sm-12 col-md-12 clearfix row help-block mtop20">
                @if ($errors->any())
                    <div class="alert alert-danger alert-feedback">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

        </form>
    </div>

@stop
