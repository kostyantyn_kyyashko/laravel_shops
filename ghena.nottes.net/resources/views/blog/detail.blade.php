@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $blog->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="blog-detail">
        <div class="container">
            <div class="blog-detail__wrap">
                <h1 class="blog-detail__title">
                    {{ $blog->{'name_'.$lng} }}
                </h1>
                <div class="blog-detail__text">
                    {!! $blog->{'short_'.$lng} !!}
                </div>
                <div class="blog-detail__date">
                    {{ date('d F, Y',strtotime($blog->date)) }}
                </div>
                <div class="blog-detail__top-img">
                    <img src=" {{ $blog->image }}" alt=" {{ $blog->{'name_'.$lng} }}">
                </div>
                <div class="blog-detail__row">
                    <div class="blog-detail__left">
                        <div class="blog-detail__about-title"> {{ $blog->{'name2_'.$lng} }}</div>
                        <div class="blog-detail__about-text">
                            {!! $blog->{'text_'.$lng} !!}
                        </div>
                       <div class="blog-detail__about-new">{{ $blog->{'name3_'.$lng} }}</div>
                    </div>
                    @if($blog->image1)
                        <div class="blog-detail__right">
                             <img src="{{ $blog->image1 }}" alt=" {{ $blog->{'name_'.$lng} }}">
                        </div>
                    @endif
                </div>
                <div class="blog-detail__quote-wrap">
                    <div class="blog-detail__quote">
                        {!! $blog->{'text2_'.$lng} !!}
                    </div>
                    <div class="blog-detail__quote-avtor">
                        {{ $blog->{'name3_'.$lng} }}
                    </div>
                </div>

            </div>
            <h2 class="blog__popular-title">
                {{ $vars['popular_news'] }}
            </h2>
            <div class="blog__row blog-detail__row-bottom">

                <div class="blog__pagination">
                    {{ $mostPopularList->links('layouts.pagination_blog') }}
                </div>

                @foreach($mostPopularList as $key => $value)
                    @include('blog.item-list')
                @endforeach

            </div>
        </div>
    </main>

@stop

