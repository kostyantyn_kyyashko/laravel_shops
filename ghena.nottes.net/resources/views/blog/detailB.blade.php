@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $blog->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="blog-page">

        <div class="container">
            <div class="flex-row justify-space align-start">

                <div class="blog-content">

                    <a href="{{ route('blog',[$location->slug]) }}" class="link-back">{{ $vars['inapoi_la_blog'] }}</a>

                    <div class="image">
                        <img src="{{ $blog->image }}" alt="">
                        <div class="label-green">{{ $vars['new'] }}</div>
                    </div>
                    <div class="wrapper">
                        <h1>{{ $blog->{'name_'.$lng} }}</h1>
                        <div class="tag-announces">{{ $vars['anunturi'] }}</div>
                        <div class="date"> {{ strtoupper(date('d F Y',strtotime($blog->date))) }}</div>
                        {!! $blog->{'text_'.$lng} !!}
                    </div>
                </div>
                @if($otherArticles)
                    <div class="blog-sidebar">
                        @foreach($otherArticles as $value)
                            @include('blog/item-list')
                        @endforeach
                    </div>
                @endif

            </div>
        </div>

    </main>

@stop

