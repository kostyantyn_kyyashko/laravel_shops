@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $blog->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="blog">
        <div class="container">
            <div class="blog__top" style='background:#000 url("{{ $blog->image }}") no-repeat center'>
                <h1 class="blog__title">{{ $blog->{'name2_'.$lng} }}</h1>
                    <div class="blog__top-btn">
                        <a href="{{ $blog->{'link_'.$lng} }}" class="button button--fill-black">
                            {{ $vars['view_more'] }}
                        </a>
                    </div>
            </div>
            <h2 class="blog__popular-title">{{ $vars['popular_news'] }}</h2>
            <div class="blog__row">

                <div class="blog__pagination">
                    {{ $newsList->links('layouts.pagination_blog') }}
                </div>

                @foreach($newsList as $key => $value)
                    @include('blog.item-list')
                @endforeach

            </div>
            @if($collection)
                <div class="blog__row-bottom">
                    <div class="blog__bottom-left">
                        <img src="{{ $collection->image }}" alt="{{ $collection->{'name_'.$lng} }}">
                    </div>
                    <div class="blog__bottom-right">
                        <h2 class="blog__bottom-title">
                            {{ $collection->{'name_'.$lng} }}
                        </h2>
                        <div class="blog__bottom-btn">
                            <a href="{{ $collection->{'link_'.$lng} }}" class="button button--fill-black">{{ $vars['view_more'] }}</a>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </main>

@stop

