<div class="blog__item">
    <div class="news">
        <div class="news__img">
            <img src="{{ $value->path.'_'.'356x218'.'.'.$value->ext }}" alt="{{ $value->{'name_'.$lng} }}">
        </div>
        <h3 class="news__title">
            {{ $value->{'name_'.$lng} }}
        </h3>
        <div class="news__text">
            {!! str_limit($value->{'short_'.$lng},130) !!}
        </div>
        <div class="news__date">
            {{ date('d F, Y',strtotime($value->date)) }}
        </div>
        <div class="news__bottom">
            <a href="{{ route('blogDetail',[$value->slug]) }}" class="news__btn">
                {{ $vars['view_more'] }}
            </a>
        </div>
    </div>
</div>