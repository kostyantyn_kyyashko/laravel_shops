@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="collections-gallery">
        <div class="container container--1530">
            <h2 class="collections-gallery__title">
                {{ $collection->{'name_'.$lng} }}
            </h2>
        <div class="collections-gallery__top-row">
            <div class="collections-gallery__top-col-1">

                {{ $collectionGallery->links('layouts.pagination_blog') }}

                </div>
                <div class="collections-gallery__top-col-2">

                    <ul class="products-nav">
                        @foreach($collectionList as $value)
                            <li>
                                <a href="{{ route('collectionDetail',$value->slug) }}" class="{{  $collection->id == $value->id ? 'active' : '' }}">
                                    {{ $value->{'name_'.$lng} }}
                                </a>
                            </li>
                        @endforeach
                    </ul>

                </div>
                <div class="collections-gallery__top-col-3">

                    <a href="{{ $collection->{'link_'.$lng} }}" class="collections-gallery__store">{{ $vars['view_in_store'] }}</a>

                </div>
            </div>


            <div class="collections-gallery__row">
                <div class="collections-gallery__row-left">

                    @foreach($collectionGallery as $key => $value)
                        @if($key <= 6)
                            @if($key == 3)
                                <div class="collections-gallery__item collections-gallery__item--width">
                                    <img src="{{ $collection->image1 }}" alt="{{ $collection->{'name_'.$lng} }}">
                                    <a data-fancybox href="{{ $collection->{'video_'.$lng} }}" class="home__video-play hoja">
                                        <span>Play video</span>
                                    </a>
                                </div>
                                <div class="collections-gallery__item">
                                    <a href="{{ $value->path }}" data-fancybox="images">
                                        <img src="{{ $value->path }}" alt="{{ $collection->{'name_'.$lng} }}">
                                    </a>
                                </div>
                            @else
                                <div class="collections-gallery__item">
                                    <a href="{{ $value->path }}" data-fancybox="images">
                                        <img src="{{ $value->path }}" alt="{{ $collection->{'name_'.$lng} }}">
                                    </a>
                                </div>
                            @endif
                        @endif
                    @endforeach

                </div>



                <div class="collections-gallery__row-right">

                    @foreach($collectionGallery as $key => $value)
                        @if($key > 6)
                            <div class="collections-gallery__item @if($key == 7) collections-gallery__item--height @endif">
                                <a href="{{ $value->path }}" data-fancybox="images">
                                    <img src="{{ $value->path }}" alt="{{ $collection->{'name_'.$lng} }}">
                                </a>
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </main>

@stop

