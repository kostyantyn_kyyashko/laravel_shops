@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="collections">
        <div class="collections__row">
            @foreach($collectionList as $key=>$value)
                <div class="collections__item">
                    <div class="collections__img">
                        <img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                    </div>
                    <h2 class="collections__title">{{ $value->{'name_'.$lng} }}</h2>
                    <div class="collections__shtamp hoja">
                        <a href="{{ route('collectionDetail',$value->slug) }}" class="view view--princess view--princess-{{ $key+1 }}">
                            <span class="view__text">{{ $vars['view_collection'] }}</span>
                        </a>
                    </div>
                </div>
                <style>
                    .view.view--princess-<?=$key+1?>::after {
                        content: '<?=$value->{'name2_'.$lng}?>';
                    }
                </style>
            @endforeach
        </div>
    </main>

@stop

