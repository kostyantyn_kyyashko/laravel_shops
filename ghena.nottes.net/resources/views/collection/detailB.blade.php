@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="about-page">
        <div class="container">

            <div class="about-wrapper">

                @if($page->image)
                    <div class="first-image" style="background-image: url({{ $page->image }});">
                        <div class="logo">
                            <img src="/i/logo-salat.svg" alt="">
                        </div>
                    </div>
                @endif

                <div class="wrapper">

                    <h1>{{ $page->{'name_'.$lng} }}</h1>

                    {!! $page->{'text_'.$lng} !!}
                </div>

            </div>

        </div>
    </main>


@stop

