<div class="filter">
    <?php
    // search gets
    $filters = [
        'sizes' => isset($_GET['sizes']) ? $_GET['sizes'] : [],// Marimi
    ];
    ?>
    <div class="filter__size filter__item">
        <div class="filter__title">Size</div>
        <ul class="filter__size-list"  >
            <li
                    v-for="size in sizeList"
                    @click="addRemovetoFilterSizes(size.id)"
            >
                <a
                        :class="{ active: isSizeActive(size.id) }"
                        href="javascript:void(0)"
                >
                    @{{ size.name_$lng }}
                </a>
            </li>
        </ul>
        <a href="#" class="filter__more">view more</a>
    </div>


    <div class="filter__color filter__item">
        <div class="filter__title">Color</div>
        <ul class="filter__color-list">
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
        </ul>
    </div>


    <div class="filter__model filter__item">
        <div class="filter__title">Model</div>
        <ul class="filter__size-list">
            <li><a class="active">dresses</a></li>
            <li><a href="#">pants</a></li>
            <li><a href="#">skirt</a></li>

            <li class="filter__more-items"><a href="#">pants</a></li>
            <li class="filter__more-items"><a href="#">skirt</a></li>

        </ul>
        <a href="#" class="filter__more">view more</a>
    </div>

    <div class="filter__collections filter__item">
        <div class="filter__title">Colections</div>
        <ul class="filter__collections-list">
            <li><a class="active">vivi princess</a></li>
            <li><a href="#">back to school</a></li>
            <li><a href="#">summer day</a></li>
        </ul>
    </div>

    <div class="filter__price filter__item">
        <div class="filter__title">Price</div>
        <div class="filter__range-wrap">
            <input type="text" class="filter__range">
        </div>
    </div>
        <a href="javascript:void(0)" class="newFunc" @click="newFunc()" >Call</a>

</div>