<div class="products__top-col-2">

    <ul class="products-nav">
        @foreach($subCategories as $key => $value)
            <li>
                <a
                        class="{{ $subCategory && $subCategory->id == $value->id ? 'active' : '' }}"
                        href="{{ route('subcategory',[$category->slug,$value->slug]) }}"
                >
                    {{ $value->{'name_'.$lng} }}
                </a>
            </li>
        @endforeach
    </ul>
    <ul class="crumbs">
        <li><a href=" {{ URL('/'.$lng) }}">{{ $vars['home'] }} / </a></li>
        <li><a href="{{ route('category',[$value->slug]) }}"> {{ $category->{'name_'.$lng} }} @if($subCategory) / @endif </a></li>
        @if($subCategory)
            <li><a class="active">{{ $subCategory->{'name_'.$lng} }}</a></li>
        @endif
    </ul>

</div>