@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <div class="container">
        <form action="{{ route('addLookListToCart') }}" method="post">
            {!! csrf_field() !!}
            @foreach($productList as $key => $value)
                @include('product/item-list-detail')
            @endforeach
            <div class="cart__add-cart">
                <button type="submit" class="button button--fill-black">Add to cart</button>
            </div>
        </form>
    </div>


@stop

