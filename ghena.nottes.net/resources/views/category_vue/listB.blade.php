@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('layouts/category-menu')

    <main class="catalog-page">
        <div class="container">

            <div class="flex-row sm-slider-catalog ">
                @foreach($productList as $value)
                    @include('product/item-list')
                @endforeach
            </div>
            <div class="pagination">
                {{ $productList->links('layouts.pagination') }}
            </div>

        </div>
    </main>

@stop

