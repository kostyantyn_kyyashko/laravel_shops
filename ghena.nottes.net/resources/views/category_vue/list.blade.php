@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="products" id="app">
        <input type="hidden" class="products_url" value="{{ url()->current() }}">
        <products-list inline-template >

            <div class="container">
                <h2 class="products__title">
                    {{ $vars['produse'] }} {{ $categoryTitle }}
                </h2>
                <div class="products__top-row">
                    {{--FILTER TITLE--}}
                    <div class="products__top-col-1">

                        <div class="products__filter">
                            <div class="filter-btn">
                                <span class="filter-btn__close"></span>
                                <span class="filter-btn__text">{{ $vars['filter'] }}</span>
                            </div>
                        </div>

                    </div>
                    {{--SUB CATEGORIES--}}
                    @include('category/_common_list/sub_category')

                    {{--PAGINATION--}}
                    <div class="products__top-col-3" >

                        <paginate
                                v-model="selectedPage"
                                :page-count="totalPages"
                                :container-class="pagination"
                                :prev-text="prev"
                                :next-text="next"
                                :click-handler="paginationCallback">
                        </paginate>

                        <div class="products__result">@{{ totalPages }} result</div>


                    </div>
                </div>
                <div class="products__row">

                    @include('category/_common_list/filter')

                    <div class="products__item"  v-for="product in products" >
                        @include('product/item_list_vue')
                    </div>

                </div>
            </div>

        </products-list>
    </main>

@stop


@section('header_styles')
    <link href="{{asset('assets/css/front/ion.rangeSlider.min.css')}}" rel="stylesheet">
@stop

