@extends('layouts/user')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('user/left_menu')

    <div class="main">
        <div class="container">
            <div class="account-wrap">
                <div class="account-title">Profile information</div>
                <form  method="POST" action="{{ route('user.updateProfile') }}" class="registration-form">
                    {{ csrf_field() }}
                    <div class="registration-form__inputs-row">
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Nume" name="first_name" value="{{ $user->first_name }}" required="required">
                            <div class="registration-form__item-placeholder">Nume</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Prenume" name="last_name" value="{{ $user->last_name }}"  required="required" >
                            <div class="registration-form__item-placeholder">Prenume</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Adressa1" name="address1" value="{{ $user->address1 }}">
                            <div class="registration-form__item-placeholder">Adressa1</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Adressa2 (optional)"name="address2" value="{{ $user->address2 }}">
                            <div class="registration-form__item-placeholder">Adressa2 (optional)</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Post code" name="postal_code" value="{{ $user->postal_code }}">
                            <div class="registration-form__item-placeholder">Post code</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Localitatea" name="location" value="{{ $user->location }}">
                            <div class="registration-form__item-placeholder">Localitatea</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="text" placeholder="Judet" name="district" value="{{ $user->district }}">
                            <div class="registration-form__item-placeholder">Judet</div>
                        </div>
                        <div class="registration-form__item-wrap registration-form__item-wrap--country">
                            <input class="registration-form__item" type="text" placeholder="Country" name="country" value="{{ $user->country }}">
                            <div class="registration-form__item-placeholder">Country</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="email" placeholder="E-mail" name="email" value="{{ $user->email }}" required="required">
                            <div class="registration-form__item-placeholder">E-mail</div>
                        </div>
                        <div class="registration-form__item-wrap registration-form__item-wrap--number">
                            <div class="registration-form__item-prefix">+40</div>
                            <input class="registration-form__item" type="text" placeholder="Number" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                    <div class="account-1__submit-wrap">
                        <button type="submit" class="button button--fill-black">Save</button>
                    </div>
                    <div class="col-md-12">
                        <div align="center" class="col-12">
                            @if(session('success'))
                                <div class="text-success bold"  >
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if(session('error'))
                                <div class="text-error bold"  >
                                    {{ session('error') }}
                                </div>
                            @endif
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@stop