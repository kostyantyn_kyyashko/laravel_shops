@extends('layouts/user')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('user/left_menu')

    <div class="main">
        <div class="container">
            <div class="account-wrap">
                <div class="account-title">payment information</div>
                <form method="POST" action="{{ route('user.updateChangePasswod') }}" class="registration-form">
                    {{ csrf_field() }}
                    <div class="registration-form__inputs-row">
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="password"  name="password" required="required" placeholder="New password">
                            <div class="registration-form__item-placeholder">New password</div>
                        </div>
                        <div class="registration-form__item-wrap">
                            <input class="registration-form__item" type="password" name="password_confirmation" required="required" placeholder="Repeat New password">
                            <div class="registration-form__item-placeholder">Repeat New password</div>
                        </div>
                    </div>
                    <div class="account-6__submit-wrap">
                        <button type="submit" class="button button--fill-black">Change</button>
                    </div>
                    <div class="col-12 text-left">
                        @if ($errors->any())
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="red bold">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @if(session('success'))
                            <div class="text-success bold"  >
                                {{ session('success') }}
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop