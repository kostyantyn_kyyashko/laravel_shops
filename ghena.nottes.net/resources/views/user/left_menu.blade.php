<aside class="account-sidebar">
    <button type="button" class="account-sidebar__btn">
        <span></span>
        <span></span>
        <span></span>
    </button>
    <div class="account-sidebar__top">
        <span class="account-sidebar__text">Hello!</span>
        <span class="account-sidebar__name"> {{ $userLogged->first_name }} {{ $userLogged->last_name }}</span>
    </div>
    <ul class="account-sidebar__nav">
        <li ><a href="{{ route('user.profile') }}" >my account</a></li>
        <li><a href="{{ route('user.orders') }}">my orders</a></li>
        <li><a href="{{ route('user.wishlist') }}">saved items</a></li>
        <li><a href="{{ route('user.changePassword') }}">change password</a></li>
    </ul>

    <div class="account-sidebar__links">
        <a href="{{ route('info',['need-help']) }}">need help?</a>
        <a href="{{ route('user.logout') }}">sign out</a>
    </div>

</aside>