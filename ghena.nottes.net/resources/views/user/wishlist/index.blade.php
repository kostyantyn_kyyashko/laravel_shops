@extends('layouts/user')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('user/left_menu')

    <div class="main">
        <div class="container">
            <div class="account-5__wrap">

                <div class="cart-6__product-row">
                    @if(!$wishlists->isEmpty())
                        @foreach($wishlists as $value)
                            <div class="cart-6__product-item cart-card_item_{{ $value->product->id }}">
                                <div class="cart-card">
                                    <div class="cart-card__img">
                                        <a href="{{ $value->product->slug() }}" class="cart-card__slider">
                                            <img src="{{ $value->product->thumb('260x370') }}" alt="{{ $value->product->{'name_'.$lng} }}">
                                        </a>
                                        <a href="javascript:void(0)" data-id="{{ $value->product->id }}" class="account-5__card-like remove_from_wishlist"></a>
                                    </div>
                                    <div class="cart-card__title">
                                        <a href="{{ $value->product->slug() }}">
                                            {{ $value->product->{'name_'.$lng} }}
                                        </a>
                                    </div>
                                    <div class="cart-card__price">{{ $value->product->{'price_'.$lng} }} {{ $valuta }}</div>
                                    <div class="cart-card__color">
                                        <div class="radio-color">
                                            @foreach($value->product->colors as $kcolor => $color)
                                                <div class="radio-color__item radio-color__item__{{ $color->id }} ch_label_{{ $value->id }}" >
                                                    <span></span>
                                                </div>
                                            @endforeach
                                        </div>
                                        @foreach($value->product->colors as $kcolor => $color)
                                            <style>
                                                .radio-color__item__<?=$color->id?> label::before,
                                                .radio-color__item__<?=$color->id?> span::before
                                                {
                                                    background-color: {{ $color->code }} !important;
                                                }
                                            </style>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="account-2__item">
                            No items
                        </div>
                    @endif
                </div>
                <div class="paginations col-md-12">
                    {{ $wishlists->links() }}
                </div>

            </div>
        </div>
    </div>

@stop