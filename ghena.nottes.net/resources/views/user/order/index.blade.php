@extends('layouts/user')

{{-- Page title --}}
@section('title')
    {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    @include('user/left_menu')

    <div class="main">
        <div class="container">
            <div class="account-wrap">
                <div class="account-2__items-wrap">
                    @if(!$orders->isEmpty())
                        @foreach($orders as $order)

                            <div class="account-2__item">
                                <div class="account-2__item-col">
                                    @if($order->products)
                                    <div class="account-2__item-img">
                                        <a href="{{ route('orderDetail',$order->invoiceID) }}" class="cart-card__slider">
                                            @foreach($order->products as $vprod)
                                                <img src="{{ $vprod->path.'_'.'260x370'.'.'.$vprod->ext }}" alt="{{ $vprod->{'name_'.$lng} }}">
                                            @endforeach
                                        </a>
                                    </div>
                                    @endif
                                </div>
                                <div class="account-2__item-col">
                                    <div class="account-2__item-text">
                                        <span><span class="mark">order Nr</span> : {{ $order->id }}</span>
                                        <span><span class="mark">shipped date:</span> {{ \Carbon\Carbon::parse($order->shipping_date)->format('d M,Y') }}</span>
                                        <span><span class="mark">date delivery:</span> {{ \Carbon\Carbon::parse($order->delivery_date)->format('d M,Y') }}</span>
                                        <span><span class="mark">{{ $order->qt }} items</span></span>
                                    </div>
                                    <div class="account-2__item-order">Your order has been sent. We hope you love it!</div>
                                    <div class="account-2__item-btn">
                                        <a href="{{ route('orderDetail',$order->invoiceID) }}" class="button button--fill-black">view more</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="paginations">
                            {{ $orders->links() }}
                        </div>

                    @else
                        <div class="account-2__item">
                            No orders
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop