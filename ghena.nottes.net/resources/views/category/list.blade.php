@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="products" id="app" >
        <input type="hidden" class="products_url" value="{{ url()->current() }}">

            <div class="container">
                <h2 class="products__title">
                    {{ $vars['produse'] }} {{ $categoryTitle }}
                </h2>
                <div class="products__top-row">

                    {{--FILTER TITLE--}}
                    <div class="products__top-col-1">
                        <div class="products__filter">
                            <div class="filter-btn">
                                <span class="filter-btn__close"></span>
                                <span class="filter-btn__text">{{ $vars['filter'] }}</span>
                            </div>
                        </div>
                    </div>
                    {{--SUB CATEGORIES--}}
                    @include('category/_common_list/sub_category')

                    {{--PAGINATION--}}
                    <div class="products__top-col-3 product_pagination_area">
                        @include('category/_common_list/product_pagination')
                    </div>

                </div>

                <div class="products__row">

                    @include('category/_common_list/filter')

                </div>

                <div class="product_list_area products__row">
                    @include('category/_common_list/product_list')
                </div>


            </div>

    </main>

@stop

@section('footer_scripts')
    <script src="{{ asset('assets/js/manifest.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
@stop

@section('header_styles')
    <link href="{{asset('assets/css/front/ion.rangeSlider.min.css')}}" rel="stylesheet">
@stop

