<div class="filter">
    <?php
    // search gets
    $filters = [
        'sizes' => isset($_GET['sizes']) ? $_GET['sizes'] : [],// Marimi
    ];
    ?>
    <div class="filter__size filter__item">
        <div class="filter__title">Size</div>
        <ul class="filter__size-list filter_size_list">
            @foreach($sizeList as $key => $value)
                @if($key+1 <= 4)
                    <li>
                        <a href="javascript:void(0)" class="li_size" data-id = "{{ $value->id }}"  >
                            {{ $value->{'name_'.$lng} }}
                        </a>
                    </li>
                @else
                    <li class="filter__more-items" >
                        <a href="javascript:void(0)" class="li_size" data-id = "{{ $value->id }}"  >
                            {{ $value->{'name_'.$lng} }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        @if(count($sizeList) > 4)
            <a href="#" class="filter__more">view more</a>
        @endif
    </div>

    <div class="filter__color filter__item">
        <div class="filter__title">Color</div>
        <ul class="filter__color-list filter_color_list">
            @foreach($colorList as $key => $value)
                <li>
                    <a href="javascript:void(0)" class="li_color" data-id = "{{ $value->id }}" >
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="filter__model filter__item">
        <div class="filter__title">Model</div>
        <ul class="filter__size-list filter_model_list">
            @foreach($subCategories as $key => $value)
                @if($key+1 <= 4)
                    <li>
                        <a href="javascript:void(0)" class="li_model" data-id = "{{ $value->id }}"  >
                            {{ $value->{'name_'.$lng} }}
                        </a>
                    </li>
                @else
                    <li class="filter__more-items" >
                        <a href="javascript:void(0)" class="li_model" data-id = "{{ $value->id }}"  >
                            {{ $value->{'name_'.$lng} }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        @if(count($subCategories) > 4)
            <a href="#" class="filter__more">view more</a>
        @endif
    </div>

    <div class="filter__collections filter__item">
        <div class="filter__title">Colections</div>
        <ul class="filter__collections-list filter_collection_list">
            @foreach($collectionList as $key => $value)
                @if($key+1 <= 4)
                    <li>
                        <a href="javascript:void(0)" class="li_collection" data-id = "{{ $value->id }}"  >
                            {{ $value->{'name_'.$lng} }}
                        </a>
                    </li>
                @else
                    <li class="filter__more-items" >
                        <a href="javascript:void(0)" class="li_collection" data-id = "{{ $value->id }}"  >
                            {{ $value->{'name_'.$lng} }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        @if(count($collectionList) > 4)
            <a href="#" class="filter__more">view more</a>
        @endif
    </div>

    <div class="filter__price filter__item">
        <div class="filter__title">Price</div>
        <div class="filter__range-wrap"><input type="text" class="filter__range"></div>
    </div>

    <input type="hidden" value="{{ $category->id }}" class="category_id" >
    <input type="hidden" value="{{ !empty($subCategory) ? $subCategory->id : '' }}" class="sub_category_id" >

</div>