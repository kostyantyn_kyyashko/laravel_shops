@foreach($productList as $key => $value)
    @include('product/item-list')
@endforeach
@if(isset($isAjax))
    <script type="text/javascript" src="{{asset('assets/js/front/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/front/slick.min.js')}}"></script>
    <script>
        // -------------------- initialize products-card__sliders/cart-card__slider ---------------------

        $('.products-card__slider, .cart-card__slider').each(function () {
            $(this).slick({
                dots: false,
                arrows: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 700,

                speed: 300,
                fade: true,
                cssEase: 'linear'

            });

            let slickPause = () => {
                $(this).slick('slickPause');
            }

            slickPause();

            $(this).mouseover(function () {
                $(this).slick('slickPlay')
            });
            $(this).mouseout(function () {
                slickPause();
            });
        });
    </script>
@endif