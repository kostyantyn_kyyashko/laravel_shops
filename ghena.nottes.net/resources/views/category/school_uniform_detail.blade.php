@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="unifromi">
        <div class="container">
            <div class="unifromi__wrap unifromi--3__wrap">
                <div class="unifromi-3__top-row">
                    <div class="unifromi-3__top-col-1">
                        <h2 class="unifromi-3__title">
                            {{ $category->{'name_'.$lng} }}
                        </h2>
                        <div class="unifromi-3__text">
                            {!! $category->{'text_'.$lng} !!}
                        </div>
                    </div>
                    <div class="unifromi-3__top-col-2">
                        <div class="unifromi-3__img">
                            <img src="{{ $category->image }}" alt=" {{ $category->{'name_'.$lng} }}">
                        </div>
                    </div>
                </div>

                @foreach($categoryList as $key => $value)

                    <div class="unifromi-3__row">
                        @if($key == 0)
                            <div class="unifromi-3__col unifromi-3__col--card">
                                <div class="unifromi-3__card">
                                    <a href="{{ route('productsByTheLook',$value->slug) }}"><img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}"></a>
                                    <a href="{{ route('productsByTheLook',$value->slug) }}">{!! $value->{'text2_'.$lng} !!}</a>
                                    <span>{{ $value->{'price_'.$lng} }} {{ $valuta }}</span>
                                </div>
                            </div>
                        @endif

                        <div class="unifromi-3__col">
                            <h2 class="unifromi-3__item-title"> {{ $value->{'name_'.$lng} }}</h2>
                            <div class="unifromi-3__item-subtitle"> {{ $value->{'name2_'.$lng} }}</div>
                            <div class="unifromi-3__item-text">
                                {!! $value->{'text_'.$lng} !!}
                            </div>
                            <div class="unifromi-3__item-row">
                                <div class="unifromi-3__item-col">
                                    <a href="{{ route('productsByTheLook',$value->slug) }}"><img src="{{ $value->image1 }}" alt="{{ $value->{'name_'.$lng} }}"></a>
                                </div>

                                <div class="unifromi-3__item-col">
                                    <a href="{{ route('productsByTheLook',$value->slug) }}">
                                    <img src="{{ $value->image2 }}" alt="{{ $value->{'name_'.$lng} }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                            @if($key > 0)
                                <div class="unifromi-3__col">
                                    <div class="unifromi-3__card">
                                        <a href="{{ route('productsByTheLook',$value->slug) }}"><img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}"></a>
                                        <a href="{{ route('productsByTheLook',$value->slug) }}">{!! $value->{'text2_'.$lng} !!}</a>
                                        <span>{{ $value->{'price_'.$lng} }} {{ $valuta }}</span>
                                    </div>
                                </div>
                            @endif

                    </div>

                @endforeach

            </div>

            @include('category/uniform_feedback')

            @include('category/uniform_partners')
        </div>
    </main>



@stop


