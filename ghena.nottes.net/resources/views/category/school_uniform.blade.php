@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="unifromi">
        <div class="container">
            <div class="unifromi__wrap">
                <div class="unifromi__top-row">
                    <div class="unifromi__top-col-1">
                        <h2 class="unifromi__title">
                            {{ $category->{'name2_'.$lng} }}
                        </h2>
                    </div>
                    <div class="unifromi__top-col-2">
                        <div class="unifromi__text">
                            {!! $category->{'text_'.$lng} !!}
                        </div>
                    </div>
                    <div class="unifromi__top-col-3">
                        <div class="unifromi__text">
                            {!! $category->{'text2_'.$lng} !!}
                        </div>
                    </div>
                </div>

                <div class="unifromi__main-row">

                    @foreach($categoryList as $value)

                        <div class="unifromi__item">
                            <div class="news-unifromi">
                                <div class="news-unifromi__img">
                                    <img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                                    <img src="{{ $value->image1 }}" alt="{{ $value->{'name_'.$lng} }}" class="news-unifromi__bage">
                                </div>
                                <h3 class="news-unifromi__title"> {{ $value->{'name_'.$lng} }}</h3>
                                <div class="news-unifromi__text">
                                    {!! $value->{'text_'.$lng} !!}
                                </div>
                                <div class="news-unifromi__bottom">
                                    <a href="{{ route('schoolUniformDetail',$value->slug) }}" class="news-unifromi__btn">
                                        {{ $vars['view_all'] }}
                                    </a>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>

            @include('category/uniform_feedback')

            @include('category/uniform_partners')

        </div>
    </main>

@stop


