<div class="unifromi__partners">
    <h2 class="unifromi__partners-title">our parners</h2>
    <div class="unifromi__partners-slider">
        @foreach($partners as $value)
            <div class="unifromi__partners-item">
                <img src="{{ $value->image }}" alt="Vivi">
            </div>
        @endforeach
    </div>

</div>