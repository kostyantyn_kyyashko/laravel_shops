<div class="unifromi__form-wrap" id="sendWorkWithUs">
    <h2 class="unifromi__form-tile">wanna work with us?</h2>
    <div class="unifromi__form-text">Our clients a synonym for high quality and creativity. The vest has a classic style but also unique accessory.</div>
    <form action="{{ route('sendWorkWithUs') }}#sendWorkWithUs" method="post" class="unifromi__form-date">
        {{ csrf_field() }}
        <div class="unifromi__form-row">
            <input class="unifromi__form-item" type="text" name="name" value="{{ old('company') }}" required placeholder="Name, Prenume">
            <input class="unifromi__form-item" type="text" name="company" value="{{ old('company') }}" required placeholder="Company">
        </div>
        <div class="unifromi__form-row">
            <input class="unifromi__form-item" type="email" name="email"  value="{{ old('email') }}" required placeholder="E-mail">
            <input class="unifromi__form-item" type="text" name="subject"  value="{{ old('subject') }}" required placeholder="Subject">
        </div>
        <div class="unifromi__message-wrap">
            <span class="unifromi__message-row-1"></span>
            <span class="unifromi__message-row-2"></span>
            <span class="unifromi__message-row-3"></span>
            <textarea class="unifromi__message" name="message"  required placeholder="Message">{{ old('subject') }}</textarea>
        </div>

        @if ($errors->any())
            <div class="col-sm-12 col-md-12 clearfix row help-block">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('success'))
            <div class="col-sm-12 col-md-12 clearfix row alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="unifromi__form-btn">
            <button type="submit" class="button button--fill-black">Send</button>
        </div>

    </form>
    <div class="unifromi__form-contacts-row">
        <div class="unifromi__form-contacts-col">
            <div class="unifromi__form-contacts-tite">phone</div>
            <div class="unifromi__form-contacts-text">+373 60 78 45 68</div>
        </div>

        <div class="unifromi__form-contacts-col">
            <div class="unifromi__form-contacts-tite">e-mail</div>
            <div class="unifromi__form-contacts-text">+373 60 78 45 68</div>
        </div>

        <div class="unifromi__form-contacts-col">
            <div class="unifromi__form-contacts-tite">adress</div>
            <div class="unifromi__form-contacts-text">București, România
                Str. Arh. Ionescu Grigore nr. 63
                bl.T73, sc.B, et.4, ap.42, sector2</div>
        </div>
    </div>

</div>