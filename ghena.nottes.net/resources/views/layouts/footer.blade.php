<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="footer__row">
                <div class="footer__col-1">
                    <div class="footer__title">{{ $vars['become_a_wholesale_customer'] }}</div>
                    <form action="javascript:void(0)" class="footer__form subscribe_form">
                        <input type="text" name="email" placeholder="Enter e-mail">
                        <button type="submit" class="subscribe">{{ $vars['send_request'] }}</button>
                    </form>
                    <input type="hidden" class="emailul_trebuie_sa_fie_valid" value="{{ $vars['emailul_trebuie_sa_fie_valid'] }}" >
                    <p class="footer__text">
                        {{ $vars['geci_camasi_bluze_subscribe_footer'] }}
                    </p>
                </div>
                <div class="footer__col-2">
                    <div class="footer__title">{{ $vars['shop'] }}</div>
                    <ul class="footer__list">
                        @foreach($navFooterShop as $value)
                            <li>
                                <a href="{{ $value->formattedSlug() }}" class="{{ (isset($activeMenu) && $activeMenu == $value->id ? 'active' : '') }}">
                                    {{ $value->{'name_'.$lng} }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="footer__col-3">
                    <div class="footer__title">{{ $vars['helpinformation'] }}</div>
                    <ul class="footer__list">
                        @foreach($navFooterHelpInformation as $value)
                            <li>
                                <a href="{{ $value->formattedSlug() }}" class="{{ (isset($activeMenu) && $activeMenu == $value->id ? 'active' : '') }}">
                                    {{ $value->{'name_'.$lng} }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="footer__col-4">
                    <div class="footer__title">{{ $vars['follow_us'] }}</div>
                    <ul class="footer__list">
                        <li><a href="{{ $vars['instagram_url'] }}">{{ $vars['instagram'] }}</a></li>
                        <li><a href="{{ $vars['facebook_url'] }}">{{ $vars['facebook'] }}</a></li>
                        <li><a href="{{ $vars['youtube_url'] }}">{{ $vars['youtube'] }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container">
            <div class="footer__bottom-row">
                <div class="footer__bottom-col-1">{{ $vars['copyright_vivi_art_botique_2020'] }}</div>
                <div class="footer__bottom-col-2">
                    <div class="footer__social">
                        <a href="{{ $vars['facebook_url'] }}" class="footer__social-fb"></a>
                        <a href="{{ $vars['instagram_url'] }}" class="footer__social-ins"></a>
                    </div>
                </div>
                <div class="footer__bottom-col-3">
                    <div class="footer__policy">
                        @foreach($privacyMenu as $value)
                            <a href="{{ $value->formattedSlug() }}" class="{{ (isset($activeMenu) && $activeMenu == $value->id ? 'active' : '') }}">
                                {{ $value->{'name_'.$lng} }}
                            </a>
                        @endforeach
                    </div>
                    <div class="footer__language">
                        <div class="footer__language-current">Romania</div>
                        <ul class="footer__language-list">
                            @foreach($languages as $value)
                                <li>
                                    <a href="{{ langSwitch($value->code) }}" >
                                        {{ $value->{'name_ro'} }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>