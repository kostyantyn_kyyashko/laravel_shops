<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <title>
        {{ isset($page->{'seo_title_'.$lng}) && $page->{'seo_title_'.$lng} ? $page->{'seo_title_'.$lng} : env('APP_NAME') }}
    </title>

    <!-- page keywords and description -->
    <meta name="keywords" content="{{ isset($page->{'seo_keywords_'.$lng}) ? $page->{'seo_keywords_'.$lng} :'' }}" />
    <meta name="description" content="{{ isset($page->{'seo_description_'.$lng}) ? $page->{'seo_description_'.$lng} :'' }}" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

@yield('meta') @show

<!-- Template Basic Images Start -->
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="icon" href="img/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->

    <link href="{{asset('assets/css/front/normalize.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/slick.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/main.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/custom.css')}}" rel="stylesheet">

    <!--begin css -->
@yield('header_styles')
<!--end css-->

</head>
<body>
<div>