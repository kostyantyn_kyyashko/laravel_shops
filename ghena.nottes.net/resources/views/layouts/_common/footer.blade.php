
<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
<!-- begin js -->
<script type="text/javascript" src="{{asset('assets/js/front/jquery-3.4.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/front/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/front/jquery.fancybox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/front/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/front/main.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/front/custom.js')}}"></script>


@yield('footer_scripts')



</div>

</body>

</html>
