<!-- Header HTML -->
@include('layouts/_common/header')

        <!-- Header Start -->
        @if($showHeader)
            @include('layouts/header')
        @endif
        <!-- //Header End -->

        <!-- the content -->
        <main>
            @yield('content')
        </main> <!-- end main -->

        <!-- Footer Section Start -->
        @if($showFooter)
            @include('layouts/footer')
        @endif
        <!-- //Footer Section End -->

<!-- FOOTER HTML -->
@include('layouts/_common/footer')
<style>
        .view::after {
            content: '<?=$vars['back_to_black']?>';
        }
</style>