<header class="header">
    <div class="container header__row-wrap">
        <div class="header__row">
            <a href="/{{ $lng }}" class="header__logo">
                <img src="/assets/img/logo.png" alt="Vivi">
            </a>
            <nav class="header__nav">
                <ul class="header__nav-list">
                    @foreach($navMenu as $value)
                        <li>
                            @if($value->haveSubmenu == 1)
                                <a href="javascript:void(0)" class="{{ (isset($activeMenu) && $activeMenu == $value->id ? 'active' : '') }}">
                                    {{ $value->{'name_'.$lng} }}
                                </a>
                                <div class="header-submenu">
                                    <div class="header-submenu__left">
                                        @php
                                            $subMenuList = $value->getSubMenu();
                                            $takeCount = round(count($subMenuList)/2);
                                        @endphp
                                        @if($subMenuList)
                                            @foreach($subMenuList->chunk($takeCount) as $chunk)
                                                <div class="header-submenu__list-wrap">
                                                    <div class="header-submenu__list-litle">Category</div>
                                                    <ul class="header-submenu__list">
                                                        @foreach ($chunk as $v)
                                                            <li>
                                                                <a href="{{ $v->slug() }}">
                                                                   {{ $v->{'name_'.$lng} }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="header-submenu__right">

                                        <div class="header-submenu__img">
                                            <img src="{{ $value->image }}" alt="Vivi">
                                            <div class="header-submenu__img-text">
                                                <span class="header-submenu__img-title">
                                                    {!! $value->{'text_'.$lng} !!}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="header-submenu__img">
                                            <img src="{{ $value->image1 }}" alt="Vivi">
                                            <div class="header-submenu__img-text">
                                                <span class="header-submenu__img-title">
                                                    {!! $value->{'text2_'.$lng} !!}
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @else
                                <a href="{{ $value->formattedSlug() }}" class="{{ (isset($activeMenu) && $activeMenu == $value->id ? 'active' : '') }}">
                                    {{ $value->{'name_'.$lng} }}
                                </a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </nav>

            <div class="header__user-block">
                <a href="#" class="header__search active">
                    <img src="/assets/img/icon-search.svg" alt="search">
                </a>
                <a href="@guest {{ route('login') }} @else {{ route('user.profile') }}  @endif" class="header__user">
                    <img src="/assets/img/icon-user.svg" alt="search">
                </a>
                <a href="{{ route('cart') }}" class="header__bag">
                    <img src="/assets/img/icon-bag.svg" alt="Vivi">
                    <span class="header__bag-quantity">{{ Cart::count() }}</span>
                </a>
            </div>
            <div class="header__nav-btn">
                <div class="nav-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="header__search-block">
        <div class="container">
            <form method="get" action="{{ route('search-fullsite') }}" class="header__search-form">
                <input class="header__search-text" type="text" name="s" placeholder="{{ $vars['cautare'] }}">
                <button class="header__search-submit" type="submit"></button>
                <button class="header__search-close" type="button"></button>
            </form>
        </div>
    </div>
</header>