<!-- Header HTML -->
@include('layouts/_common/header')

        <!-- Header Start -->
        @include('layouts/header')
        <!-- //Header End -->

        <!-- the content -->
        <main>
            @yield('content')
        </main> <!-- end main -->


<!-- FOOTER HTML -->
@include('layouts/_common/footer')