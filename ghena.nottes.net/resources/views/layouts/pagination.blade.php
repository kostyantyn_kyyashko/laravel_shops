@if ($paginator->hasPages())

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="javascript:void(0)" class="active">
                            {{ $page }}
                        </a>
                    @else

                            <a href="{{ $url }}">{{ $page }}</a>

                    @endif
                @endforeach
            @endif
        @endforeach

@endif
