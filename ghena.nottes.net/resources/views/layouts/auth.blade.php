<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <title>
        {{ isset($page->{'seo_title_'.$lng}) && $page->{'seo_title_'.$lng} ? $page->{'seo_title_'.$lng} : env('APP_NAME') }}
    </title>

    <!-- page keywords and description -->
    <meta name="keywords" content="{{ isset($page->{'seo_keywords_'.$lng}) ? $page->{'seo_keywords_'.$lng} :'' }}" />
    <meta name="description" content="{{ isset($page->{'seo_description_'.$lng}) ? $page->{'seo_description_'.$lng} :'' }}" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    @yield('meta') @show

    <!-- Template Basic Images Start -->
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="icon" href="img/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/favicon/apple-touch-icon-180x180.png">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->

    <link href="{{asset('assets/css/front/normalize.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/slick.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/main.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/front/custom.css')}}" rel="stylesheet">

    <!--begin css -->
    @yield('header_styles')
    <!--end css-->

</head>
<body>
<div>

        <!-- the content -->
        <main>
            @yield('content')
        </main> <!-- end main -->

        <input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
        <!-- begin js -->
        <script type="text/javascript" src="{{asset('assets/js/front/jquery-3.4.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/front/slick.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/front/ion.rangeSlider.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/front/main.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/front/custom.js')}}"></script>

        @yield('footer_scripts')

</div>

</body>

</html>
