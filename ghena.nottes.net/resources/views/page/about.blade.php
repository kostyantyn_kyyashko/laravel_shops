@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="about">
        <div class="container">
            <div class="about__wrap">
                <h2 class="about__title"> {{ $page->{'name_'.$lng} }}</h2>

                <div class="about__video">
                    <img src="{{ $page->image }}" alt="{{ $page->{'name_'.$lng} }}">
                    <a href="{{ $page->{'video_'.$lng} }}" class="home__video-play hoja">
                        <span>{{ $vars['play_video'] }}</span>
                    </a>
                </div>

                <div class="about__history">
                    <div class="about__history-title">{{ $page->{'name2_'.$lng} }}</div>
                    <div class="about__history-row">
                        <div class="about__history-col">
                            {!! $page->{'text_'.$lng} !!}
                        </div>
                        <div class="about__history-col">
                            {!! $page->{'text2_'.$lng} !!}
                        </div>
                        <div class="about__history-col">
                            {!! $page->{'text3_'.$lng} !!}
                        </div>
                        <div class="about__history-col">
                            <div class="about__history-director">
                                <img src="{{ $director->image }}" alt="{{ $page->{'name_'.$lng} }}">
                                <div class="about__history-director-name">{{ $director->{'name_'.$lng} }}</div>
                                <div class="about__history-director-pos">{{ $director->{'name2_'.$lng} }}</div>
                                <div class="about__history-director-text">
                                    {!! $director->{'text_'.$lng} !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="about__shop-row">
                    <div class="about__shop-left">
                        <div class="about__shop-left-row">
                            <div class="about__shop-img">
                                <img src="{{ $page->image1 }}" alt="Vivi">
                            </div>
                            <div class="about__shop-img">
                                <img src="{{ $page->image2 }}" alt="Vivi">
                            </div>
                        </div>
                    </div>
                    <div class="about__shop-right">
                        <div class="about__shop-btn">
                            <div class="about__shop-btn-title">{{ $vars['our_showeoom'] }}</div>
                            <a href="{{ $vars['go_to_shop_url'] }}" class="button button--fill-black">
                                {{ $vars['go_to_shop'] }}
                            </a>
                        </div>
                    </div>
                </div>

                <div class="about__view">
                    <div class="about__view-img">
                        <img src="{{ $collection->image }}" alt="{{ $collection->{'name_'.$lng} }}">
                        <div class="about__view-content">
                            <h2 class="about__view-title">{{ $collection->{'name_'.$lng} }}</h2>
                            <div class="about__view-btn">
                                <a href="{{ $collection->{'link_'.$lng} }}" class="button button--fill-black">{{ $vars['view_more'] }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@stop

