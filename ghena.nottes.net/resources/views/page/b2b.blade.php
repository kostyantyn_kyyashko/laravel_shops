@extends('layouts/default')

{{-- Page title --}}
@section('title')
    {{ $page->{'name_'.$lng} }} | {{ ENV('APP_NAME') }}
@stop

{{-- content --}}
@section('content')

    <main class="unifromi">
        <div class="container">
            <div class="unifromi__wrap">
                <div class="unifromi__top-row">
                    <div class="unifromi__top-col-1">
                        <h2 class="unifromi__title">{{ $category->{'name2_'.$lng} }}</h2>
                    </div>
                    <div class="unifromi__top-col-2">
                        <div class="unifromi__text">
                            {!! $category->{'text_'.$lng} !!}
                        </div>
                    </div>
                    <div class="unifromi__top-col-3">
                        <div class="unifromi__text">
                            {!! $category->{'text2_'.$lng} !!}
                        </div>
                    </div>
                </div>

                <div class="unifromi-2__bages">
                    <div class="unifromi-2__bages-row">

                        @foreach($b2bList as $value)

                            <div class="unifromi-2__bages-item">
                                <img src="{{ $value->image }}" alt="{{ $value->{'name_'.$lng} }}">
                                <span>{{ $value->{'name_'.$lng} }}</span>
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>

            @include('category/uniform_feedback')

            @include('category/uniform_partners')

        </div>
    </main>

@stop

