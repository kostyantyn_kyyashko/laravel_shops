<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" />
<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script>
    $(function() {
        // sub cats
        $(".multiselect_subcats").select2({
            theme: "bootstrap",
            placeholder: "select a value"
        }).on('change', function (e) {
            var selectedIDs = [];
            $(".multiselect_subcats option:selected").each(function(){
                selectedIDs.push($(this).val());
            });
            $.ajax({
                type: "POST",
                url: "/admin/product/getSubSubCats",
                data: {catIDs : selectedIDs ,'_token': $('.csrf_token').val()},
                dataType : "json",
                success: function(data) {
                    $('.subsubcats_area').html(data.html);
                }
            });
            console.log("onchange");

        });

    });
</script>
<div class="form-group {{ $errors->first('subcategories', 'has-error') }}">
    <label for="type" class="">{{ trans('product/form.subcats') }}</label>
    {!!Form::select('subcategories[]', $subcategories, null, ['multiple' => 'multiple','style' =>'width:70%','class' => 'form-control select2 multiselect_subcats'])!!}
    {!! $errors->first('subcategories', '<span class="help-block">:message</span>') !!}
</div>