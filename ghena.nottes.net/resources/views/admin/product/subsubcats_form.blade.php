<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" />
<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script>
    $(function() {
        $(".multiselect2").select2({
            theme: "bootstrap",
            placeholder: "select a value"
        });
    });
</script>
<div class="form-group {{ $errors->first('subsubcategories', 'has-error') }}">
    <label for="type" class="">{{ trans('product/form.subcats') }}</label>
    {!!Form::select('subsubcategories[]', $subsubcategories, null, ['multiple' => 'multiple','style' =>'width:70%','class' => 'form-control select2 multiselect2'])!!}
    {!! $errors->first('subsubcategories', '<span class="help-block">:message</span>') !!}
</div>