{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/materialdate/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/summernote/summernote.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}" ></script>

    <script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/pages/editorPage.js') }}"  type="text/javascript"></script>

    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
    <script>
        $(function() {
            $( ".image-select" ).click(function() {
                $('.fileupload-preview').remove();
            });
        });
        // $('.sizle_list').on("select2:select", function (e) {
        //     var data = e.params.data.text;
        //     if(data=='All'){
        //         $(".sizle_list > option").prop("selected","selected");
        //         $(".sizle_list").trigger("change");
        //     }
        // });
        $(".multiselect").select2({
            theme: "bootstrap",
            placeholder: "select a value"
        });

        $(".multiselect_cats").select2({
            theme: "bootstrap",
            placeholder: "select a value"
        }).on('change', function (e) {
            var selectedIDs = [];
            $(".multiselect_cats option:selected").each(function(){
                selectedIDs.push($(this).val());
            });
            $.ajax({
                type: "POST",
                url: "/admin/product/getSubCats",
                data: {catIDs : selectedIDs ,'_token': $('.csrf_token').val()},
                dataType : "json",
                success: function(data) {
                    $('.subcats_area').html(data.html);
                }
            });
        });

        // sub cats
        $(".multiselect_subcats").select2({
            theme: "bootstrap",
            placeholder: "select a value"
        }).on('change', function (e) {
            var selectedIDs = [];
            $(".multiselect_subcats option:selected").each(function(){
                selectedIDs.push($(this).val());
            });
            $.ajax({
                type: "POST",
                url: "/admin/product/getSubSubCats",
                data: {catIDs : selectedIDs ,'_token': $('.csrf_token').val()},
                dataType : "json",
                success: function(data) {
                    $('.subsubcats_area').html(data.html);
                }
            });
            console.log("onchange");

        });
        // update product propriets
        $(document).ready(function () {

            $(document).on("click", ".delete-product-prop", function(e) {
                var id    = $(this).attr('data-id');
                var table = $(this).attr('data-table');
                $.ajax({
                    url: '/admin/product/deleteProprietes',
                    type: 'post',
                    data: {'id': id,'table' : table,"_token" : "{{ csrf_token() }}" },
                    success: function (resp) {
                        $('.'+table+'_item_'+id).remove();
                    }
                });
                e.preventDefault();
            });

            $('.add-product-prop').click(function (e) {

                var product_id    = $(this).attr('data-product_id');
                var table = $(this).attr('data-table');
                var price = $('.'+table+'_price').val();
                if(price == '') {
                    alert('Cantitatea este obligatorie !!!');
                    e.preventDefault();
                    return false;
                }

                var selected_value = $('.'+table+'_selected_value').val();

                $.ajax({
                    url: '/admin/product/addProprietes',
                    type: 'post',
                    data: {price:price,'selected_value': selected_value,product_id: product_id,'table' : table,"_token" : "{{ csrf_token() }}" },
                    success: function (resp) {
                        $('.'+table+'_list').html(resp.html);
                        $('.'+table+'_price').val('');
                    }
                });
                e.preventDefault();
            });

        });

    </script>

@stop