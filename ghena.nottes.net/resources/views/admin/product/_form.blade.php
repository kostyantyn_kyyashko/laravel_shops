<input type="hidden" class="csrf_token" name="_token" value="{{ csrf_token() }}" />
@if ($errors->any())
    <div class="alert alert-danger alert-feedback margin-top-10">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">
            <ul class="nav nav-tabs">
                @foreach(langs() as $key => $lang)
                    <li class="{{ $key == 0 ? 'active' : '' }}">
                        <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                            {{ $lang['title'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content tab-content-area" >
                @foreach(langs() as $key => $lang)
                    <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                        <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('product/form.title') }}</label>
                            {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('product/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="title-textarea">{{ trans('product/form.text') }}</label>
                            {!! Form::textarea('text_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('product/form.text'))) !!}
                            {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->first('price_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('product/form.price') }}</label>
                            {!! Form::text('price_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('product/form.price'))) !!}
                            <span class="help-block">{{ $errors->first('price_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('name2_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('product/form.title') }} (100% coton)</label>
                            {!! Form::text('name2_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('product/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name2_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('name3_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('product/form.title') }} (Dry cleaning)</label>
                            {!! Form::text('name3_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('product/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name3_'.$lang['code'], ':message') }}</span>
                        </div>


                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <style>
                .img-area {
                    height: 80px;
                    margin-left: -15px;
                }
                .fileupload {
                    margin-bottom: -10px;
                }
                .check-for-delete-file {
                    height: 18px;
                    overflow: hidden;
                    margin-top: -5px;
                }
                .mleft-20 {
                    margin-left: 0px !important;
                }
                .fileupload-preview {
                    margin-top: -10px;
                }
            </style>
            <div class="form-group img-area col-md-6 row">
                <div class="" >
                    <label>@lang('product/form.image')</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">@lang('product/form.select-file')</span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
                    </div>
                </div>
                @if(isset($product->image) && !empty($product->image))
                    <div class="pull-left mleft-20">
                    <span class="fileupload-preview">
                        <a href="{{URL::to($product->image)}}" target="_blank" >
                            Download
                            {{--<img src="{{URL::to($product->image)}}" style="max-height: 55px" class="img-responsive" alt="Image">--}}
                        </a>
                    </span>
                        <label class="check-for-delete-file">
                            <input type="checkbox" name="delete_image"  >
                            @lang('general.check_for_delete')
                        </label>
                    </div>
                @endif
            </div>
            {{--IMAGE 2--}}
            <div class="form-group img-area col-md-6 row" >
                <label>@lang('page/form.image')  2</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('product/form.select-file')</span>
                            {!! Form::file('image1', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($product->image1) && !empty($product->image1))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($product->image1)}}" target="_blank" >
                                Download
                                {{--<img src="{{URL::to($product->image1)}}" style="max-height: 30px" class="img-responsive" alt="Image">--}}
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image1"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            {{--IMAGE 3--}}
            <div class="form-group img-area col-md-6" >
                <label>@lang('page/form.image') 3</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('product/form.select-file')</span>
                            {!! Form::file('image2', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($product->image2) && !empty($product->image2))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($product->image2)}}" target="_blank" >
                                Download
                                {{--<img src="{{URL::to($product->image2)}}" style="max-height: 30px" class="img-responsive" alt="Image">--}}
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image2"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            {{--IMAGE 4--}}
            <div class="form-group img-area col-md-6 row" >
                <label>@lang('page/form.image')  4</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('product/form.select-file')</span>
                            {!! Form::file('image3', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($product->image3) && !empty($product->image3))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($product->image3)}}" target="_blank" >
                                Download
                                {{--<img src="{{URL::to($product->image3)}}" style="max-height: 30px" class="img-responsive" alt="Image">--}}
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image3"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            {{--IMAGE 5--}}
            <div class="form-group img-area col-md-6" >
                <label>@lang('page/form.image')  5</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('product/form.select-file')</span>
                            {!! Form::file('image4', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($product->image4) && !empty($product->image4))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($product->image4)}}" target="_blank" >
                                Download
                                {{--<img src="{{URL::to($product->image4)}}" style="max-height: 30px" class="img-responsive" alt="Image">--}}
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image4"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            {{--IMAGE 5--}}
            <div class="form-group img-area col-md-6" >
                <label>Video (Doar MP4)</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('product/form.select-file')</span>
                            {!! Form::file('video', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($product->video) && !empty($product->video))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($product->video)}}" target="_blank" >
                               Download
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_video"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            <div class="clear"></div>
            <div class="form-group {{ $errors->first('categories', 'has-error') }}">
                <label for="type" class="">{{ trans('product/form.category') }}</label>
                {!!Form::select('categories[]', $categories, null, ['multiple' => 'multiple','style' =>'width:70%','class' => 'form-control multiselect_cats select2 multiselect'])!!}
                {!! $errors->first('categories', '<span class="help-block">:message</span>') !!}
            </div>
            <div class="subcats_area">
                @if(isset($product) && !$subcategories->isEmpty())
                    <div class="form-group {{ $errors->first('subcategories', 'has-error') }}">
                        <label for="type" class="">{{ trans('product/form.subcats') }}</label>
                        {!!Form::select('subcategories[]', $subcategories, null, ['multiple' => 'multiple','style' =>'width:100%','class' => 'form-control select2 multiselect_subcats'])!!}
                        {!! $errors->first('subcategories', '<span class="help-block">:message</span>') !!}
                    </div>
                @endif
            </div>
            <div class="subsubcats_area">
                @if(isset($product) && !$subsubcategories->isEmpty())
                    <div class="form-group {{ $errors->first('subsubcategories', 'has-error') }}">
                        <label for="type" class="">{{ trans('product/form.subsubcats') }}</label>
                        {!!Form::select('subsubcategories[]', $subsubcategories, null, ['multiple' => 'multiple','style' =>'width:100%','class' => 'form-control select2 multiselect'])!!}
                        {!! $errors->first('subsubcategories', '<span class="help-block">:message</span>') !!}
                    </div>
                @endif
            </div>
            {{--SIZES--}}

            <div class="form-group sizes-area product-prop-area">
                <label for="type" class="">{{ trans('product/form.size') }}</label>
                @include('admin.product.product_sizes_list')
                <div class="item-content-v1">
                    <div class="form-group col-md-6" style="margin-left: -15px">
                        <label>{{ trans('product/form.select') }}</label>
                        {!! Form::select('sizes[]', $sizes, null, ['style' =>'width:100%','class' => 'form-control select2 product_sizes_selected_value ']) !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label>{{ trans('product/form.qt') }}</label>
                        <input type="text" class="product_sizes_price form-control" value="">
                    </div>
                    <div class="form-group col-md-3">
                        <label></label>
                        <div></div>
                        <button
                                data-product_id="{{ isset($product->id) ? $product->id : '' }}"
                                data-table="product_sizes"
                                class="add-product-prop pull-right btn btn-success btn-sm"
                        >
                            {{ trans('general.add') }}
                        </button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            {{--END SIZES--}}
            <div class="form-group {{ $errors->first('colors', 'has-error') }}">
                <label for="type" class="">{{ trans('product/form.color') }}</label>
                {!!Form::select('colors[]', $colors, null, ['multiple' => 'multiple','style' =>'width:100%','class' => 'form-control select2 multiselect'])!!}
                {!! $errors->first('colors', '<span class="help-block">:message</span>') !!}
            </div>
            <div class="form-group">
                <label class="check-for-delete-file" style="margin-left: -10px;">
                    {!! Form::number('orderOnCategory', null, array('class' => 'order_page_input')) !!}
                    Nr de ordine on SHOP BY home category
                </label>
            </div>
            <div class="form-group">
                <label class="check-for-delete-file pointer">
                    <input type="checkbox" name="onMostPopularHome" @if(isset($product) && $product->onMostPopularHome == 1) checked @endif  value="1"  >
                    In Most Popular home page
                </label>
                <label class="check-for-delete-file pointer">
                    <input type="checkbox" name="onLastCollectionHome" @if(isset($product) && $product->onLastCollectionHome == 1) checked @endif  value="1"  >
                    In Last Collection home page
                </label>
            </div>
            @if(isset($product))
                <div class="form-group">
                    <a href="{{ $product->slug() }}" target="_blank">
                        View Product
                    </a>
                </div>
            @endif

            <div class="form-group btn-area-form">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                <a href="{!! URL::to('admin/product') !!}" class="btn btn-danger">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    @lang('general.back')
                </a>
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>

        </div>
    </div>
</div>