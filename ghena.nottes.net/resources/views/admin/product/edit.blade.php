@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('product/title.edit')
    @parent
@stop

@include('admin.product._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('product/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/product') }}">
                    @lang('product/form.list')
                </a>
            </li>
            <li class="active">
                @lang('product/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($product, array('url' => URL::to('admin/product/' . $product->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.product._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.product._footer')
