<div class="product_sizes_list">
    @if(isset($productSizes))
        @foreach($productSizes as $value)
            <div class="item-content-v2 product_sizes_item_{{ $value->id }}">
                <div class="form-group col-md-6" style="margin-left: -15px">
                    {{ isset($value->sizes->name_ro) ? $value->sizes->name_ro : '' }}
                </div>
                <div class="form-group col-md-3">
                    {{ $value->qt }}
                </div>
                <div class="form-group col-md-3">
                    <button
                            data-id="{{ $value->id }}"
                            data-product_id="{{ $value->id }}"
                            data-table="product_sizes"
                            class="delete-product-prop pull-right btn btn-danger btn-sm mb0"
                    >
                        {{ trans('general.delete') }}
                    </button>
                </div>
                <div class="clear"></div>
            </div>
        @endforeach
    @endif
</div>