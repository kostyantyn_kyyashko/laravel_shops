@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Charts
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/pages/flot.css') }}"/>

    <link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/morrisjs/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/dashboard2.css') }}"/>

    <style>
        .blueBG {
            background  : #418bca;
            color       : #FFF;
            font-family : Lato, sans-serif;
            margin      : 10px 0;
        }
        .greenbg {
            background  : #01BC8C;
            color       : #FFF;
            font-family : Lato, sans-serif;
            margin      : 10px 0;
        }
        .bluelighbg {
            background  : #67c5df;
            color       : #FFF;
            font-family : Lato, sans-serif;
            margin      : 10px 0;
        }
    </style>
@stop

{{-- Page content --}}

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Dashboard</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="material-icons breadmaterial">home</i>
                    Dashboard
                </a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
                <!-- Trans label pie charts strats here-->
                <div class="bluelighbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Venit Astazi</span>
                                    <div class="number" id="myTargetElement1">{{ $totalSumToday }} MDL</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
                <!-- Trans label pie charts strats here-->
                <div class="bluelighbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Comenzi Astazi</span>
                                    <div class="number" id="myTargetElement1">{{ $totalOrderedToday }}</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
                <!-- Trans label pie charts strats here-->
                <div class="bluelighbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Comenzi Neachitate Astazi</span>
                                    <div class="number" id="myTargetElement1">{{ $totalNotPaidToday }}</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
                <!-- Trans label pie charts strats here-->
                <div class="greenbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Venit 7 zile in urma</span>
                                    <div class="number" id="myTargetElement1">{{ $totalLast7Days }} MDL</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
                <!-- Trans label pie charts strats here-->
                <div class="greenbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Comenzi 7 zile in urma</span>
                                    <div class="number" id="myTargetElement1">{{ $totalOrderedLast7Days }} MDL</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
                <!-- Trans label pie charts strats here-->
                <div class="greenbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Comenzi Neachitate 7 zile in urma</span>
                                    <div class="number" id="myTargetElement1">{{ $totalNotPaidLast7Days }} MDL</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Venit pe toata perioada</span>
                                    <div class="number" id="myTargetElement1">{{ $totalFullPeriod }} MDL</div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Comenzi pe toata perioada</span>
                                    <div class="number" id="myTargetElement1">{{ $totalOrderedFullPeriod }} </div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Comenzi Neachitate pe toata perioada</span>
                                    <div class="number" id="myTargetElement1">{{ $totalNotPaidFullPeriod }} </div>
                                </div>
                                <i class="material-icons pull-right square_material">assessment</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- content -->

@stop

