@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    @lang('service/form.name')
    @parent
@stop

{{-- service level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/page.css') }}" rel="stylesheet" type="text/css">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1> @lang('service/form.name')</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="material-icons breadmaterial">home</i>
                    @lang('general.dashboard')
                </a>
            </li>
            <li class="active">@lang('service/form.name')</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary blog_service">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left">
                        <i class="fa fa-bars"></i>
                        @lang('service/title.list')
                    </h4>
                    <div class="pull-right">
                        <a href="{{ URL::to('admin/service/create') }}" class="btn btn-sm btn-default">
                            <i class="material-icons add">add</i> @lang('general.add')
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table1">
                            <thead>
                            <tr class="filters">
                                <th>@lang('service/table.name')</th>
                                <th>@lang('service/table.parent')</th>
                                <th>@lang('service/form.location')</th>
                                <th>Order</th>
                                <th>@lang('general.options')</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    <!-- row-->
    </section>

@stop

{{-- service level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script>
        $(function() {
            var table = $('#table1').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.service.data') !!}',
                order: [[ 1, "desc" ]],
                columns: [
                    { data: 'name_ro', name: 'name_ro' },
                    { data: 'parent', name: 'parent' },
                    { data: 'location', name: 'location' },
                    { data: 'order', name: 'order', width:'100px' },
                    { data: 'action', name: 'action', width:'200px', orderable: false, searchable: false },

                ]
            });
        });
    </script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="blogservice_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>
        $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
        $(document).on("click", ".blogservice_exists", function () {
            var group_name = $(this).data('name');
            $(".modal-header h4").text( group_name+" service" );
        });</script>
@stop
