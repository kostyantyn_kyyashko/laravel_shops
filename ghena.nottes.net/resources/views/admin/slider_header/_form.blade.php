<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-9">
        <ul class="nav nav-tabs">
            @foreach(langs() as $key => $lang)
                <li class="{{ $key == 0 ? 'active' : '' }}">
                    <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                        {{ $lang['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content tab-content-area" >
            @foreach(langs() as $key => $lang)
                <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                    <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                        <label for="service_name" class="">{{ trans('service/form.title') }}</label>
                        {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('service/form.title'))) !!}
                        <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                    </div>

                    <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                        <label for="service_name" class="title-textarea">{{ trans('service/form.text') }}</label>
                        {!! Form::textarea('text_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('blog/form.ph-content'))) !!}
                        {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group img-area" >
            <label>
                @lang('service/form.image')
            </label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">@lang('general.select-file')</span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
            </div>
            @if(isset($service->image) && !empty($service->image))
                <span class="fileupload-preview">
                        <a href="{{URL::to($service->image)}}" target="_blank" >
                            <img src="{{URL::to($service->image)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                        </a>
                    </span>
                <label class="check-for-delete-file">
                    <input type="checkbox" name="delete_image"  >
                    @lang('general.check_for_delete')
                </label>
            @endif
        </div>

        <div class="form-group {{ $errors->first('parent_id', 'has-error') }}" >
            {!! Form::select('parent_id',$services ,null, array('class' => 'form-control select2', 'placeholder'=>trans('service/form.parentParent'))) !!}
            <span class="help-block">{{ $errors->first('parent_id', ':message') }}</span>
        </div>
        <div class="form-group {{ $errors->first('location_id', 'has-error') }}" >
            {!! Form::select('location_id',$locations ,null, array('class' => 'form-control select2')) !!}
            <span class="help-block">{{ $errors->first('location_id', ':message') }}</span>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">

                {!! Form::number('order', null, array('class' => 'order_service_input')) !!}
                @lang('service/form.orderMenu')
            </label>
        </div>
        <div class="form-group" >
            <div class="form-group {{ $errors->first('service_url', 'has-error') }}">
                {!! Form::text('service_url', null, array('class' => 'form-control', 'placeholder'=>trans('service/form.service_url'))) !!}
                <span class="help-block">{{ $errors->first('service_url', ':message') }}</span>
            </div>
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok"></i>
                @lang('general.save')
            </button>
            <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                <i class="glyphicon glyphicon-check"></i>
                @lang('general.save_and_exit')
            </button>
            <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
        </div>
    </div>
</div>