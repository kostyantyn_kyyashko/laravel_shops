@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('service/title.create') @parent
@stop

{{-- service level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/summernote/summernote.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/page.css') }}" rel="stylesheet" type="text/css">
    <!--end of service level css-->
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('service/form.add')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i>
                @lang('general.dashboard')
            </a>
        </li>
        <li >
            <a href="{{ URL::to('admin/service') }}">
                @lang('service/form.list')
            </a>
        </li>
        <li class="active">
            @lang('service/form.addPage')
        </li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            {!! Form::open(array('url' => URL::to('admin/service'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
                @include('admin.service._form')
            {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

{{-- service level scripts --}}
@section('footer_scripts')
<!-- begining of service level js -->
<!--edit service-->
<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/materialdate/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
<script>
    $(".daterange").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        nowButton : true,
        locale: {
            format: 'DD.MM.YYYY',
        }
    });
</script>

<script src="{{ asset('assets/vendors/summernote/summernote.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}" type="text/javascript" ></script>
<script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript"></script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" ></script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript"></script>
<script  src="{{ asset('assets/js/pages/editorPage.js') }}"  type="text/javascript"></script>
@stop