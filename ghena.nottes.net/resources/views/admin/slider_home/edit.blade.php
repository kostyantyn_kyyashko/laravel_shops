@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('slider_home/title.edit')
    @parent
@stop

@include('admin.slider_home._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('slider_home/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/slider_home') }}">
                    @lang('slider_home/form.list')
                </a>
            </li>
            <li class="active">
                @lang('slider_home/form.editPage')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($slider_home, array('url' => URL::to('admin/slider_home/' . $slider_home->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.slider_home._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.slider_home._footer')