@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('slider_home/title.create') @parent
@stop

@include('admin.slider_home._head')


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('slider_home/form.add')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i>
                @lang('general.dashboard')
            </a>
        </li>
        <li >
            <a href="{{ URL::to('admin/slider_home') }}">
                @lang('slider_home/form.list')
            </a>
        </li>
        <li class="active">
            @lang('slider_home/form.addPage')
        </li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            {!! Form::open(array('url' => URL::to('admin/slider_home'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
                @include('admin.slider_home._form')
            {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

@include('admin.slider_home._footer')
