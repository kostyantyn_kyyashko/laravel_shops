<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            @foreach(langs() as $key => $lang)
                <li class="{{ $key == 0 ? 'active' : '' }}">
                    <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                        {{ $lang['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="col-md-8">
            <div class="tab-content tab-content-area" >
                @foreach(langs() as $key => $lang)
                    <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                        <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('color/form.title') }}</label>
                            {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('color/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->first('code', 'has-error') }}">
                <label for="page_name" class="">{{ trans('product/form.code') }}</label>
                {!! Form::text('code', null, array('class' => 'form-control', 'placeholder'=>trans('product/form.code'))) !!}
                <span class="help-block">{{ $errors->first('code', ':message') }}</span>
            </div>
            <div class="form-group btn-area-form">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                <a href="{!! URL::to('admin/color') !!}" class="btn btn-danger">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    @lang('general.back')
                </a>
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>

        </div>
    </div>
</div>