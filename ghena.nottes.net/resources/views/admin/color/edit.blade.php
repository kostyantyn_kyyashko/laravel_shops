@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('color/title.edit')
    @parent
@stop

@include('admin.color._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('color/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/color') }}">
                    @lang('color/form.list')
                </a>
            </li>
            <li class="active">
                @lang('color/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($color, array('url' => URL::to('admin/color/' . $color->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.color._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.color._footer')
