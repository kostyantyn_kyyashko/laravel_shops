@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('location/title.create') @parent
@stop

@include('admin.location._head')

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('location/form.add')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i>
                @lang('general.dashboard')
            </a>
        </li>
        <li >
            <a href="{{ URL::to('admin/location') }}">
                @lang('location/form.list')
            </a>
        </li>
        <li class="active">
            @lang('location/form.add')
        </li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            {!! Form::open(array('url' => URL::to('admin/location'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
                @include('admin.location._form')
            {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

@include('admin.location._footer')