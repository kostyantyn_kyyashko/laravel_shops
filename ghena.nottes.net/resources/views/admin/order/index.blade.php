@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    @lang('order/form.name')
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/page.css') }}" rel="stylesheet" type="text/css">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1> @lang('order/form.name')</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="material-icons breadmaterial">home</i>
                    @lang('general.dashboard')
                </a>
            </li>
            <li class="active">@lang('order/form.name')</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary blog_page">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left">
                        <i class="fa fa-bars"></i>
                        @lang('order/title.list')
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table1">
                            <thead>
                            <tr class="filters">
                                <th>OrderID</th>
                                <th>@lang('order/form.first_name')</th>
                                <th>@lang('order/form.date')</th>
                                <th>@lang('order/form.total')</th>
                                <th>@lang('order/form.status')</th>
                                <th>Plata</th>
                                <th>@lang('general.options')</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    <!-- row-->
    </section>

@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script>
        $(function() {
            var table = $('#table1').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.order.data') !!}',
                order: [[ 0, "desc" ]],
                columns: [
                    { data: 'id', name: 'id', width:'100px' },
                    { data: 'fullName', name: 'fullName' },
                    { data: 'date', name: 'date' },
                    { data: 'total', name: 'total' },
                    { data: 'status', name: 'status' },
                    { data: 'payType', name: 'payType' },
                    { data: 'action', name: 'action', width:'250px', orderable: false, searchable: false },

                ]
            });
        });
    </script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="blogpage_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>
        $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
        $(document).on("click", ".blogpage_exists", function () {
            var group_name = $(this).data('name');
            $(".modal-header h4").text( group_name+" page" );
        });</script>
@stop


