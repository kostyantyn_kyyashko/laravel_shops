<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">

            <div class="">
                    <div class="col-md-6 row">
                        <div class="form-group"><label for="">Detalii</label></div>
                        <ul class="media-list media-xs media-dotted">
                            <li>
                                Sutotal : {{ $order->subtotal }} MDL
                            </li>
                            @if($order->discount)
                                <li>
                                    Discount : {{ $order->discount }} %
                                </li>
                            @endif
                            <li>
                                Livrare : {{ $order->shipping > 0 ? $order->shipping : 'gratis'  }}
                            </li>
                            <li>
                                Total : {{ $order->total }} MDL
                            </li>
                            <li>
                                Data : {{ date('d.m.Y',strtotime($order->date)) }}
                            </li>
                            <li>
                                Regiunea : {{ $order->region == 'other' ? 'Alte Regiuni' : 'Chisinau' }}
                            </li>
                            <li>
                                Metoda de Plata : {{ $order->payType == 1 ? env('cash_on_delivery') : env('card_on_delivery') }}
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group"><label for="">Date Personale</label></div>
                        <ul class="media-list media-xs media-dotted">
                            <li>
                                Nume : {{ $order->first_name.' '.$order->last_name }}
                            </li>
                            <li>
                                Telefon : {{ $order->phone }}
                            </li>
                            <li>
                                Email : {{ $order->email }}
                            </li>
                            @if($order->location_id)
                                <li>
                                    Localitatea : {{ isset($order->locations->name_ro) ? $order->locations->name_ro : '' }}
                                </li>
                            @endif
                            <li>
                                Adresa : {{ $order->address }}
                            </li>
                            <li>
                                Nr Apartament : {{ $order->nrApartment }}
                            </li>
                            <li>
                                Etaj : {{ $order->floorApartment }}
                            </li>

                        </ul>
                    </div>
                    @if( $order->payNote)
                        <div class="col-md-12 row">
                            <div class="form-group"><label for="">Notite Plata</label></div>
                            <ul class="media-list media-xs media-dotted">
                                <li>
                                    {{ $order->payNote }}
                                </li>
                            </ul>
                        </div>
                    @endif

                    <div class="clear"></div>
                    <div class="col-md-12 row">
                        <div class="form-group"><label for="">Produse</label></div>
                        <ul class="media-list media-xs media-dotted">
                        @if($order->products)
                            <table class="table">
                                <tr>
                                    <th>Nume produs</th>
                                    <th>Pret</th>
                                    <th>Cantitate</th>
                                    <th>Pret Total</th>
                                </tr>
                                @foreach($order->products as $value)
                                    <tr>
                                        <td>{{ $value->name_ro }}</td>
                                        <td>{{ $value->price }} MDL</td>
                                        <td>{{ $value->pivot->qt }}</td>
                                        <td>{{ $value->pivot->qt*$value->price }} MDL</td>
                                    </tr>
                                @endforeach
                            </table>

                        @endif
                    </ul>
                    </div>

            </div>

        </div>
        <div class="col-md-4">

            <div class="form-group {{ $errors->first('status', 'has-error') }}" >
                <label for="status" class="">{{ trans('order/form.status') }}</label>
                {!! Form::select('status',$statusType ,null, array('class' => 'form-control select2', 'placeholder'=>trans('order/form.status'))) !!}
                <span class="help-block">{{ $errors->first('status', ':message') }}</span>
            </div>
            <div class="form-group btn-area-form">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                <a href="{!! URL::to('admin/order') !!}" class="btn btn-danger">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    @lang('general.back')
                </a>
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>

        </div>
    </div>
</div>