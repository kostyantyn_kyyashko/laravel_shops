@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('order/title.edit')
    @parent
@stop

@include('admin.order._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('order/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/order') }}">
                    @lang('order/form.list')
                </a>
            </li>
            <li class="active">
                @lang('order/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($order, array('url' => URL::to('admin/order/' . $order->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.order._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.order._footer')
