@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('restaurant/title.edit')
    @parent
@stop

@include('admin.restaurant._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('restaurant/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/restaurant') }}">
                    @lang('restaurant/form.list')
                </a>
            </li>
            <li class="active">
                @lang('restaurant/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($restaurant, array('url' => URL::to('admin/restaurant/' . $restaurant->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.restaurant._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.restaurant._footer')
