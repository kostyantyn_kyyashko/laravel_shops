<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">
            <ul class="nav nav-tabs">
                @foreach(langs() as $key => $lang)
                    <li class="{{ $key == 0 ? 'active' : '' }}">
                        <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                            {{ $lang['title'] }}
                        </a>
                    </li>
                @endforeach
                @if(isset($restaurant))
                    <li>
                        <a href="#gallery" class="upppercase" data-toggle="tab" aria-expanded="true">
                            {{ trans('restaurant/form.gallery') }}
                        </a>
                    </li>
                @endif
            </ul>
            <div class="tab-content tab-content-area" >
                @foreach(langs() as $key => $lang)
                    <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                        <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('restaurant/form.title') }}</label>
                            {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('restaurant/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="title-textarea">{{ trans('restaurant/form.text') }}</label>
                            {!! Form::textarea('text_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('restaurant/form.text'))) !!}
                            {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->first('text2_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="title-textarea">{{ trans('restaurant/form.text2') }}</label>
                            {!! Form::textarea('text2_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('restaurant/form.text'))) !!}
                            {!! $errors->first('text2_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                @endforeach
                @if(isset($restaurant))
                    <div id="gallery" class="tab-pane">
                        <div class="form-group">
                            <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
                            <span class="btn btn-default btn-sm">
                                <span class="fileupload-new">@lang('restaurant/form.select-file')</span>
                                <input type="file"
                                       name="image_do_upload"
                                       id="upload_gallery"
                                       class="uploader_about_popup"
                                       data-url="/admin/upload/do_upload"
                                       data-form-data='{"data-type": "restaurant_gallery","_token" : "{{ csrf_token() }}","id":"{{ $restaurant->id }}"}'
                                       class=""
                                >

                            </span>
                            </div>
                            <input type='button' class="btn btn-primary pull-right" value='{{ trans('general.save_order') }}' id='submit' />
                            <div class="loading pull-right"></div>

                        </div>
                        <div class="clear"></div>
                        <div class="form-group success-div hide">
                            <div class="alert alert-success alert-dismissable margin5">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Success:</strong> Cu success
                            </div>
                        </div>
                        @if($gallery)
                            <ul id="image-list" >
                                @foreach($gallery as $value)
                                    <li id="image_{{ $value->id }}" >
                                        <img src="{{ $value->path }}" >
                                        <a href="javascript:void(0)" onclick='return deleteImage("{{ $value->id }}");' >
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->first('lat', 'has-error') }}">
                <label for="page_name" class="">{{ trans('restaurant/form.lat') }}</label>
                {!! Form::text('lat', null, array('class' => 'form-control', 'placeholder'=>trans('restaurant/form.lat'))) !!}
                <span class="help-block">{{ $errors->first('lat', ':message') }}</span>
            </div>
            <div class="form-group {{ $errors->first('lng', 'has-error') }}">
                <label for="page_name" class="">{{ trans('restaurant/form.lng') }}</label>
                {!! Form::text('lng', null, array('class' => 'form-control', 'placeholder'=>trans('restaurant/form.lng'))) !!}
                <span class="help-block">{{ $errors->first('lng', ':message') }}</span>
            </div>

            <div class="form-group col-md-12 row-bootsrap">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                <a href="{!! URL::to('admin/restaurant') !!}" class="btn btn-danger">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    @lang('general.back')
                </a>
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>
        </div>
    </div>
</div>