{{-- page level scripts --}}
@section('footer_scripts')

    <style>
        .hide {
            display: none;
        }
    </style>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/materialdate/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/summernote/summernote.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}" ></script>

    <script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript"></script>

    <script  src="{{ asset('assets/js/pages/editorPage.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/admin/jquery.fileupload.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/admin/adminConfig.js') }}"  type="text/javascript"></script>
    <script>
        $('textarea#ckeditor_full').ckeditor({
            height: '70px',
            toolbar: [
                {
                    name: 'document',
                    items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates']
                },
                ['Cut', 'Copy', 'Paste', 'PasteText'], // Defines toolbar group without name.
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic']
                },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','BidiLtr','BidiRtl','Table' ] },
                { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
                { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
                { name: 'colors', items : [ 'TextColor','BGColor' ] },
                { name: 'tools', items : [ 'Maximize', 'ShowBlocks'] }
            ]
        });
        $('textarea#ckeditor_full_v1').ckeditor();
    </script>

    {{--gallery--}}
    <script src="{{ asset('assets/js/admin/jquery-ui-1.10.4.custom.js') }}"  type="text/javascript"></script>
    <script>

        function deleteImage(id)
        {
            var x = confirm("Esti sigur ca doreste sa stergi imaginea?");
            if (x) {
                $.ajax({
                    url: '/admin/upload/removeGallery',
                    type: 'post',
                    data: { id: id,'data-type' : 'restaurant_gallery',"_token" : "{{ csrf_token() }}" },
                    success: function (resp) {
                        $("#image_"+id).remove();
                    }
                });
            } else {
                return false;
            }

        }

        $(document).ready(function () {

            // todo add upload_gallery on adminConfig.js

            var dropIndex;
            $("#image-list").sortable({
                update: function (event, ui) {
                    dropIndex = ui.item.index();
                }
            });

            $('#submit').click(function (e) {
                $(".success-div").addClass('hide');
                var galleryIDs = [];
                $('#image-list li').each(function (index) {
                    // if(index <= dropIndex) {
                    //     var id = $(this).attr('id');
                    //     var split_id = id.split("_");
                    //     galleryIDs.push(split_id[1]);
                    // }
                    galleryIDs.push($(this).attr('id'));
                });

                $.ajax({
                    url: '/admin/upload/reorderGallery',
                    type: 'post',
                    data: {galleryIDs: galleryIDs,'data-type' : 'restaurant_gallery',"_token" : "{{ csrf_token() }}" },
                    success: function (resp) {
                        $(".success-div").removeClass('hide');
                    }
                });
                e.preventDefault();
            });


        });

    </script>
@stop