@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('restaurant/title.create') @parent
@stop

@include('admin.restaurant._head')

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('restaurant/form.add')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i>
                @lang('general.dashboard')
            </a>
        </li>
        <li >
            <a href="{{ URL::to('admin/restaurant') }}">
                @lang('restaurant/form.list')
            </a>
        </li>
        <li class="active">
            @lang('restaurant/form.add')
        </li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            {!! Form::open(array('url' => URL::to('admin/restaurant'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
                @include('admin.restaurant._form')
            {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

@include('admin.restaurant._footer')