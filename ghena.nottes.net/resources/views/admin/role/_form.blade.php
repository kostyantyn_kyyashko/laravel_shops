<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">
            <div class="form-group{{ $errors->first('name', 'has-error') }}"  >
                <label for="page_name" class="">{{ trans('general.name') }}</label>
                {!! Form::text('name', null, array('class' => 'form-control', 'placeholder'=>trans('general.name'))) !!}
                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group btn-area-form">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                @if(Sentinel::getUser()->id == 1)
                    <a href="javascript:void(0)" class="btn btn-danger selectall">
                        Check All
                    </a>
                @endif
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>
        </div>
        @if(isset($permissions))
            <div class="col-md-12 row">
                @foreach($permissions as $value)
                    <div class="col-md-2">
                       <div class="permission-block">
                           <h4>{{ $value->name }}</h4>
                           <div class="from-group"  >
                               <input type="checkbox"
                                      name="{{ $value->slug }}.view"
                                      @if(isset($role_permissions[$value->slug.'.view']) && $role_permissions[$value->slug.'.view'] == true)
                                        checked="checked"
                                      @endif
                                      id="view-{{$value->id}}"
                                      class="checkbox_item"
                               >
                               <label for="view-{{$value->id}}" class="pointer">{{ trans('general.viewPermision') }}</label>
                           </div>
                           <div class="from-group"  >
                               <input type="checkbox"
                                      name="{{ $value->slug }}.edit"
                                      @if(isset($role_permissions[$value->slug.'.edit']) && $role_permissions[$value->slug.'.edit'] == true)
                                      checked="checked"
                                      @endif
                                      id="edit-{{$value->id}}"
                                      class="checkbox_item"
                               >
                               <label for="edit-{{$value->id}}" class="pointer">{{ trans('general.edit') }}</label>
                           </div>
                           <div class="from-group"  >
                               <input type="checkbox"
                                      name="{{ $value->slug }}.delete"
                                      @if(isset($role_permissions[$value->slug.'.delete']) && $role_permissions[$value->slug.'.delete'] == true)
                                      checked="checked"
                                      @endif
                                      id="delete-{{$value->id}}"
                                      class="checkbox_item"
                               >
                               <label for="delete-{{$value->id}}" class="pointer">{{ trans('general.delete') }}</label>
                           </div>
                           <div class="clear"></div>
                       </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
@section('footer_scripts')
<script>
    $(function () {
        $('.selectall').click(function () {
            $('.permission-block').find(':checkbox').attr('checked','checked');
            $(".checkbox_item").prop('checked', 'checked');
            $(".checkbox_item").attr('checked', 'checked');
        });
    });
</script>
@stop
