@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('role/title.edit')
    @parent
@stop

@include('admin.role._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('role/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/role') }}">
                    @lang('role/form.list')
                </a>
            </li>
            <li class="active">
                @lang('role/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($role, array('url' => URL::to('admin/role/' . $role->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.role._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.role._footer')
