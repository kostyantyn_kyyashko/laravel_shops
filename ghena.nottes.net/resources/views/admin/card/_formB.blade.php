<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">
            <ul class="nav nav-tabs">
                @foreach(langs() as $key => $lang)
                    <li class="{{ $key == 0 ? 'active' : '' }}">
                        <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                            {{ $lang['title'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content tab-content-area" >
                @foreach(langs() as $key => $lang)
                    <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                        <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('card/form.title') }}</label>
                            {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('card/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="title-textarea">{{ trans('card/form.text') }}</label>
                            {!! Form::textarea('text_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('card/form.text'))) !!}
                            {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="image-area-list row">
                <div class="form-group img-area pull-left" >
                    <label>@lang('card/form.image')</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">@lang('card/form.select-file')</span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
                    </div>
                </div>
                @if(isset($card->image) && !empty($card->image))
                    <div class="form-group pull-left mleft-20">
                    <span class="fileupload-preview">
                        <a href="{{URL::to($card->image)}}" target="_blank" >
                            <img src="{{URL::to($card->image)}}" style="max-height: 55px" class="img-responsive" alt="Image">
                        </a>
                    </span>
                        <label class="check-for-delete-file">
                            <input type="checkbox" name="delete_image"  >
                            @lang('general.check_for_delete')
                        </label>
                    </div>
                @endif
            </div>
            <div class="form-group {{ $errors->first('price', 'has-error') }}">
                <label for="page_name" class="">{{ trans('card/form.price') }}</label>
                {!! Form::text('price', null, array('class' => 'form-control', 'placeholder'=>trans('card/form.price'))) !!}
                <span class="help-block">{{ $errors->first('price', ':message') }}</span>
            </div>
            <div class="form-group btn-area-form">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                <a href="{!! URL::to('admin/card') !!}" class="btn btn-danger">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    @lang('general.back')
                </a>
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>

        </div>
    </div>
</div>