@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('card/title.edit')
    @parent
@stop

@include('admin.card._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('card/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/card') }}">
                    @lang('card/form.list')
                </a>
            </li>
            <li class="active">
                @lang('card/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($card, array('url' => URL::to('admin/card/' . $card->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.card._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.card._footer')
