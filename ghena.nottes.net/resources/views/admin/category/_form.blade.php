<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-9">
        <ul class="nav nav-tabs">
            @foreach(langs() as $key => $lang)
                <li class="{{ $key == 0 ? 'active' : '' }}">
                    <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                        {{ $lang['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content tab-content-area" >
            @foreach(langs() as $key => $lang)
                <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                    <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                        <label for="category_name" class="">{{ trans('category/form.title') }}</label>
                        {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('category/form.title'))) !!}
                        <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('name2_'.$lang['code'], 'has-error') }}">
                        <label for="category_name" class="">{{ trans('category/form.title2') }}</label>
                        {!! Form::text('name2_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('category/form.title2'))) !!}
                        <span class="help-block">{{ $errors->first('name2_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                        <label for="category_name" class="title-textarea">{{ trans('category/form.text') }}(col1,short text)</label>
                        {!! Form::textarea('text_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('blog/form.ph-content'))) !!}
                        {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('text2_'.$lang['code'], 'has-error') }}">
                        <label for="category_name" class="title-textarea">{{ trans('category/form.text2') }}(col2,log text,short title(Babydoll-Kleid mit Tupfenprint))</label>
                        {!! Form::textarea('text2_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('blog/form.ph-content'))) !!}
                        {!! $errors->first('text2_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('price_'.$lang['code'], 'has-error') }}">
                        <label for="category_name" class="">{{ trans('category/form.price') }}</label>
                        {!! Form::text('price_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('category/form.price'))) !!}
                        <span class="help-block">{{ $errors->first('price_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('link_'.$lang['code'], 'has-error') }}">
                        <label for="category_name" class="">{{ trans('category/form.link') }}</label>
                        {!! Form::text('link_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('category/form.link'))) !!}
                        <span class="help-block">{{ $errors->first('link_'.$lang['code'], ':message') }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group img-area" >
            <label>@lang('category/form.image') 1</label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">@lang('general.select-file')</span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
            </div>
            @if(isset($category->image) && !empty($category->image))
                <span class="fileupload-preview">
                        <a href="{{URL::to($category->image)}}" target="_blank" >
                            <img src="{{URL::to($category->image)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                        </a>
                    </span>
                <label class="check-for-delete-file">
                    <input type="checkbox" name="delete_image"  >
                    @lang('general.check_for_delete')
                </label>
            @endif
        </div>
        {{--IMAGE 1--}}
        <div class="form-group img-area" >
            <label>@lang('page/form.image')  2</label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('general.select-file')</span>
                            {!! Form::file('image1', null, array('class' => 'form-control')) !!}
                        </span>
            </div>
            @if(isset($category->image1) && !empty($category->image1))
                <span class="fileupload-preview">
                            <a href="{{URL::to($category->image1)}}" target="_blank" >
                                <img src="{{URL::to($category->image1)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                            </a>
                        </span>
                <label class="check-for-delete-file">
                    <input type="checkbox" name="delete_image1"  >
                    @lang('general.check_for_delete')
                </label>
            @endif
        </div>
        {{--IMAGE 2--}}
        <div class="form-group img-area" >
            <label>@lang('page/form.image') 3</label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('general.select-file')</span>
                            {!! Form::file('image2', null, array('class' => 'form-control')) !!}
                        </span>
            </div>
            @if(isset($category->image2) && !empty($category->image2))
                <span class="fileupload-preview">
                            <a href="{{URL::to($category->image2)}}" target="_blank" >
                                <img src="{{URL::to($category->image2)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                            </a>
                        </span>
                <label class="check-for-delete-file">
                    <input type="checkbox" name="delete_image2"  >
                    @lang('general.check_for_delete')
                </label>
            @endif
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">
                {!! Form::number('order', null, array('class' => 'order_category_input')) !!}
                @lang('category/form.orderMenu')
            </label>
        </div>
        <div class="form-group {{ $errors->first('parent_id', 'has-error') }}" >
            {!! Form::select('parent_id',$categorys ,null, array('class' => 'form-control select2', 'placeholder'=>trans('page/form.parentParent'))) !!}
            <span class="help-block">{{ $errors->first('parent_id', ':message') }}</span>
        </div>
        <div class="form-group {{ $errors->first('product1_id', 'has-error') }}" >
            <label>Product 1 home shop by</label>
            {!! Form::select('product1_id',$products ,null, array('class' => 'form-control multiselect select2', 'placeholder'=>trans('slider_home/form.selectProduct'))) !!}
            <span class="help-block">{{ $errors->first('product1_id', ':message') }}</span>
        </div>
        <div class="form-group {{ $errors->first('product2_id', 'has-error') }}" >
            <label>Product 1 home shop by</label>
            {!! Form::select('product2_id',$products ,null, array('class' => 'form-control multiselect select2', 'placeholder'=>trans('slider_home/form.selectProduct'))) !!}
            <span class="help-block">{{ $errors->first('product2_id', ':message') }}</span>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file pointer">
                <input type="checkbox" name="onHomeShopBy" @if(isset($category) && $category->onHomeShopBy == 1) checked @endif  value="1"  >
                In home page at bottom on SHOP BY
            </label>
            <label class="check-for-delete-file pointer">
                <input type="checkbox" name="isCollection" @if(isset($category) && $category->isCollection == 1) checked @endif  value="1"  >
                Is collection for serach filter on left
            </label>
        </div>
        <div class="form-group {{ $errors->first('page_id', 'has-error') }}" >
            {!! Form::select('page_id',$pages ,null, array('class' => 'form-control select2', 'placeholder'=>'Page Submenu In Header')) !!}
            <span class="help-block">{{ $errors->first('page_id', ':message') }}</span>
        </div>
        <div class="form-group" >
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok"></i>
                @lang('general.save')
            </button>
            <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                <i class="glyphicon glyphicon-check"></i>
                @lang('general.save_and_exit')
            </button>
            <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
        </div>
    </div>
</div>