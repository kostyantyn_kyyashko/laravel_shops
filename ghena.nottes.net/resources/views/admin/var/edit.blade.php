@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('blog/title.edit')
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/app.css') }}" rel="stylesheet" type="text/css" />
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('var/form.edit')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
            </a>
        </li>
        <li >
            <a href="{{ URL::to('admin/var') }}">
                @lang('var/form.list')
            </a>
        </li>
        <li class="active">
            @lang('var/form.editVar')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <i class="fa fa-edit"></i>
                        @lang('var/form.editVar')
                    </h4>
                </div>
                <div class="panel-body">

                    {!! Form::model($var, ['url' => URL::to('admin/var/'. $var->id), 'method' => 'put', 'class' => 'form-horizontal', 'files'=> true]) !!}
                        @include('admin.var._form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop