<ul class="nav nav-tabs">
    @foreach(langs() as $key => $lang)
        <li class="{{ $key == 0 ? 'active' : '' }}">
            <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                {{ $lang['title'] }}
            </a>
        </li>
    @endforeach
</ul>
<div class="tab-content tab-content-area" >
    @foreach(langs() as $key => $lang)
        <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
            <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                <label for="title" class="col-sm-2 control-label">
                    @lang('var/form.title')
                </label>
                <div class="col-sm-5">
                    {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('var/form.varName'))) !!}
                </div>
                <div class="col-sm-4">
                    {!! $errors->first('name_'.$lang['code'], '<span class="help-block">:message</span> ') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                <label for="title" class="col-sm-2 control-label">
                    @lang('var/form.info')
                </label>
                <div class="col-sm-5">
                    {!! Form::text('text_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('var/form.varName'))) !!}
                </div>
                <div class="col-sm-4">
                    {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span> ') !!}
                </div>
            </div>
        </div>
    @endforeach
</div>
@if(Sentinel::getUser()->id == 1)
    <div class="form-group {{ $errors->first('var', 'has-error') }}">
        <label for="title" class="col-sm-2 control-label">
            @lang('var/form.var')
        </label>
        <div class="col-sm-5">
            {!! Form::text('var', null, array('class' => 'form-control', 'placeholder'=>trans('var/form.var'))) !!}
        </div>
        <div class="col-sm-4">
            {!! $errors->first('var', '<span class="help-block">:message</span> ') !!}
        </div>
    </div>
@endif
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i>
            @lang('general.save')
        </button>
        <a class="btn btn-danger" href="{{ URL::to('admin/var/') }}">
            <i class="fa fa-rotate-left"></i>
            @lang('general.cancel')
        </a>
    </div>
</div>