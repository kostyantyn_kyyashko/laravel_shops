@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Add User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>@lang('general.editUser')</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="material-icons breadmaterial">home</i>
                    Dashboard
                </a>
            </li>
            <li><a href="#"> @lang('general.users')</a></li>
            <li class="active">@lang('general.editUser')</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            @lang('general.editUser')
                        </h3>
                        <span class="pull-right clickable">
                                    <i class="material-icons">keyboard_arrow_up</i>
                                </span>
                    </div>
                    <div class="panel-body">
                        <!--main content-->
                        {!! Form::model($user, array('url' => URL::to('admin/users/' . $user->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}

                        <!-- CSRF Token -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <input type="hidden" name="activate" value="1"/>

                            <div id="rootwizard">
                            <ul>
                                <li><a href="#tab1" data-toggle="tab">@lang('general.user_profile')</a></li>
                            </ul>
                            <div class="tab-content" style="margin-top: -35px;">
                                <div class="tab-pane" id="tab1">
                                    <h2 class="hidden">&nbsp;</h2>
                                    <div class="form-group {{ $errors->first('first_name', 'has-error') }}">
                                        <label for="first_name" class="col-sm-2 control-label">@lang('general.first_name') *</label>
                                        <div class="col-sm-10">
                                            <input id="first_name" name="first_name" type="text"
                                                   placeholder="@lang('general.first_name')" class="form-control required"
                                                   value="{!! $user->first_name !!}"/>

                                            {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->first('last_name', 'has-error') }}">
                                        <label for="last_name" class="col-sm-2 control-label">@lang('general.last_name') *</label>
                                        <div class="col-sm-10">
                                            <input id="last_name" name="last_name" type="text"
                                                   placeholder="@lang('general.last_name')"
                                                   class="form-control required" value="{!! $user->last_name !!}"/>

                                            {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                        <label for="email" class="col-sm-2 control-label">Username *</label>
                                        <div class="col-sm-10">
                                            <input id="email" name="email" placeholder="Username" type="text"
                                                   class="form-control required email"
                                                   value="{!! $user->email !!}"/>
                                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                        <label for="password" class="col-sm-2 control-label">@lang('general.password') *</label>
                                        <div class="col-sm-10">
                                            <input id="password" name="password" type="password"
                                                   placeholder="@lang('general.password')"
                                                   class="form-control required" value="{!! old('password') !!}"/>
                                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
                                        <label for="password_confirm" class="col-sm-2 control-label">
                                            @lang('general.confirmPassword') *</label>
                                        <div class="col-sm-10">
                                            <input id="password_confirm" name="password_confirm" type="password"
                                                   placeholder="@lang('general.confirmPassword') " class="form-control required"
                                            />
                                            {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label for="group" class="col-sm-2 control-label">Roluri *</label>
                                        <div class="col-sm-10">
                                            <select class="form-control required" title="Select group..." name="groups"
                                                    id="group">
                                                @foreach($groups as $role)
                                                    @if($role->id > 1)
                                                        <option value="{!! $role->id !!}" {{ (array_key_exists($role->id, $userRoles) ? ' selected="selected"' : '') }}>{{ $role->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            {!! $errors->first('group', '<span class="help-block">:message</span>') !!}
                                        </div>
                                        <span class="help-block">{{ $errors->first('group', ':message') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->first('money', 'has-error') }}">
                                        <label for="money" class="col-sm-2 control-label">
                                            Bani in cont
                                        </label>
                                        <div class="col-sm-10">
                                            <input id="money" name="money" type="text"
                                                   class="form-control required"
                                                   value="{!! $user->money !!}"
                                            />
                                            {!! $errors->first('money', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">@lang('general.save')</button>
                                <ul class="pager wizard">
                                    <li class="next finish" >

                                    </li>
                                </ul>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editConfirmModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Activate Confirm</h4>
                    </div>
                    <div class="modal-body">
                        <p>Please check the checkbox to active your account</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>
@stop
