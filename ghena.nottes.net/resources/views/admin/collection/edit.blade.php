@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('collection/title.edit')
    @parent
@stop

@include('admin.collection._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('collection/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/collection') }}">
                    @lang('collection/form.list')
                </a>
            </li>
            <li class="active">
                @lang('collection/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($collection, array('url' => URL::to('admin/collection/' . $collection->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.collection._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.collection._footer')
