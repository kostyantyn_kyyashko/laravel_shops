{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/materialdate/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script>
        $(".daterange").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            nowButton : true,
            locale: {
                format: 'DD.MM.YYYY',
            }
        });
    </script>

    <script type="text/javascript" src="{{ asset('assets/vendors/summernote/summernote.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}" ></script>

    <script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/pages/editorPage.js') }}"  type="text/javascript"></script>

    <script>
        $(function() {
            $( ".image-select" ).click(function() {
                $('.fileupload-preview').remove();
            });
        });
    </script>

    {{--gallery--}}
    <link href="{{ asset('assets/css/admin/custom.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('assets/js/admin/jquery.fileupload.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/admin/adminConfig.js') }}"  type="text/javascript"></script>

    <script src="{{ asset('assets/js/admin/jquery-ui-1.10.4.custom.js') }}"  type="text/javascript"></script>
    <script>

        function deleteImage(id)
        {
            var x = confirm("Esti sigur ca doreste sa stergi imaginea?");
            if (x) {
                $.ajax({
                    url: '/admin/upload/removeGallery',
                    type: 'post',
                    data: { id: id,'data-type' : 'collection_gallery',"_token" : "{{ csrf_token() }}" },
                    success: function (resp) {
                        $("#image_"+id).remove();
                    }
                });
            } else {
                return false;
            }

        }

        $(document).ready(function () {
            var dropIndex;
            $("#image-list").sortable({
                update: function (event, ui) {
                    dropIndex = ui.item.index();
                }
            });

            $('#submit').click(function (e) {
                $(".success-div").addClass('hide');
                var galleryIDs = [];
                $('#image-list li').each(function (index) {
                    if(index <= dropIndex) {
                        var id = $(this).attr('id');
                        var split_id = id.split("_");
                        galleryIDs.push(split_id[1]);
                    }
                });

                $.ajax({
                    url: '/admin/upload/reorderGallery',
                    type: 'post',
                    data: {galleryIDs: galleryIDs,'data-type' : 'product_gallery',"_token" : "{{ csrf_token() }}" },
                    success: function (resp) {
                        $(".success-div").removeClass('hide');
                    }
                });
                e.preventDefault();
            });


        });
    </script>
@stop