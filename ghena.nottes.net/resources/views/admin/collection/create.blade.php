@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('collection/title.create') @parent
@stop

@include('admin.collection._head')

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('collection/form.add')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i>
                @lang('general.dashboard')
            </a>
        </li>
        <li >
            <a href="{{ URL::to('admin/collection') }}">
                @lang('collection/form.list')
            </a>
        </li>
        <li class="active">
            @lang('collection/form.add')
        </li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            {!! Form::open(array('url' => URL::to('admin/collection'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
                @include('admin.collection._form')
            {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

@include('admin.collection._footer')