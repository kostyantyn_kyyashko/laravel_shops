<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">
            <ul class="nav nav-tabs">
                @foreach(langs() as $key => $lang)
                    <li class="{{ $key == 0 ? 'active' : '' }}">
                        <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                            {{ $lang['title'] }}
                        </a>
                    </li>
                @endforeach
                @if(isset($collection))
                    <li>
                        <a href="#gallery" class="upppercase" data-toggle="tab" aria-expanded="true">
                            {{ trans('general.gallery') }}
                        </a>
                    </li>
                @endif
            </ul>
            <div class="tab-content tab-content-area" >
                @foreach(langs() as $key => $lang)
                    <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                        <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('collection/form.title') }}</label>
                            {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('collection/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('name2_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">Title on Circle</label>
                            {!! Form::text('name2_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('collection/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name2_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('video_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('general.video') }}</label>
                            {!! Form::text('video_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('general.video'))) !!}
                            <span class="help-block">{{ $errors->first('video_'.$lang['code'], ':message') }}</span>
                        </div>
                        <div class="form-group {{ $errors->first('link_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('general.link') }}</label>
                            {!! Form::text('link_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('general.link'))) !!}
                            <span class="help-block">{{ $errors->first('link_'.$lang['code'], ':message') }}</span>
                        </div>
                    </div>
                @endforeach
                @if(isset($collection))
                    <div id="gallery" class="tab-pane">
                        <div class="form-group">
                            <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
                            <span class="btn btn-default btn-sm">
                                <span class="fileupload-new">@lang('page/form.select-file')</span>
                                <input type="file"
                                       name="image_do_upload"
                                       id="upload_gallery"
                                       class="uploader_about_popup"
                                       data-url="/admin/upload/do_upload"
                                       data-form-data='{"data-type": "collection_gallery","_token" : "{{ csrf_token() }}","id":"{{ $collection->id }}"}'
                                       class=""
                                >
                            </span>
                            </div>
                            <input type='button' class="btn btn-primary pull-right" value='{{ trans('general.save_order') }}' id='submit' />
                            <div class="loading pull-right"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="form-group success-div hide">
                            <div class="alert alert-success alert-dismissable margin5">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Success:</strong> Cu success
                            </div>
                        </div>
                        @if($gallery)
                            <ul id="image-list" >
                                @foreach($gallery as $value)
                                    <li id="image_{{ $value->id }}" >
                                        <img src="{{ $value->path }}" >
                                        <a href="javascript:void(0)" onclick='return deleteImage("{{ $value->id }}");' >
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group col-md-4">
            <div class="form-group img-area" >
                <label>@lang('category/form.image')</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">@lang('general.select-file')</span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
                </div>
                @if(isset($collection->image) && !empty($collection->image))
                    <span class="fileupload-preview">
                        <a href="{{URL::to($collection->image)}}" target="_blank" >
                            <img src="{{URL::to($collection->image)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                        </a>
                    </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            {{--IMAGE 1--}}
            <div class="form-group img-area" >
                <label>@lang('page/form.image') Image for Video</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('general.select-file')</span>
                            {!! Form::file('image1', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($collection->image1) && !empty($collection->image1))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($collection->image1)}}" target="_blank" >
                                <img src="{{URL::to($collection->image1)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image1"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            <div class="form-group">
                <label class="check-for-delete-file">
                    <input type="checkbox" name="onNewsFooter" @if(isset($collection) && $collection->onNewsFooter == 1) checked @endif  value="1"  >
                    Is on News Footer
                </label>
            </div>
            <div class="form-group"></div>
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok"></i>
                @lang('general.save')
            </button>
            <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                <i class="glyphicon glyphicon-check"></i>
                @lang('general.save_and_exit')
            </button>
            <a href="{!! URL::to('admin/collection') !!}" class="btn btn-danger">
                <i class="glyphicon glyphicon-arrow-left"></i>
                @lang('general.back')
            </a>
            <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
        </div>

    </div>
</div>