<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>
        @section('title')
            | {{ ENV('APP_NAME') }}
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    [endif]-->
    <!-- global css -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('assets/css/admin/app.css') }}" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->


    <!-- end of global css -->
    <!--page level css-->
    @yield('header_styles')
    <style>
        .page-sidebar-menu ,.page-sidebar-menu  {
            min-height: 868px !important;
        }

    </style>
            <!--end of page level css-->

<body class="skin-josh">
<header class="header">
    <a href="{{ route('admin.dashboard') }}" class="logo">
        <img src="{{ asset('assets/img/logo.png') }}" style="height: 55px" alt="logo">
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <div class="responsive_nav"></div>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">

                @include('admin.layouts._top_right_menu')

            </ul>
        </div>
    </nav>
</header>
<div class="wrapper">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side">
        <section class="sidebar ">
            <div class="page-sidebar  sidebar-nav">
                <div class="nav_icons">
                    <ul class="sidebar_threeicons">
                        <li>
                            <a href="{{ URL::to('admin/news/create') }}">
                                <i class="material-icons fsize">note_add</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/news') }}">
                                <i class="material-icons fsize">open_with</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/page') }}">
                                <i class="material-icons fsize">chrome_reader_mode</i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/var') }}">
                                <i class="material-icons fsize">translate</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <!-- BEGIN SIDEBAR MENU -->
                @include('admin.layouts._left_menu')
                <!-- END SIDEBAR MENU -->
            </div>
        </section>
    </aside>
    <aside class="right-side">

        {{--<!-- Notifications -->--}}
        <div id="notification_remove">
            @include('notifications')
        </div>

                <!-- Content -->
        @yield('content')

    </aside>
    <!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary  back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="material-icons btn_tak fsize">flight_takeoff</i>
</a>
<!-- global js -->

<script src="{{ asset('assets/js/admin/app.js') }}" type="text/javascript"></script>
<!-- end of global js -->
<!-- begin page level js -->
@yield('footer_scripts')
        <!-- end page level js -->
</body>
</html>
