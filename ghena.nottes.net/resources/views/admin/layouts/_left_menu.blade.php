<ul id="menu" class="page-sidebar-menu">
    <li {!! (Request::segment(2) == 'page' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/page') }}">
            <i class="material-icons leftsize">chrome_reader_mode</i>
            @lang('page/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'slider_home' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/slider_home') }}">
            <i class="material-icons leftsize">chrome_reader_mode</i>
            @lang('slider_home/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'category' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/category') }}">
            <i class="fa fa-reorder"></i>
            @lang('category/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'product' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/product') }}">
            <i class="fa fa-reorder"></i>
            @lang('product/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'size' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/size') }}">
            <i class="fa fa-tasks"></i>
            @lang('size/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'color' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/color') }}">
            <i class="fa fa-tasks"></i>
            @lang('color/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'location' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/location') }}">
            <i class="fa fa-reorder"></i>
            @lang('location/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'collection' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/collection') }}">
            <i class="material-icons leftsize">chrome_reader_mode</i>
            @lang('collection/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'news' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/news') }}">
            <i class="material-icons leftsize">chrome_reader_mode</i>
            @lang('news/form.name')
        </a>
    </li>
    <li {!! (Request::segment(2) == 'partner' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/partner') }}">
            <i class="material-icons leftsize">chrome_reader_mode</i>
            @lang('partner/form.name')
        </a>
    </li>

    <li {!! Request::segment(2) == 'order'   ? 'class="active"' : '' !!}>
        <a href="{{  URL::to('admin/order') }}">
            <i class="fa fa-credit-card"></i>
            @lang('order/form.name')
        </a>
    </li>
    <li {!! Request::segment(2) == 'email'   ? 'class="active"' : '' !!}>
        <a href="{{  URL::to('admin/email') }}">
            <i class="fa fa-envelope"></i>
            @lang('email/form.name')
        </a>
    </li>
    <li {!! Request::segment(2) == 'users'   ? 'class="active"' : '' !!}>
        <a href="{{  URL::to('admin/users') }}">
            <i class="fa fa-users"></i>
            @lang('general.users')
        </a>
    </li>

    <li {!! (Request::is('admin/var') || Request::is('admin/var/create') || Request::is('admin/var/edit')  ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/var') }}">
            <i class="material-icons leftsize">translate</i>
            @lang('var/form.name')
        </a>
    </li>


    <!-- Menus generated by CRUD generator -->
    @include('admin/layouts/menu')
</ul>
