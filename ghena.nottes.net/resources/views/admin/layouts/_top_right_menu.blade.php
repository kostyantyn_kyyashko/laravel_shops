<li class="dropdown user user-menu">
    <a href="/" target="_blank" style="line-height: 35px;color: white">
        <i class="material-icons leftsize">gavel</i>
    </a>
</li>
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        @if(Sentinel::getUser()->pic)
            <img src="{!! url('/').'/uploads/users/'. Sentinel::getUser()->pic !!}" alt="img"
                 class="img-circle  img-responsive pull-left" width="35" height="35"/>
        @else
            <img src="{{ asset('assets/img/authors/no_avatar.jpg') }}" alt="..."
                 class="img-circle img-responsive pull-left" width="35" height="35"/>

        @endif
        <div class="riot">
            <div>
                <p class="user_name_max" id="user_nav">{{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}</p>
                <span>
                            <i class="caret"></i>
                        </span>
            </div>
        </div>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header bg-light-blue">

            @if(Sentinel::getUser()->pic)
                <img src="{!! url('/').'/uploads/users/'.Sentinel::getUser()->pic !!}" alt="img"
                     class="img-circle  "/>
            @elseif(Sentinel::getUser()->gender === "male")
                <img src="{{ asset('assets/images/authors/avatar3.png') }}" alt="..."
                     class="img-circle "/>
            @elseif(Sentinel::getUser()->gender === "female")
                <img src="{{ asset('assets/images/authors/avatar5.png') }}" alt="..."
                     class="img-circle "/>
            @else
                <img src="{{ asset('assets/img/authors/no_avatar.jpg') }}" alt="..."
                     class="img-circle "/>

            @endif
            <p class="topprofiletext">{{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}</p>
        </li>
        <!-- Menu Body -->
        <li>
            <a href="{{ URL::route('admin.users.show',Sentinel::getUser()->id) }}">
                <i class="material-icons">person</i>
                Profil Personal
            </a>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="{{ URL::route('lockscreen',Sentinel::getUser()->id) }}">
                    <i class="material-icons">lock</i>
                    Lock
                </a>
            </div>
            <div class="pull-right">
                <a href="{{ URL::to('admin/logout') }}">
                    <i class="material-icons">exit_to_app</i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</li>