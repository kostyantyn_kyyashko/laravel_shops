<div class="panel panel-primary ">
    <div class="panel-heading clickable panel-collapsed ">
        <h3 class="panel-title">
            <i class="material-icons">show_chart</i> SEO
        </h3>
        <span class="pull-right">
            <i class="material-icons showhide">keyboard_arrow_down</i>
        </span>
    </div>
    <div class="panel-body" style="display: none" >
        <div class="form-group {{ $errors->first('seo_title_'.$lang['code'], 'has-error') }}">
            <label class="">{{ trans('general.seo_title') }}</label>
            {!! Form::text('seo_title_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('general.seo_title'))) !!}
            <span class="help-block">{{ $errors->first('seo_title_'.$lang['code'], ':message') }}</span>
        </div>
        <div class="form-group {{ $errors->first('seo_keywords_'.$lang['code'], 'has-error') }}">
            <label class="">{{ trans('general.seo_keywords') }}</label>
            {!! Form::text('seo_keywords_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('general.seo_keywords'))) !!}
            <span class="help-block">{{ $errors->first('seo_keywords_'.$lang['code'], ':message') }}</span>
        </div>
        <div class="form-group {{ $errors->first('seo_description_'.$lang['code'], 'has-error') }}">
            <label class="">{{ trans('general.seo_description') }}</label>
            {!! Form::text('seo_description_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('general.seo_description'))) !!}
            <span class="help-block">{{ $errors->first('seo_description_'.$lang['code'], ':message') }}</span>
        </div>
    </div>
</div>