<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-9">
        <ul class="nav nav-tabs">
            @foreach(langs() as $key => $lang)
                <li class="{{ $key == 0 ? 'active' : '' }}">
                    <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                        {{ $lang['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content tab-content-area" >
            @foreach(langs() as $key => $lang)
                <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                    <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="">{{ trans('page/form.title') }}</label>
                        {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('page/form.title'))) !!}
                        <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('name2_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="">{{ trans('page/form.title2') }}</label>
                        {!! Form::text('name2_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('page/form.title2'))) !!}
                        <span class="help-block">{{ $errors->first('name2_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('text_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="title-textarea">{{ trans('page/form.text') }}</label>
                        {!! Form::textarea('text_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('blog/form.ph-content'))) !!}
                        {!! $errors->first('text_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('text2_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="title-textarea">{{ trans('restaurant/form.text2') }}</label>
                        {!! Form::textarea('text2_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('restaurant/form.text'))) !!}
                        {!! $errors->first('text2_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('text3_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="title-textarea">{{ trans('restaurant/form.text2') }} 2</label>
                        {!! Form::textarea('text3_'.$lang['code'], null, array('id' => 'ckeditor_full','class' => 'textarea form-control', 'placeholder'=>trans('restaurant/form.text'))) !!}
                        {!! $errors->first('text3_'.$lang['code'], '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('video_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="">{{ trans('page/form.video') }}</label>
                        {!! Form::text('video_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('page/form.video'))) !!}
                        <span class="help-block">{{ $errors->first('video_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('link_'.$lang['code'], 'has-error') }}">
                        <label for="page_name" class="">Link (view more on news. etc)</label>
                        {!! Form::text('link_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('page/form.video'))) !!}
                        <span class="help-block">{{ $errors->first('link_'.$lang['code'], ':message') }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group img-area" >
            <label>
                @lang('page/form.image') 1  (or Icon for Submenu)
                @if((isset($page) && $page->id ==13))
                    Video
                @endif
            </label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">
                            @lang('general.select-file')
                        </span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
            </div>
            @if(isset($page->image) && !empty($page->image))
                <span class="fileupload-preview">
                        <a href="{{URL::to($page->image)}}" target="_blank" >
                            <img src="{{URL::to($page->image)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                        </a>
                    </span>
                <label class="check-for-delete-file">
                    <input type="checkbox" name="delete_image"  >
                    @lang('general.check_for_delete')
                </label>
            @endif
        </div>

            {{--IMAGE 1--}}
            <div class="form-group img-area" >
                <label>@lang('page/form.image') Image 2 (or Icon for Submenu)</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('general.select-file')</span>
                             {!! Form::file('image1', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($page->image1) && !empty($page->image1))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($page->image1)}}" target="_blank" >
                                <img src="{{URL::to($page->image1)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image1"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            {{--IMAGE 2--}}
            <div class="form-group img-area" >
                <label>@lang('page/form.image') Image 3</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-default btn-sm">
                            <span class="fileupload-new">@lang('general.select-file')</span>
                            {!! Form::file('image2', null, array('class' => 'form-control')) !!}
                        </span>
                </div>
                @if(isset($page->image2) && !empty($page->image2))
                    <span class="fileupload-preview">
                            <a href="{{URL::to($page->image2)}}" target="_blank" >
                                <img src="{{URL::to($page->image2)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                            </a>
                        </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image2"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>

        <div class="form-group {{ $errors->first('parent_id', 'has-error') }}" >
            {!! Form::select('parent_id',$pages ,null, array('class' => 'form-control select2', 'placeholder'=>trans('page/form.parentParent'))) !!}
            <span class="help-block">{{ $errors->first('parent_id', ':message') }}</span>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">
                <input type="checkbox" name="onNavMenu" @if(isset($page) && $page->onNavMenu == 1) checked @endif  value="1"  >
                @lang('page/form.onHeader')
                {!! Form::number('order', null, array('class' => 'order_page_input')) !!}
                @lang('page/form.orderMenu')
            </label>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">
                <input type="checkbox" name="haveSubmenu" @if(isset($page) && $page->haveSubmenu == 1) checked @endif  value="1"  >
                Have SubMenu (need to select on Category Page Parent)
            </label>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">
                <input type="checkbox" name="isCategory" @if(isset($page) && $page->isCategory == 1) checked @endif  value="1"  >
                Is category Page (need to select Category Menu)
            </label>
        </div>
        <div class="form-group {{ $errors->first('category_id', 'has-error') }}" >
            {!! Form::select('category_id',$categories ,null, array('class' => 'form-control select2', 'placeholder'=>'Category Menu')) !!}
            <span class="help-block">{{ $errors->first('category_id', ':message') }}</span>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">
                <input type="checkbox" name="onFooterShop" @if(isset($page) && $page->onFooterShop == 1) checked @endif  value="1"  >
                On footer Shop
            </label>
        </div>
        <div class="form-group">
            <label class="check-for-delete-file">
                <input type="checkbox" name="onFooterHelpAndInformation" @if(isset($page) && $page->onFooterHelpAndInformation == 1) checked @endif  value="1"  >
                On footer Help&Information
            </label>
        </div>

        <div class="form-group" >
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok"></i>
                @lang('general.save')
            </button>
            <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                <i class="glyphicon glyphicon-check"></i>
                @lang('general.save_and_exit')
            </button>
            <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
        </div>
    </div>
</div>