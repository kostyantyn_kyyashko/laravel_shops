<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-9">
        <ul class="nav nav-tabs">
            @foreach(langs() as $key => $lang)
                <li class="{{ $key == 0 ? 'active' : '' }}">
                    <a href="#{{ $lang['title'] }}" data-toggle="tab" aria-expanded="true">
                        {{ $lang['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content tab-content-area" >
            @foreach(langs() as $key => $lang)
                <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                    <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                        <label for="partner_name" class="">{{ trans('partner/form.title') }}</label>
                        {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('partner/form.title'))) !!}
                        <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('link_'.$lang['code'], 'has-error') }}">
                        <label for="partner_name" class="">{{ trans('partner/form.page_url') }}</label>
                        {!! Form::text('link_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('partner/form.page_url'))) !!}
                        <span class="help-block">{{ $errors->first('link_'.$lang['code'], ':message') }}</span>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <div class="form-group img-area" >
                <label>@lang('partner/form.image')</label>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-default btn-sm">
                        <span class="fileupload-new">@lang('general.select-file')</span>
                        {!! Form::file('image', null, array('class' => 'form-control')) !!}
                    </span>
                </div>
                @if(isset($partner->image) && !empty($partner->image))
                    <span class="fileupload-preview">
                        <a href="{{URL::to($partner->image)}}" target="_blank" >
                            <img src="{{URL::to($partner->image)}}" style="max-height: 60px" class="img-responsive" alt="Image">
                        </a>
                    </span>
                    <label class="check-for-delete-file">
                        <input type="checkbox" name="delete_image"  >
                        @lang('general.check_for_delete')
                    </label>
                @endif
            </div>
            <div class="clear"></div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok"></i>
                @lang('general.save')
            </button>
            <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                <i class="glyphicon glyphicon-check"></i>
                @lang('general.save_and_exit')
            </button>
            <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
        </div>
    </div>
</div>