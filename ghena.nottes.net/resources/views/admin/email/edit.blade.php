@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('email/title.edit')
    @parent
@stop

@include('admin.email._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('email/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/email') }}">
                    @lang('email/form.list')
                </a>
            </li>
            <li class="active">
                @lang('email/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($email, array('url' => URL::to('admin/email/' . $email->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.email._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.email._footer')
