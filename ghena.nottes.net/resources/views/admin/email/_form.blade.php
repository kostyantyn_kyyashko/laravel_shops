<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-8">

            <div class="tab-content tab-content-area" >
                @foreach(langs() as $key => $lang)
                    <div id="{{ $lang['title'] }}" class="tab-pane {{ $key == 0 ? 'active' : '' }}">
                        <div class="form-group {{ $errors->first('name_'.$lang['code'], 'has-error') }}">
                            <label for="page_name" class="">{{ trans('email/form.title') }}</label>
                            {!! Form::text('name_'.$lang['code'], null, array('class' => 'form-control', 'placeholder'=>trans('email/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('name_'.$lang['code'], ':message') }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">


            <div class="form-group btn-area-form">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok"></i>
                    @lang('general.save')
                </button>
                <button type="submit" class="btn btn-success" onclick="$('#save_and_exit').val(1);" >
                    <i class="glyphicon glyphicon-check"></i>
                    @lang('general.save_and_exit')
                </button>
                <a href="{!! URL::to('admin/email') !!}" class="btn btn-danger">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    @lang('general.back')
                </a>
                <input type="hidden" value="0" name="save_and_exit" id="save_and_exit">
            </div>

        </div>
    </div>
</div>