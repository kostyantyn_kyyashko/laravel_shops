@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
    @lang('size/title.edit')
    @parent
@stop

@include('admin.size._head')

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            @lang('size/form.edit')
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="material-icons breadmaterial">home</i> Dashboard
                </a>
            </li>
            <li >
                <a href="{{ URL::to('admin/size') }}">
                    @lang('size/form.list')
                </a>
            </li>
            <li class="active">
                @lang('size/form.edit')
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content paddingleft_right15">
        <!--main content-->
        <div class="row">
            <div class="the-box no-border">
                {!! Form::model($size, array('url' => URL::to('admin/size/' . $size->id), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
                    @include('admin.size._form')
                {!! Form::close() !!}
            </div>
        </div>

        <!--main content ends-->
    </section>
@stop

@include('admin.size._footer')
