<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'size'			=> 'Marimi',
    'color'			=> 'Culori',
    'code'			=> 'Cod Culoare',
    'select'			=> 'Alege',
    'qt'			=> 'Cantitatea',
    'type'			=> 'Tip',
    'subcats'			=> 'Subcategorii',
    'subsubcats'			=> 'Sub Subcategorii',
    'link'			=> 'Link',
    'weight'			=> 'Greutate',
    'slug'			=> 'Url',
    'selectCat'			=> 'Selecteaza',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Nume',
    'name'			=> 'Produse',
    'list'			=> 'Lista Produse',
    'text' 		    => 'Continut',
    'category' 		    => 'Categoria',
    'add' 	        => 'Adauga Produs',
    'edit' 	        => 'Editeaza Produs'
);
