<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Produse',
    'add' => 'Adauga Produs',
    'create'			=> 'Adauga Produs',
    'edit' 				=> 'Editeaza Produs',
    'list' => 'Lista Produse',
);
