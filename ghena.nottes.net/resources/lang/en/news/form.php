<?php
/**
* Language file for group management form text
*
*/
return array(

    'edit'			=> 'Editeaza',
    'title'			=> 'Nume',
    'title2'			=> 'Titlu Continut',
    'title3'			=> 'Titlu Colectie',
    'title4'			=> 'Titlu Jos',
    'category'	    => 'Categoria',
    'date'			=> 'Data',
    'time'			=> 'Ora',
    'image'			=> 'Imagine',
    'text'			=> 'Continut',
    'text2'			=> 'Continut Jos',
    'name'			=> 'Blog',
    'short'			=> 'Descriere Scurta',
    'list'			=> 'Lista de Blog',
    'general' 		=> 'General',
    'add' 		    => 'Adauga',
    'var' 		    => 'Indetificator Unic',
    'addNews' 		=> 'Adauga Blog',
    'editNews' 		=> 'Editeaza Blog',
    'countryName'       => 'Nume Blog',
    'update-blog'    => 'update blog',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
