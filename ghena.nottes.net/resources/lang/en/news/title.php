<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Blog',
    'create'			=> 'Create',
    'edit' 				=> 'Edit',
    'management' => 'Manage Blog Categories',
    'list' => 'Lista de Blog',
    'groups' => 'Groups',
    'categories' => 'Categories',
    'blogcategorylist' => 'Blog Categories List',


    
);
