<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Categorii',
    'addArticle' => 'Adauga Categoria',
    'create'			=> 'Adauga Categoria',
    'edit' 				=> 'Editeaza Categoria',
    'list' => 'Lista Categorii',
    'groups' => 'Groups',
    'categories' => 'Categories',
);
