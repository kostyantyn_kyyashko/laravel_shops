<?php
/**
* Language file for group management form text
*
*/
return array(

    'onTop'			=> 'In meniul de Sus',
    'onHeader'			=> 'In meniul din Header',
    'onFooter'			=> 'In Meniul de Jos',
    'edit'			=> 'Editeaza',
    'orderMenu'			=> 'Nr de ordine',
    'link'		=> 'Link',
    'address'		=> 'Adresa',
    'price'		=> 'Pret',
    'page_url'		=> 'Url Meniu',
    'tags'			=> 'Taguri',
    'selectCat'			=> 'Alege',
    'date'			=> 'Data',
    'text'			=> 'Continut',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Nume',
    'title2'	    => 'Titlu adaugator',
    'parent'		=> 'Parinte',
    'name'			=> 'Categorii',
    'list'			=> 'Lista Categorii',
    'general' 		=> 'General',
    'text' 		    => 'Continut',
    'text2' 		=> 'Continut adaugator',
    'category' 		=> 'Categoria',
    'parentParent' 		=> 'Pagina Parinte',
    'selectCategory'=> 'Alegea Categoria',
    'add' 		    => 'Adauga',
    'addPage' 	=> 'Adauga Pagina',
    'editPage' 	=> 'Editeaza Pagina',
    'articleName'   => 'Nume Categorii',
    'update-blog'   => 'update blog',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
