<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'selectLocation'=> 'Alege Locatia',
    'selectProduct'	=> 'Alege Produs',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'link'			=> 'Link Colectii',
    'title'			=> 'Nume',
    'title2'	    => 'Functia',
    'product'		=> 'Pordus',
    'name'			=> 'Slider Home',
    'nameMenu'		=> 'Slider Home',
    'list'			=> 'Lista Slider Home',
    'text' 		    => 'Continut',
    'text2' 	    => 'Continut Aduagator',
    'add' 	        => 'Adauga Slider',
    'edit' 	        => 'Editeaza Slider'
);
