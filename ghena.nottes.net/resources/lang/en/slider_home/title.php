<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Slider Home',
    'titleMenu' => 'Slider Home',
    'add'   => 'Adauga Slider',
    'create'			=> 'Adauga Slider',
    'edit' 				=> 'Editeaza Slider',
    'list' => 'Lista Slider Home',
);
