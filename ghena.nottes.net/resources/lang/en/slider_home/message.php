<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Slider Home already exists!',
    'category_not_found'           => 'Slider Home [:id] does not exist.',
    'category_have_blog'           => 'Slider Home have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Slider Home was successfully updated.',
        'delete'    => 'Slider Home was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
