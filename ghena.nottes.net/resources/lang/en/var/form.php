<?php
/**
* Language file for group management form text
*
*/
return array(

    'edit'			=> 'Editeaza',
    'title'			=> 'Nume',
    'name'			=> 'Variabile',
    'list'			=> 'Lista Variabile',
    'general' 		=> 'General',
    'info' 		    => 'Explicatie Utilizare',
    'add' 		    => 'Adauga',
    'var' 		    => 'Indetificator Unic',
    'addVar' 		=> 'Adauga Variabila',
    'editVar' 		=> 'Editeaza Variabila',
    'varName'       => 'Nume Variabila',
    'update-blog'    => 'update blog',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
