<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'id'         => 'Id',
    'name'       => 'Nume',
    'blogs'      => 'No. of Blogs',
    'created_at' => 'Created at',
    'actions'	 => 'Actions',

);
