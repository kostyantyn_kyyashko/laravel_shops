<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'id'         => 'Id',
    'parent'       => 'Parinte',
    'name'       => 'Nume',
    'blogs'      => 'No. of Blogs',
    'created_at' => 'Created at',
    'date'       => 'Data',
    'category'       => 'Categoria',
    'actions'	 => 'Actions',

);
