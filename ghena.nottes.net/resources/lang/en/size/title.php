<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Marimi',
    'add' => 'Adauga Marimea',
    'create'			=> 'Adauga Marimea',
    'edit' 				=> 'Editeaza Marimea',
    'list' => 'Lista Marimi',
);
