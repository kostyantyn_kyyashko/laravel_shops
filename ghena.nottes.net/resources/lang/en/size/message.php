<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Marimea already exists!',
    'category_not_found'           => 'Marimea [:id] does not exist.',
    'category_have_blog'           => 'Marimea have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Marimea was successfully updated.',
        'delete'    => 'Marimea was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
