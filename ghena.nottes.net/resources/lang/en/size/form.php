<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'type'			=> 'Tip',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Marimi',
    'name'			=> 'Marimea',
    'list'			=> 'Lista Marimi',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Marimi',
    'edit' 	        => 'Editeaza Marimi'
);
