<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Comenzi',
    'add' => 'Adauga Comanda',
    'create'			=> 'Adauga Comanda',
    'edit' 				=> 'Editeaza Comanda',
    'list' => 'Lista Comenzi',
);
