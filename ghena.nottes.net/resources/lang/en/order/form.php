<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'first_name'	=> 'Nume',
    'date'	=> 'Data',
    'user'       	=> 'Utilizator',
    'last_name'		=> 'Prenume',
    'status'		=> 'Statut',
    'total'			=> 'Total',
    'sum'			=> 'Suma',
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Nume',
    'name'			=> 'Comenzi',
    'list'			=> 'Lista Comenzi',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Comanda',
    'edit' 	        => 'Editeaza Comanda'
);
