<?php
/**
* Language file for group management form text
*
*/
return array(

    'onTop'			=> 'In meniul de Sus',
    'onHeader'			=> 'In meniul din Header',
    'onFooter'			=> 'In Meniul de Jos',
    'edit'			=> 'Editeaza',
    'orderMenu'			=> 'Nr de ordine',
    'address'		=> 'Adresa',
    'page_url'		=> 'Url Meniu',
    'tags'			=> 'Taguri',
    'date'			=> 'Data',
    'video'			=> 'Video',
    'text'			=> 'Continut',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Nume',
    'title2'	    => 'Titlu adaugator',
    'name'			=> 'Pagini',
    'list'			=> 'Lista Pagine',
    'general' 		=> 'General',
    'location' 		=> 'Locatia',
    'text' 		    => 'Continut',
    'text2' 		=> 'Continut adaugator',
    'category' 		=> 'Categoria',
    'parentParent' 		=> 'Pagina Parinte',
    'selectCategory'=> 'Alegea Categoria',
    'add' 		    => 'Adauga',
    'addPage' 	=> 'Adauga Pagina',
    'editPage' 	=> 'Editeaza Pagina',
    'articleName'   => 'Nume Pagine',
    'update-blog'   => 'update blog',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
