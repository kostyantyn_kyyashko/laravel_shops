<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Pagine already exists!',
    'category_not_found'           => 'Pagine [:id] does not exist.',
    'category_have_blog'           => 'Pagine have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Pagine was successfully updated.',
        'delete'    => 'Pagine was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
