<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Pagini',
    'addArticle' => 'Adauga Pagina',
    'create'			=> 'Adauga Pagina',
    'edit' 				=> 'Editeaza Pagina',
    'management' => 'Manage Blog Categories',
    'list' => 'Lista Pagini',
    'groups' => 'Groups',
    'categories' => 'Categories',
    'blogcategorylist' => 'Blog Categories List',


    
);
