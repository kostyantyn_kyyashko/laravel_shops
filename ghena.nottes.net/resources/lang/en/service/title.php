<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Servicii',
    'addArticle' => 'Adauga Serviciu',
    'create'			=> 'Adauga Serviciu',
    'edit' 				=> 'Editeaza Serviciu',
    'management' => 'Manage Blog Categories',
    'list' => 'Lista Servicii',
    'groups' => 'Groups',
    'categories' => 'Categories',
    'blogcategorylist' => 'Blog Categories List',


    
);
