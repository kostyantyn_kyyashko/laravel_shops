<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Servicii already exists!',
    'category_not_found'           => 'Servicii [:id] does not exist.',
    'category_have_blog'           => 'Servicii have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Servicii was successfully updated.',
        'delete'    => 'Servicii was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
