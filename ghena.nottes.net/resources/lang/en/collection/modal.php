<?php

/**
* Language file for blog delete modal
*
*/
return array(

    'title'         => 'Sterge',
    'body'			=> 'Sunte-ti sigur ca doriti sa stergeti? Operatia data este ireversibila.',
    'cancel'		=> 'Nu',
    'confirm'		=> 'Da',

);
