<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Categorii',
    'create'			=> 'Adauga',
    'edit' 				=> 'Editeaza Colectia',
    'management' => 'Manage Blog Colectii',
    'list' => 'Lista Categorii Home',
    'groups' => 'Groups',
    'categories' => 'Colectii',
    'blogcategorylist' => 'Blog Colectii List',
);
