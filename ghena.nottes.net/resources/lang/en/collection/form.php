<?php
/**
* Language file for group management form text
*
*/
return array(

    'checkTop'			=> 'Bifeaza daca dorest sa apara in meniu de sus',
    'checkNav'			=> 'Bifeaza daca dorest sa apara in meniu din mijloc',
    'checkFooter'	=> 'Bifeaza daca dorest sa apara in meniu de jos',
    'edit'			=> 'Editeaza',
    'onTop'			=> 'In meniul de sus',
    'onNavMenu'			=> 'In meniul de mijloc',
    'onFooter'			=> 'In meniul de jos',
    'title'			=> 'Nume',
    'text'			=> 'Continut',
    'name'			=> 'Colectii',
    'list'			=> 'Lista Colectii',
    'general' 		=> 'General',
    'add' 		=> 'Adauga',
    'addCategory' 		=> 'Adauga Colectie',
    'editCategory' 		=> 'Editeaza Colectie',
    'categoryName' => 'Nume Colectie',
    'update-blog' => 'update blog',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
