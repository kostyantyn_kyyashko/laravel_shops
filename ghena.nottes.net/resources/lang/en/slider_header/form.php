<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Nume',
    'title2'	    => 'Functia',
    'name'			=> 'Slider Header',
    'nameMenu'		=> 'Slider Header',
    'list'			=> 'Lista Slider Header',
    'text' 		    => 'Continut',
    'text2' 	    => 'Continut Aduagator',
    'add' 	        => 'Adauga Galleria',
    'edit' 	        => 'Editeaza Galleria'
);
