<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Slider Header already exists!',
    'category_not_found'           => 'Slider Header [:id] does not exist.',
    'category_have_blog'           => 'Slider Header have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Slider Header was successfully updated.',
        'delete'    => 'Slider Header was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
