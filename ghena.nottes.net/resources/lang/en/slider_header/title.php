<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Slider Header',
    'titleMenu' => 'Slider Header',
    'add'   => 'Adauga Slider',
    'create'			=> 'Adauga Slider',
    'edit' 				=> 'Editeaza Slider',
    'list' => 'Lista Slider Header',
);
