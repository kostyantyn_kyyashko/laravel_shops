<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Rolul',
    'add' => 'Adauga Rolul',
    'create'			=> 'Adauga Rolul',
    'edit' 				=> 'Editeaza Rolul',
    'list' => 'Lista Roluri',
);
