<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Nume',
    'name'			=> 'Roluri',
    'list'			=> 'Lista Roluri',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Roluri',
    'edit' 	        => 'Editeaza Roluri'
);
