<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'id'         => 'Id',
    'name'       => 'Nume',
    'date'       => 'Data',
    'actions'	 => 'Actions',

);
