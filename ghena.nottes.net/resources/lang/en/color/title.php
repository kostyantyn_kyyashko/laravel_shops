<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Culori',
    'add' => 'Adauga Culoarea',
    'create'			=> 'Adauga Culoarea',
    'edit' 				=> 'Editeaza Culoarea',
    'list' => 'Lista Culori',
);
