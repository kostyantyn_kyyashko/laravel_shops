<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'type'			=> 'Tip',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Culori',
    'name'			=> 'Culori',
    'list'			=> 'Lista Culori',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Culori',
    'edit' 	        => 'Editeaza Culori'
);
