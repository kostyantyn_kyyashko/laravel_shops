<?php
/**
* Language file for general strings
*
*/
return array(
    'check_for_delete'  => 'Bifeaza daca dorest sa stergi fisierul',
    'save_order'  		=> 'Salveaza Ordinea',
    'video_url'  		=> 'Adresa Video',
    'text'  			=> 'Continut',
    'name'  			=> 'Nume',
    'link'  			=> 'Link',
    'video'  			=> 'Video Url',
    'gallery'  			=> 'Galerie',
    'viewPermision'  			=> 'Vizualizeaza',
    'options'  			=> 'Optiuni',
    'select-file'  		=> 'Selecteaza Fisier',
    'edit'  			=> 'Editeaza',
    'date'  			=> 'Data',
    'delete'  			=> 'Sterge',
    'save'  			=> 'Salveaza',
    'save_and_exit'  	=> 'Salveaza si Iesi',
    'cancel'  			=> 'Anuleaza',
    'back'  			=> 'Inapoi',
    'add'  			=> 'Adauga',
    'noresults'  	=> 'No Results',
    'yes' 			=> 'Yes',
    'phone'			=> 'Telefon',
    'email' 			=> 'Email',
    'view' 			=> 'Vizualizare',
    'order' 			=> 'Numar de ordine',
    'site_name' => 'SiteName',
    'message' => 'Mesaj',
    'home' => 'Home',
    'status' => 'Statut',
    'dashboard' => 'Dashboard',
    'public' => 'Publica',
    'draft' => 'Ciorna',

    'seo_title' => 'Seo Titlu',
    'seo_keywords' => 'Seo Cuvinte Cheie',
    'seo_description' => 'Seo Descriere',


    'user_profile'			=> 'Profil Utilizator',
    'users'			        => 'Utilizatori',
    'usersList'			    => 'Lista de utilizatori',
    'actions'			    => 'Actiuni',
    'first_name'			=> 'Nume',
    'last_name'				=> 'Prenume',
    'password'				=> 'Parola',
    'addUser'				=> 'Adauga Utilizator',
    'editUser'				=> 'Editeaza Utilizator',
    'confirmPassword'		=> 'Confirma Parola',
    'email'					=> 'E-mail',

    // for daac hermes
    'daac_hermes_md' => 'daac-hermes.md'

);
