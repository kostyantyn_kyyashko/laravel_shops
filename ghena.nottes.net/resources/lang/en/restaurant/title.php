<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Restauranturi',
    'add' => 'Adauga Restaurant',
    'create'			=> 'Adauga Restaurant',
    'edit' 				=> 'Editeaza Restaurant',
    'list' => 'Lista Restauranturi',
);
