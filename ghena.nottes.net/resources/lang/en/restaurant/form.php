<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'gallery'	    => 'Galerie',
    'title'			=> 'Nume',
    'lat'			=> 'Latitudine',
    'lng'			=> 'Longitudine',
    'name'			=> 'Restaurante',
    'list'			=> 'Lista Restaurante',
    'text' 		    => 'Continut',
    'text2' 		=> 'Continut Adaugator',
    'add' 	        => 'Adauga Restaurant',
    'edit' 	        => 'Editeaza Restaurant'
);
