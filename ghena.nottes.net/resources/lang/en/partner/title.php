<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Parteneri',
    'addArticle' => 'Adauga Partener',
    'create'			=> 'Adauga Partener',
    'edit' 				=> 'Editeaza Partener',
    'management' => 'Manage Blog Categories',
    'list' => 'Lista Parteneri',
    'groups' => 'Groups',
    'categories' => 'Categories',
    'blogcategorylist' => 'Blog Categories List',


    
);
