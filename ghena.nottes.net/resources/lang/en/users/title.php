<?php
/**
* Language file for user section titles
*
*/

return array(

	'user_profile'			=> 'Profil Utilizator',
	'users'			        => 'Utilizatori',
	'usersList'			    => 'Lista de utilizatori',
	'actions'			    => 'Actiuni',
	'first_name'			=> 'Nume',
	'last_name'				=> 'Prenume',
	'password'				=> 'Parola',
	'confirmPassword'		=> 'Confirma Parola',
	'email'					=> 'E-mail',
	'phone'					=> 'Phone Number',
	'address'				=> 'Address',
	'city'					=> 'City',
	'status'				=> 'Status',
	'created_at'			=> 'Created At',
    'select_image'			=> 'Select Image',
    'gender'				=> 'Gender / Sex',
    'dob'					=> 'Birth Date',
    'country'				=> 'Country',
    'state'					=> 'State',
    'postal'				=> 'Postal / Zip code'
    
);
