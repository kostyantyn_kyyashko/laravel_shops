<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Emailuri',
    'add' => 'Adauga Email',
    'create'			=> 'Adauga Email',
    'edit' 				=> 'Editeaza Email',
    'list' => 'Lista Emailuri',
);
