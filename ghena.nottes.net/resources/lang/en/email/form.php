<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'type'			=> 'Tip',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Email',
    'name'			=> 'Emaluri',
    'list'			=> 'Lista Emaluri',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Email',
    'edit' 	        => 'Editeaza Email'
);
