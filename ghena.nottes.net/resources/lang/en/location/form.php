<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'shipping_cost'	=> 'Pret Livrare',
    'shipping_free_from' => 'Pret Gratis Livrare de la',
    'price'			=> 'Pret',
    'type'			=> 'Tip',
    'code'			=> 'Short Cod',
    'short_title'			=> 'Short Title',
    'video'			=> 'Video',
    'lat'			=> 'Latitudine',
    'lng'			=> 'Longitudine',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Localitate',
    'name'			=> 'Localitati',
    'list'			=> 'Lista Localitati',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Localitate',
    'edit' 	        => 'Editeaza Localitate'
);
