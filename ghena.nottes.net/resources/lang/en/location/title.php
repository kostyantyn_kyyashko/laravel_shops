<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Localitati',
    'add' => 'Adauga Localitatea',
    'create'			=> 'Adauga Localitatea',
    'edit' 				=> 'Editeaza Localitatea',
    'list' => 'Lista Localitati',
);
