<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Localitate already exists!',
    'category_not_found'           => 'Localitate [:id] does not exist.',
    'category_have_blog'           => 'Localitate have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Localitate was successfully updated.',
        'delete'    => 'Localitate was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
