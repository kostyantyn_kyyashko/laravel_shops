<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Carduri',
    'add' => 'Adauga Card',
    'create'			=> 'Adauga Card',
    'edit' 				=> 'Editeaza Card',
    'list' => 'Lista Carduri',
);
