<?php
/**
* Language file for group management form text
*
*/
return array(
    'edit'			=> 'Editeaza',
    'text'			=> 'Continut',
    'price'			=> 'Pret',
    'type'			=> 'Tip',
    'link'			=> 'Link',
    'slug'			=> 'Url',
    'image'			=> 'Imagine',
    'select-file'	=> 'Alege Fisier',
    'change'	    => 'Schimba',
    'title'			=> 'Card',
    'client'		=> 'Client',
    'barcode'		=> 'Barcode',
    'discount'		=> 'Reducere %',
    'name'			=> 'Carduri',
    'list'			=> 'Lista Carduri',
    'text' 		    => 'Continut',
    'add' 	        => 'Adauga Card',
    'edit' 	        => 'Editeaza Card'
);
