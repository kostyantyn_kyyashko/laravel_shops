<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Card already exists!',
    'category_not_found'           => 'Card [:id] does not exist.',
    'category_have_blog'           => 'Card have blogs!',

    'success' => array(
        'create'    => 'Creat cu sucess.',
        'update'    => 'Card was successfully updated.',
        'delete'    => 'Card was successfully deleted.',
        'destroy'   => 'Sters cu success.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the blog category. Please try again.',
        'update'    => 'There was an issue updating the blog category. Please try again.',
        'delete'    => 'There was an issue deleting the blog category. Please try again.',
    ),

);
