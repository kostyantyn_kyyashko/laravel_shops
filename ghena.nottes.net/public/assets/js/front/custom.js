$(document).ready(function(){

    // -------------------- ADD TO CART ------------------

    $(document).on("click", ".ajax_add_to_cart", function (e) {
        e.preventDefault();
        var product_id = $(this).attr('data-product_id');
        var  page = $(this).attr('data-page');
        $.ajax({
            type: "POST",
            url: "/api/v1/cart/addToCart",
            data: {type : $(this).attr('data-type'), size_id: $(this).attr('data-size'), color_id: $(this).attr('data-color'), qt: $(this).attr('data-qt'),id : product_id,'_token': $('.csrf_token').val() },
            dataType : "json",
            success: function(data) {
                $('.header__bag-quantity').html(data.count);
                $('.view-cart').show();
            }
        });
    });

    $(document).on("click", ".ajax_remove_from_cart", function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/api/v1/cart/removeFromCart",
            data: {rowId: $(this).attr('data-rowId'),'_token': $('.csrf_token').val() },
            dataType : "json",
            success: function(data) {
                window.location.href =$('.current_url').val();
                location.reload();
                window.location.reload();

            }
        });
        window.location.href = $('.current_url').val();

    });
    $(document).on("click", ".cart_product_color", function (e) {
        updateCartProductOptions('color_id',$(this))
    })
    $(document).on("click", ".cart_product_size", function (e) {
        updateCartProductOptions('size_id',$(this))
    })
    function updateCartProductOptions(optionName,i) {
        $.ajax({
            type: "POST",
            url: "/api/v1/cart/updateCartProductOptions",
            data: { optionName: optionName,optionValue: i.attr('data-value'),rowId :  i.attr('data-rowId'),'_token': $('.csrf_token').val() },
            dataType : "json",
            success: function(data) {
                $('.product_rowId_'+i.attr('data-product_id')).attr('data-rowId',data.rowId);
            }
        });
    }

    $(document).on("click", ".btn-delivery-info", function (e) {

        var valid = true;
        // terms
        var term1 = $('[name="term1"]');
        if (!term1.is(':checked')) {
            $('.checkmark').addClass('error');
            valid = false;
        } else {
            $('.checkmark').removeClass('error');
        }
        if(valid) {
            $(".delivery-info-form").submit();
        }
        $('[name="first_name"],[name="last_name"],[name="email"],[name="phone"]').each(function() {
            if ($(this).val() === '') {
                $(this).addClass('error').prev().addClass('error');
                valid = false;
            } else {
                $(this).removeClass('error').prev().removeClass('error');
            }
        });

    })

    $(document).on("click", ".shipping_method", function (e) {
        var total = $('.cart_total_value').val();
        var shipping = $(this).attr('data-price');
        $('.cart_total').html(parseInt(total)+parseInt(shipping));
    })

    // -------------------- cart__quantity --------------------------
    // todo remove from main.js

    $('.product-quantity__bnt--minus').on('click', function () {
        let $input = $(this).next('.product-quantity__input');
        let inputValue = $input.val();
        if (inputValue > 0) {
            inputValue--;
            if(inputValue == 0) { inputValue = 1;}
            $input.val(inputValue)
            $('.add_to_cart_'+$(this).attr('data-product_id')).attr("data-qt",inputValue);
            updateCartProductQt(inputValue,$(this));
        }
    });

    $('.product-quantity__bnt--plus').on('click', function () {
        let $input = $(this).prev('.product-quantity__input');
        let inputValue = $input.val();
        inputValue++;
        $input.val(inputValue);
        $('.add_to_cart_'+$(this).attr('data-product_id')).attr("data-qt",inputValue);
        updateCartProductQt(inputValue,$(this));
    });

    function updateCartProductQt(count,i) {
        if(count>0) {
            var product_id    = i.attr('data-id');
            var product_price = i.attr('data-price');
            // $('.price_item_'+product_id).html(count*product_price);
            $.ajax({
                type: "POST",
                url: "/api/v1/cart/updateCartProductQt",
                data: { qt: count,rowId :  i.attr('data-rowId'),'_token': $('.csrf_token').val() },
                dataType : "json",
                success: function(data) {
                    $('.cart_total').html(data.total);
                    $('.header__bag-quantity').html(data.count);
                }
            });
        }

    }


    // -------------------- END ADD TO CART ------------------

    // -------------------- WISHLIST ---------------

    $(document).on("click", ".remove_from_wishlist", function (e) {
        e.preventDefault();
        var product_id = $(this).attr('data-id');
        $('.cart-card_item_'+product_id).remove();
        $.ajax({
            type: "POST",
            url: '/api/v1/product/removeWishlist',
            data: {product_id:product_id,'_token': $('.csrf_token').val()},
            dataType : "json",
            success: function(data) {

            }
        });
    });

    // -------------------- END WISHLIST ---------------


    // -------------------- SEARCH PRODUCTS ------------------

    // product slider range todo remove from main.js filter range__slider
    // http://ionden.com/a/plugins/ion.rangeSlider/demo_interactions.html
    $('.filter__range').ionRangeSlider({
        type: "double",
        skin: "round",
        min: 15,
        max: 500,
        from: 15,
        to: 500,
        postfix: ' RON',
        hide_min_max: true,
        onChange: function (data) {
            updateProductList(data.from_pretty,data.to_pretty)
        },
    });

    function updateProductList(price_from,price_to) {
        var  upUrl = new URL($('.products_url').val());

        upUrl.searchParams.delete('price_from');
        upUrl.searchParams.delete('price_to');
        if(price_from) {
            upUrl.searchParams.append('price_from', price_from);
            upUrl.searchParams.append('price_to', price_to);
        }

        upUrl.searchParams.delete('sizes[]');
        var sizeIDs = [];
        $(".filter_size_list li a").each(function(){
            if($(this).hasClass('active')) {
                var id = $(this).attr('data-id');
                if(id != undefined) {
                    sizeIDs.push(id);
                    upUrl.searchParams.append('sizes[]', id);
                }
            }
        });

        upUrl.searchParams.delete('colors[]');
        var colorIDs = [];
        $(".filter_color_list li a").each(function(){
            if($(this).hasClass('active')) {
                var id = $(this).attr('data-id');
                if(id != undefined) {
                    colorIDs.push(id);
                    upUrl.searchParams.append('colors[]', id);
                }
            }
        });

        upUrl.searchParams.delete('collections[]');
        var collectionIDs = [];
        $(".filter_collection_list li a").each(function(){
            if($(this).hasClass('active')) {
                var id = $(this).attr('data-id');
                if(id != undefined) {
                    collectionIDs.push(id);
                    upUrl.searchParams.append('collections[]', id);
                }
            }
        });

        upUrl.searchParams.delete('models[]');
        var modelIDs = [];
        $(".filter_model_list li a").each(function(){
            if($(this).hasClass('active')) {
                var id = $(this).attr('data-id');
                if(id != undefined) {
                    modelIDs.push(id);
                    upUrl.searchParams.append('models[]', id);
                }
            }
        });

        $.ajax({
            type: "POST",
            url: "/api/v1/product/list",
            data: {   category_id: $('.category_id').val(), sub_category_id: $('.sub_category_id').val(),lng: $('.lng').val(),models: modelIDs,collections: collectionIDs,sizes: sizeIDs,colors: colorIDs,'_token': $('.csrf_token').val() },
            dataType : "json",
            success: function(resp) {
                // generate push url
                if(upUrl) {
                    history.pushState(null,null,upUrl.href);
                }

                $('.product_list_area').html(resp.product_list_area);
                $('.product_pagination_area').html(resp.product_pagination_area);

            }
        });
    }
    // click on size list
    $(document).on("click", ".li_size", function (e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        updateProductList();
    });

    // click on color list
    $(document).on("click", ".li_color", function (e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        updateProductList();
    });


    // click on model list
    $(document).on("click", ".li_model", function (e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        updateProductList();
    });


    // click on collection list
    $(document).on("click", ".li_collection", function (e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        updateProductList();
    });

    // -------------------- END SEARCH PRODUCTS ------------------

    // radio buttons
    $(document).on("click", ".ch_label_radio_look input[type=radio]", function () {
        var el = $(this);
        // remove checked for current product
        var product_id = el.attr('data-product_id');
        $('.ch_label_' + product_id).removeClass('active');
        $('.ch_label_' + product_id).find('input').prop('checked',false);
        $('.ch_label_' + product_id).find('input').removeAttr('checked');

        if (el.is(':checked')) {
            el.parent().addClass("active");
            el.prop('checked',false);
            el.attr('checked',false);
        }
        else {
            el.parent().removeClass("active");
            el.prop('checked',true);
            el.attr('checked','checked');
        }
        $('.add_to_cart_'+$(this).attr('data-product_id')).attr("data-color",$(this).attr('data-id'));
    });
    // end radio buttons

    // todo removed from main.js
    $('.select-size__list li').on('click', function (e) {
        if (!$(this).children('.select-size__stock').length) {
            let sizeItemValue = $(this).children('.select-size__item-value').text();
            var product_id =  $(this).children('.select-size__item-value').attr('data-id');
            let $sizeArea = $(this).parent('.select-size__list').prev('.select-size__value');
            $sizeArea.text(sizeItemValue);
            $sizeArea.addClass('active');
            var product_id =  $(this).children('.select-size__item-value').attr('data-product_id');
            var size_id   =  $(this).children('.select-size__item-value').attr('data-id');
            $('.select_size_'+product_id).val(size_id);
            $('.add_to_cart_'+product_id).attr("data-size",size_id);

            $('.selected_location_code').val($(this).children('.select-size__item-value').attr('data-code'));

        } else {
            e.stopPropagation();
        }
    });


    // -------------------- SUBSCRIBE FOOTER ---------------

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    // subscribe
    $(".subscribe_form").on("click", ".subscribe", function() {
        var email = $('.subscribe_email').val();
        if(!validateEmail(email) || email == '')
        {
            alert($('.emailul_trebuie_sa_fie_valid').val());
        }
        else
        {
            $.post("/subscribeNow",{email : email, _token: $('.csrf_token').val()},function(data){
                alert(data.message);
                $('.subscribe_email').val('');
            });
        }
    });

    // -------------------- END SUBSCRIBE FOOTER ---------------

});