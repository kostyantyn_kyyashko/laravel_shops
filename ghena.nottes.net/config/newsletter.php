<?php

return [

    /*
     * The API key of a MailChimp account. You can find yours at
     * https://us10.admin.mailchimp.com/account/api-key-popup/.
     */
    'apiKey' => 'c6fbe2a924828b5d2c722c1c991f23f8-us15',

    /*
     * The listName to use when no listName has been specified in a method.
     */
    'defaultListName' => 'subscribers',

    /*
     * Here you can define properties of the lists.
     */
    'lists' => [

        /*
         * This key is used to identify this list. It can be used
         * as the listName parameter provided in the various methods.
         *
         * You can set it to any string you want and you can add
         * as many lists as you want.
         */
        'subscribers' => [

            /*
             * A MailChimp list id. Check the MailChimp docs if you don't know
             * how to get this value:
             * http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id.
             * https://us19.admin.mailchimp.com/lists/dashboard/manage-subscribers?id=90149
             * https://us19.admin.mailchimp.com/lists/settings/defaults?id=90149
             * Some plugins and integrations may request your List ID.
                Typically, this is what they want: 7499949955
             */
            'id' => '04a89684d9',
        ],
    ],

    /*
     * If you're having trouble with https connections, set this to false.
     */
    'ssl' => false,

];
